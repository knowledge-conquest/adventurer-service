package controllers;

import com.jackeri.kc.controllers.AdventurersController;
import com.jackeri.kc.generation.RealmGenerator;
import com.jackeri.kc.models.entities.Adventurer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import play.mvc.Http;
import com.jackeri.kc.services.ControllerHelper;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.utils.exceptions.NoUserIdException;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

public class AdventurersControllerTest {

    private ControllerHelper controllerHelper;
    private AdventurerService adventurerService;
    private RealmGenerator realmGenerator;
    private Http.Request request;

    @BeforeEach
    void setup() {
        controllerHelper = mock(ControllerHelper.class);
        adventurerService = mock(AdventurerService.class);
        realmGenerator = mock(RealmGenerator.class);
        request = mock(Http.Request.class);
    }

    @Test
    public void itCanCreateAnAdventurer() throws NoUserIdException {
        String key = "123";
        String name = "Jack";
        long level = 1L;

        Adventurer adventurer = new Adventurer(name, level);
        adventurer.setKey(key);

        when(controllerHelper.extractUserIDFrom(any())).thenReturn(level);
        when(adventurerService.create(anyString(), anyLong())).thenReturn(
                CompletableFuture.completedFuture(adventurer)
        );
        when(realmGenerator.createRealm(anyString())).thenReturn(
                CompletableFuture.completedFuture(null)
        );

        try {
            new AdventurersController(controllerHelper, adventurerService, realmGenerator)
                    .createAdventurer(request, name)
                    .get();

            verify(controllerHelper, times(1)).extractUserIDFrom(any());
            verify(adventurerService, times(1)).create(name, level);
            verify(realmGenerator, times(1)).createRealm(key);

        } catch (InterruptedException e) {
            e.printStackTrace();
            fail();
        } catch (ExecutionException e) {
            e.printStackTrace();
            fail();
        }
    }
}
