# Adventurer Service

Service managing adventurers data such as levels, inventories or skills.

## Environment variables
This project uses environement variables. In brackets lies the default value.

- HTTP_SECRET (`changeme`): Contains the http secret required by play to run properly in deployment mode.
- HTTP_PORT (`8080`): The port on which the application should run (Might have no effect in development run).
- JWT_ISSUER (`kc-auth`): The issuer of the JWT tokens used for authentication.
- JWT_SECRET (`secret`): The secret for JWT tokens.
- ARANGO_HOST (`localhost`): The host of ArangoDB.
- ARANGO_PORT (`8529`): The port of ArangoDB.
- ARANGO_NAME (`kc-game-db`): The name of the databse to use.
- ARANGO_USERNAME (`root`): The username to use in order to connect to ArangoDB.
- ARANGO_PASSWORD (`secret`): The password to  use in order to connect to ArangoDB.
- ARANGO_TTL (`null`): The maximum TTL (in miliseconds) for each connection in the ArangoDB connection pool. `null` means no maximum TTl.