package com.jackeri.kc.exceptions;

import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletionException;

public class EntityBusyException extends CustomException {

    public EntityBusyException() {
        super("entity busy");
    }

    @Override
    public Result toResult() {
        return Results.badRequest("entity busy");
    }

    public static CompletionException completionException() {
        return CustomException.completionException(new EntityBusyException());
    }
}
