package com.jackeri.kc.exceptions;

import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletionException;

public class WrongAreaException extends CustomException {

    public WrongAreaException() {
        super("Wrong area.");
    }

    @Override
    public Result toResult() {
        return Results.badRequest("wrong area");
    }

    public static CompletionException completionException() {
        return CustomException.completionException(new WrongAreaException());
    }
}
