package com.jackeri.kc.exceptions;

import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletionException;

public class BadArgumentException extends CustomException {

    public BadArgumentException() {
        super("bad argument");
    }

    @Override
    public Result toResult() {
        return Results.badRequest("bad argument");
    }

    public static CompletionException completionException() {
        return CustomException.completionException(new BadArgumentException());
    }
}
