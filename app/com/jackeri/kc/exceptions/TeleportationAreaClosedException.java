package com.jackeri.kc.exceptions;

import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletionException;

public class TeleportationAreaClosedException extends CustomException {

    public TeleportationAreaClosedException() {
        super("teleportation area closed");
    }

    @Override
    public Result toResult() {
        return Results.badRequest("teleportation area closed");
    }

    public static CompletionException completionException() {
        return CustomException.completionException(new TeleportationAreaClosedException());
    }
}
