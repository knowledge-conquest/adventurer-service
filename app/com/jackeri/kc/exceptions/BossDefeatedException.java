package com.jackeri.kc.exceptions;

import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletionException;

public class BossDefeatedException extends CustomException {

    public BossDefeatedException() {
        super("boss already defeated");
    }

    @Override
    public Result toResult() {
        return Results.badRequest("boss already defeated");
    }

    public static CompletionException completionException() {
        return CustomException.completionException(new BossDefeatedException());
    }
}
