package com.jackeri.kc.exceptions;

import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletionException;

public class NoPathException extends CustomException {

    public NoPathException() {
        super("no path");
    }

    @Override
    public Result toResult() {
        return Results.badRequest("no path");
    }

    public static CompletionException completionException() {
        return CustomException.completionException(new NoPathException());
    }
}
