package com.jackeri.kc.exceptions;

import com.jackeri.kc.models.items.Item;
import lombok.AllArgsConstructor;
import lombok.Getter;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Results;

import java.util.Vector;
import java.util.concurrent.CompletionException;

public class MissingItemsException extends CustomException {

    private final Response response;

    public MissingItemsException(Vector<Item> items) {
        super("missing items");
        response = new Response(items);
    }

    @Override
    public Result toResult() {
        return Results.badRequest(Json.toJson(response));
    }

    public static CompletionException completionException(Vector<Item> items) {
        return CustomException.completionException(new MissingItemsException(items));
    }

    @AllArgsConstructor
    private static class Response {

        @Getter
        private final Vector<Item> missingItems;
    }
}
