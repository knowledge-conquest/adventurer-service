package com.jackeri.kc.exceptions;

import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletionException;

public class BadRequestException extends CustomException {

    public BadRequestException() {
        super("bad request");
    }

    @Override
    public Result toResult() {
        return Results.badRequest();
    }

    public static CompletionException completionException() {
        return CustomException.completionException(new BadRequestException());
    }
}
