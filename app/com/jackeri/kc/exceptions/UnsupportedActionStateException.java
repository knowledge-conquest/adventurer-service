package com.jackeri.kc.exceptions;

import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletionException;

public class UnsupportedActionStateException extends CustomException {

    public UnsupportedActionStateException() {
        super("unsupported action type");
    }

    @Override
    public Result toResult() {
        return Results.badRequest("unsupported action type");
    }

    public static CompletionException completionException() {
        return CustomException.completionException(new UnsupportedActionStateException());
    }
}
