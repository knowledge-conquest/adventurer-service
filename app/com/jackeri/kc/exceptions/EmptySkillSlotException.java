package com.jackeri.kc.exceptions;

import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletionException;

public class EmptySkillSlotException extends CustomException {

    public EmptySkillSlotException() {
        super("empty skill slot");
    }

    @Override
    public Result toResult() {
        return Results.badRequest("empty skill slot");
    }

    public static CompletionException completionException() {
        return CustomException.completionException(new EmptySkillSlotException());
    }
}
