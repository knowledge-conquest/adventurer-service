package com.jackeri.kc.exceptions;

import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletionException;

public class UnauthorizedException extends CustomException {

    public UnauthorizedException() {
        super("Unauthorized");
    }

    @Override
    public Result toResult() {
        return Results.unauthorized();
    }

    public static CompletionException completionException() {
        return CustomException.completionException(new UnauthorizedException());
    }
}
