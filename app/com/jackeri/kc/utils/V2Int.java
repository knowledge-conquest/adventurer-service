package com.jackeri.kc.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jackeri.kc.generation.map.Movement;
import lombok.Getter;
import lombok.Setter;

import java.util.Random;

public class V2Int {

    private final static Random random = new Random();

    @Getter @Setter
    private int x;

    @Getter @Setter
    private int y;

    public V2Int(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public V2Int copy() {
        return new V2Int(x, y);
    }

    public void move(Movement direction) {
        x += direction.getX();
        y += direction.getY();
    }

    public V2Int getMoved(Movement direction) {
        return new V2Int(x + direction.getX(), y + direction.getY());
    }

    public static V2Int random(int minX, int maxX, int minY, int maxY) {
        return new V2Int(
                random.nextInt(maxX - minX) + minX,
                random.nextInt(maxY - minY) + minY
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        V2Int v2Int = (V2Int) o;

        if (x != v2Int.x) return false;
        return y == v2Int.y;
    }

    @Override
    public int hashCode() {
        return 31 * x + y;
    }

    public static int toIndex(V2Int position, int width) {
        return position.x + position.y * width;
    }

    public static V2Int fromIndex(int index, int width) {
        return new V2Int(index % width, index / width);
    }
}
