package com.jackeri.kc.utils.exceptions;

import com.jackeri.kc.exceptions.UnauthorizedException;

public class NoUserIdException extends UnauthorizedException {}
