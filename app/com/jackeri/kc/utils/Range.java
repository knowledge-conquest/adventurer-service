package com.jackeri.kc.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class Range<T extends Number> {

    @Getter @Setter
    private T min;

    @Getter @Setter
    private T max;

    public static Range<Long> generateAttributeRange(long value, long bonus) {
        value += (long) ((double) bonus / 100.0 * (double) value);
        return new Range<>(
                (long) ((double) value * 0.9),
                (long) ((double) value * 1.1)
        );
    }
}
