package com.jackeri.kc.utils;

import com.jackeri.kc.services.jwt.VerifiedJWT;
import play.libs.typedmap.TypedKey;

public class Attrs {
    public static final TypedKey<VerifiedJWT> VERIFIED_JWT = TypedKey.create("VerifiedJWT");
}
