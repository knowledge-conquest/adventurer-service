package com.jackeri.kc.utils;

import java.util.Collections;
import java.util.Vector;

public class Random {

    private static java.util.Random random = new java.util.Random();

    public static void update() {
        random = new java.util.Random(1234);
    }

    public static java.util.Random get() {
        return random;
    }

    public static long getLong(Range<Long> range) {
        return getLong(range.getMin(), range.getMax());
    }

    public static long getLong(long min, long max) {
        return (Math.abs(random.nextLong()) % (max - min + 1)) + min;
    }

    public static Range<Long> getLongRange(long min, long max) {
        long a = getLong(min, max);
        return new Range<>(a, getLong(a, max));
    }

    public static int getInt(int max) {
        return random.nextInt(max);
    }

    public static boolean getBoolean(int percentage) {
        return getInt(100) < percentage;
    }

    public static Vector<Integer> getRandomIndexes(int max) {
        Vector<Integer> indexes = new Vector<>(max);
        for (int i = 0; i < max; i += 1) {
            indexes.add(i);
        }
        Collections.shuffle(indexes, get());
        return indexes;
    }
}
