package com.jackeri.kc.controllers;

import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.recipes.Recipe;
import com.jackeri.kc.services.ControllerHelper;
import com.jackeri.kc.services.CraftingService;
import com.jackeri.kc.services.jwt.JWTRequired;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class CraftingController extends Controller {

    private final CraftingService craftingService;

    @Inject
    public CraftingController(ControllerHelper controllerHelper,
                              CraftingService craftingService) {
        super(controllerHelper);
        this.craftingService = craftingService;
    }

    @JWTRequired
    public CompletableFuture<Result> fetchAreaRecipesForAdventurer
            (Http.Request request, String key, int offset, int count) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> craftingService.fetchAreaRecipesForAdventurer(key, offset, count))
                .thenApply(getControllerHelper()::toResponses);
    }

    @JWTRequired
    public CompletableFuture<Result> craft(Http.Request request, String key, String recipe, int count) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> craftingService.craft(key, recipe, count))
                .thenApply(getControllerHelper()::toResponses);
    }

    @JWTRequired
    public CompletableFuture<Result> adventurerIngredients(Http.Request request, String key, String recipe) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> craftingService.adventurerIngredients(key, recipe))
                .thenApply(getControllerHelper()::toResponses);
    }
}
