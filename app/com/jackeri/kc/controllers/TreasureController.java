package com.jackeri.kc.controllers;

import com.jackeri.kc.services.ControllerHelper;
import com.jackeri.kc.services.TreasureService;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.services.jwt.JWTRequired;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class TreasureController extends Controller {

    private final AdventurerService adventurerService;
    private final TreasureService treasureService;

    @Inject
    public TreasureController(ControllerHelper controllerHelper,
                              AdventurerService adventurerService,
                              TreasureService treasureService) {
        super(controllerHelper);
        this.adventurerService = adventurerService;
        this.treasureService = treasureService;
    }

    @JWTRequired
    public CompletableFuture<Result> fetchLootTreasureArea(Http.Request request, String key) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> treasureService.getLootTreasureAreaForAdventurer(key))
                .thenApply(area -> ok(Json.toJson(area.toTreasureResponse())));
    }

    @JWTRequired
    public CompletableFuture<Result> fetchSkillTreasureArea(Http.Request request, String key) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> treasureService.getSkillTreasureAreaForAdventurer(key))
                .thenApply(area -> ok(Json.toJson(area.toTreasureResponse())));
    }

    @JWTRequired
    public CompletableFuture<Result> openLootTreasure(Http.Request request, String key) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> adventurerService.idleGuard(key))
                .thenCompose(ignoredVoid -> treasureService.openLootTreasure(key))
                .thenApply(getControllerHelper()::toResponses);
    }

    @JWTRequired
    public CompletableFuture<Result> openSkillTreasure(Http.Request request, String key) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> adventurerService.idleGuard(key))
                .thenCompose(ignoredVoid -> treasureService.openSkillTreasure(key))
                .thenApply(getControllerHelper()::toResponse);
    }
}
