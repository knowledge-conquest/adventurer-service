package com.jackeri.kc.controllers;

import com.jackeri.kc.exceptions.UnauthorizedException;
import com.jackeri.kc.generation.RealmGenerator;
import com.jackeri.kc.services.ControllerHelper;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.services.jwt.JWTRequired;
import com.jackeri.kc.services.realms.RealmService;
import com.jackeri.kc.utils.exceptions.NoUserIdException;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class AdventurersController extends Controller {

    private final AdventurerService adventurerService;
    private final RealmGenerator realmGenerator;

    @Inject
    public AdventurersController(ControllerHelper controllerHelper,
                                 AdventurerService adventurerService,
                                 RealmGenerator realmGenerator) {
        super(controllerHelper);
        this.adventurerService = adventurerService;
        this.realmGenerator = realmGenerator;
    }

    @JWTRequired
    public CompletableFuture<Result> createAdventurer(Http.Request request, String name) throws NoUserIdException {
        long userId = getControllerHelper().extractUserIDFrom(request);
        return adventurerService.create(name, userId)
                .thenCompose(adventurer -> realmGenerator.createRealm(adventurer.getKey())
                        .thenApply(ignored -> getControllerHelper().toResponse(adventurer)));
    }

    @JWTRequired
    public CompletableFuture<Result> getAdventurer(Http.Request request, String adventurerKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> adventurerService.getAdventurer(adventurerKey))
                .thenApply(getControllerHelper()::toResponse);
    }

    @JWTRequired
    public CompletableFuture<Result> getWeaponProficiencies(Http.Request request, String adventurerKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> adventurerService.getWeaponProficiencies(adventurerKey))
                .thenApply(getControllerHelper()::toResponses);
    }

    @JWTRequired
    public CompletableFuture<Result> getLocation(Http.Request request, String key) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> adventurerService.getLocation(key))
                .thenApplyAsync(getControllerHelper()::toResponse);
    }

    @JWTRequired
    public CompletableFuture<Result> getAdventurersOf(Http.Request request, long userId) throws NoUserIdException {
        long id = getControllerHelper().extractUserIDFrom(request);
        if (id != userId) throw UnauthorizedException.completionException();

        return adventurerService.getUserAdventurers(userId)
                .thenApplyAsync(getControllerHelper()::toResponses);
    }

    @JWTRequired
    public CompletableFuture<Result> getAdventurerState(Http.Request request, String key) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> adventurerService.getState(key))
                .thenApplyAsync(getControllerHelper()::toJson);
    }
}
