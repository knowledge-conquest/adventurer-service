package com.jackeri.kc.controllers;

import com.jackeri.kc.exceptions.BadArgumentException;
import com.jackeri.kc.exceptions.NotFoundException;
import com.jackeri.kc.exceptions.UnauthorizedException;
import com.jackeri.kc.generation.RealmGenerator;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.services.ControllerHelper;
import com.jackeri.kc.services.TravelService;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.services.jwt.JWTRequired;
import com.jackeri.kc.utils.Attrs;
import com.jackeri.kc.utils.exceptions.NoUserIdException;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletableFuture;

public class TravelController extends Controller {

    private final TravelService travelService;
    private final RealmGenerator realmGenerator;
    private final AdventurerService adventurerService;

    @Inject
    public TravelController(ControllerHelper controllerHelper,
                            TravelService travelService,
                            RealmGenerator realmGenerator,
                            AdventurerService adventurerService) {
        super(controllerHelper);
        this.travelService = travelService;
        this.realmGenerator = realmGenerator;
        this.adventurerService = adventurerService;
    }

    @JWTRequired
    public CompletableFuture<Result> travel(Http.Request request, String adventurerKey, String destinationKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> adventurerService.idleGuard(adventurerKey))
                .thenCompose(ignoredVoid -> travelService.adventurerTravelTo(adventurerKey, destinationKey))
                .thenApply(getControllerHelper()::toResponse);
    }

    @JWTRequired
    public CompletableFuture<Result> getMap(Http.Request request, String adventurerKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> adventurerService.getAdventurer(adventurerKey))
                .thenCompose(adventurer -> {
                    if (adventurer.getLocation() == null)
                        throw NotFoundException.completionException();

                    return travelService.getAdventurerRegionMap(adventurerKey)
                            .thenApply(map -> ok(map.toJson()));
                });
    }

    @JWTRequired
    public CompletableFuture<Result> getRealmMap(Http.Request request, String adventurerKey, int radius) {
        if (radius < 0) throw BadArgumentException.completionException();

        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> travelService.getAdventurerRealmMap(adventurerKey, radius))
                .thenApply(getControllerHelper()::toResponses);
    }

    @JWTRequired
    public CompletableFuture<Result> useTeleportationArray(Http.Request request, String adventurerKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> adventurerService.idleGuard(adventurerKey))
                .thenCompose(ignoredVoid -> travelService.adventurerTeleport(adventurerKey))
                .thenApply(getControllerHelper()::toJson);
    }
}
