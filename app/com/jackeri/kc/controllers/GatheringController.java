package com.jackeri.kc.controllers;

import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.services.ControllerHelper;
import com.jackeri.kc.services.GatheringService;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.services.jwt.JWTRequired;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class GatheringController extends Controller {

    private final AdventurerService adventurerService;
    private final GatheringService gatheringService;

    @Inject
    public GatheringController(ControllerHelper controllerHelper,
                               AdventurerService adventurerService,
                               GatheringService gatheringService) {
        super(controllerHelper);
        this.adventurerService = adventurerService;
        this.gatheringService = gatheringService;
    }

    @JWTRequired
    public CompletableFuture<Result> fetchGatheringArea(Http.Request request, String key) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> gatheringService.getGatheringAreaForAdventurer(key))
                .thenApply(area -> ok(Json.toJson(area.toGatheringResponse())));
    }

    @JWTRequired
    public CompletableFuture<Result> gather(Http.Request request, String key) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> adventurerService.idleGuard(key))
                .thenCompose(ignoredVoid -> gatheringService.gather(key))
                .thenApply(getControllerHelper()::toResponses);
    }
}
