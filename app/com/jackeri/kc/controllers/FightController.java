package com.jackeri.kc.controllers;

import com.jackeri.kc.exceptions.BossDefeatedException;
import com.jackeri.kc.exceptions.EntityBusyException;
import com.jackeri.kc.exceptions.WrongAreaException;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.areas.monster.MonsterBossArea;
import com.jackeri.kc.models.entities.Adventurer;
import com.jackeri.kc.models.entities.Entity;
import com.jackeri.kc.models.skills.Skill;
import com.jackeri.kc.repositories.entities.EntityRepository;
import com.jackeri.kc.services.ControllerHelper;
import com.jackeri.kc.services.FightService;
import com.jackeri.kc.services.FutureService;
import com.jackeri.kc.services.areas.MonsterSpawnerAreaService;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.services.jwt.JWTRequired;
import com.jackeri.kc.utils.Attrs;
import com.jackeri.kc.utils.exceptions.NoUserIdException;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletableFuture;

public class FightController extends Controller {

    private final AdventurerService adventurerService;
    private final EntityRepository entityRepository;
    private final MonsterSpawnerAreaService monsterSpawnerAreaService;
    private final FightService fightService;

    @Inject
    public FightController(ControllerHelper controllerHelper,
                           AdventurerService adventurerService,
                           EntityRepository entityRepository,
                           MonsterSpawnerAreaService monsterSpawnerAreaService,
                           FightService fightService) {
        super(controllerHelper);
        this.adventurerService = adventurerService;
        this.entityRepository = entityRepository;
        this.monsterSpawnerAreaService = monsterSpawnerAreaService;
        this.fightService = fightService;
    }

    /**
     * Get the current state of the fight of an adventurer.
     *
     * @param request The request object used to retrieve the user id.
     * @param key     The key of the adventurer.
     * @return A json response with the fight data.
     */
    @JWTRequired
    public CompletableFuture<Result> adventurerFight(Http.Request request, String key) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> fightService.getAdventurerFight(key))
                .thenApply(getControllerHelper()::toResponse);
    }

    @JWTRequired
    public CompletableFuture<Result> adventurerAttack
            (Http.Request request, String key, String target, int skillSlotIndex) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> fightService.getAdventurerFight(key))
                .thenCompose(fight -> {
                    if (fight == null || !fight.hasEntity(target)) {
                        return CompletableFuture.completedFuture(badRequest());
                    }

                    Adventurer adventurer = (Adventurer) fight.getEntity(key).getDocument();
                    if (!adventurer.isSkillReady(skillSlotIndex)) {
                        return CompletableFuture.completedFuture(badRequest());
                    }

                    return fightService.nextTurn(fight)
                            .thenCompose(ignoredVoid -> fightService
                                    .executeAttack(fight, adventurer, target, adventurer.getSkill(skillSlotIndex)))
                            .thenApply(f -> ok(Json.toJson(f.toResponse())));
                });
    }

    @JWTRequired
    public CompletableFuture<Result> adventurerFlee(Http.Request request, String key) {
        return CompletableFuture.completedFuture(ok(Json.toJson("TODO")));
    }

    @JWTRequired
    public CompletableFuture<Result> startMonsterAreaFight(Http.Request request, String adventurerKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> adventurerService.idleGuard(adventurerKey))
                .thenCompose(ignoredVoid -> monsterSpawnerAreaService.getMonsterAreaForAdventurer(adventurerKey))
                .thenCompose(area -> {
                    if (area == null) throw WrongAreaException.completionException();

                    if (!area.canSpawnMonster()) {
                        throw BossDefeatedException.completionException();
                    }

                    return fightService.startMonsterAreaFight(adventurerKey, area);
                }).thenApply(getControllerHelper()::toResponse);
    }
}
