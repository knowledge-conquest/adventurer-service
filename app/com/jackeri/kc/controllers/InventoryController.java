package com.jackeri.kc.controllers;

import com.google.common.base.Enums;
import com.google.common.base.Optional;
import com.jackeri.kc.exceptions.NotFoundException;
import com.jackeri.kc.exceptions.UnauthorizedException;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.itemdata.EquipmentSlot;
import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.repositories.items.*;
import com.jackeri.kc.services.ControllerHelper;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.services.jwt.JWTRequired;
import com.jackeri.kc.services.skills.SkillService;
import com.jackeri.kc.utils.Attrs;
import com.jackeri.kc.utils.exceptions.NoUserIdException;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class InventoryController extends Controller {

    private final AdventurerService adventurerService;
    private final ResourceRepository resourceRepository;
    private final WeaponRepository weaponRepository;
    private final ArmourRepository armourRepository;
    private final EquipmentRepository equipmentRepository;
    private final SkillService skillService;

    @Inject
    public InventoryController(ControllerHelper controllerHelper,
                               AdventurerService adventurerService,
                               ResourceRepository resourceRepository,
                               WeaponRepository weaponRepository,
                               ArmourRepository armourRepository,
                               EquipmentRepository equipmentRepository,
                               SkillService skillService) {
        super(controllerHelper);
        this.adventurerService = adventurerService;
        this.resourceRepository = resourceRepository;
        this.weaponRepository = weaponRepository;
        this.armourRepository = armourRepository;
        this.equipmentRepository = equipmentRepository;
        this.skillService = skillService;
    }

    @JWTRequired
    public CompletableFuture<Result> getResources(Http.Request request, String key,
                                                  int offset, int count, int sort) {
        return getItems(request, key, offset, count, sort, resourceRepository);
    }

    @JWTRequired
    public CompletableFuture<Result> getWeapons(Http.Request request, String key,
                                                int offset, int count, int sort) {
        return getItems(request, key, offset, count, sort, weaponRepository);
    }

    @JWTRequired
    public CompletableFuture<Result> getArmours(Http.Request request, String key,
                                                int offset, int count, int sort) {
        return getItems(request, key, offset, count, sort, armourRepository);
    }

    @JWTRequired
    public CompletableFuture<Result> getWornWeapon(Http.Request request, String key) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> weaponRepository.fetchWorn(CollectionType.ENTITIES.getId(key)))
                .thenApply(getControllerHelper()::toResponseOrNotFound);
    }

    @JWTRequired
    public CompletableFuture<Result> getWornArmours(Http.Request request, String key) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> armourRepository.fetchWorn(CollectionType.ENTITIES.getId(key)))
                .thenApply(getControllerHelper()::toResponses);
    }

    @JWTRequired
    public CompletableFuture<Result> unEquip(Http.Request request, String key, String slotName) {
        Optional<EquipmentSlot> equipmentSlot = Enums.getIfPresent(EquipmentSlot.class, slotName);

        if (!equipmentSlot.isPresent()) {
            return CompletableFuture.completedFuture(badRequest());
        }

        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> adventurerService.idleGuard(key))
                .thenCompose(ignoredVoid -> equipmentRepository
                        .unEquip(CollectionType.ENTITIES.getId(key), equipmentSlot.get()))
                .thenApply(ignoredVoid -> ok());
    }

    @JWTRequired
    public CompletableFuture<Result> equip(Http.Request request, String key, String equipmentKey, String slotName) {
        Optional<EquipmentSlot> equipmentSlot = Enums.getIfPresent(EquipmentSlot.class, slotName);

        if (!equipmentSlot.isPresent()) {
            return CompletableFuture.completedFuture(badRequest());
        }

        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> adventurerService.idleGuard(key))
                .thenCompose(ignoredVoid -> equipmentRepository
                        .unEquip(CollectionType.ENTITIES.getId(key), equipmentSlot.get()))
                .thenCompose(ignoredVoid -> equipmentRepository.equip(
                        CollectionType.ENTITIES.getId(key),
                        CollectionType.ITEMS.getId(equipmentKey),
                        equipmentSlot.get()))
                .thenApply(ignoredVoid -> ok());
    }

    private <T extends Item> CompletableFuture<Result> getItems
            (Http.Request request, String key, int offset, int count, int sort, ItemRepository<T> itemRepository) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> itemRepository
                        .fetch(CollectionType.ENTITIES.getId(key), offset, count, sort))
                .thenApply(getControllerHelper()::toResponses);
    }

    @JWTRequired
    public CompletableFuture<Result> getSkills(Http.Request request, String key, int offset, int count, int sort) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> skillService.getSkills(key, count, offset, sort))
                .thenApply(getControllerHelper()::toResponses);
    }

    @JWTRequired
    public CompletableFuture<Result> getEquippedSkills(Http.Request request, String key) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> skillService.getEquippedSkills(key))
                .thenApply(getControllerHelper()::toResponses);
    }

    @JWTRequired
    public CompletableFuture<Result> equipSkill(Http.Request request, String key, String skill, int slot) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> adventurerService.idleGuard(key))
                .thenCompose(ignoredVoid -> skillService.equip(key, skill, slot))
                .thenApply(ignoredVoid -> ok());
    }

    @JWTRequired
    public CompletableFuture<Result> unEquipSkill(Http.Request request, String key, int slot) {
        return getControllerHelper().userAuthorizationGuard(request, key)
                .thenCompose(ignoredVoid -> adventurerService.idleGuard(key))
                .thenCompose(ignoredVoid -> skillService.unEquip(key, slot))
                .thenApply(ignoredVoid -> ok());
    }
}
