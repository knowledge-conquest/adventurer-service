package com.jackeri.kc.controllers;

import com.google.common.base.Enums;
import com.google.common.base.Optional;
import com.jackeri.kc.exceptions.NotFoundException;
import com.jackeri.kc.exceptions.UnauthorizedException;
import com.jackeri.kc.exceptions.UnsupportedActionStateException;
import com.jackeri.kc.models.itemdata.WeaponType;
import com.jackeri.kc.models.participations.ActionState;
import com.jackeri.kc.services.ControllerHelper;
import com.jackeri.kc.services.IdleService;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.services.jwt.JWTRequired;
import com.jackeri.kc.utils.Attrs;
import com.jackeri.kc.utils.exceptions.NoUserIdException;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class IdleController extends Controller {

    private final AdventurerService adventurerService;
    private final IdleService idleService;

    @Inject
    public IdleController(ControllerHelper controllerHelper,
                          AdventurerService adventurerService,
                          IdleService idleService) {
        super(controllerHelper);
        this.adventurerService = adventurerService;
        this.idleService = idleService;
    }

    @JWTRequired
    public CompletableFuture<Result> get(Http.Request request, String adventurerKey, String actionStateName) {
        Optional<ActionState> actionState = Enums.getIfPresent(ActionState.class, actionStateName.toUpperCase());

        if (!actionState.isPresent() || !actionState.get().isIdle()) {
            throw UnsupportedActionStateException.completionException();
        }

        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> idleService.get(adventurerKey, actionState.get()))
                .thenApply(getControllerHelper()::toResponse);
    }

    @JWTRequired
    public CompletableFuture<Result> startCultivation
            (Http.Request request, String adventurerKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> idleService.startCultivation(adventurerKey))
                .thenApply(getControllerHelper()::toResponse);
    }

    @JWTRequired
    public CompletableFuture<Result> stopCultivation
            (Http.Request request, String adventurerKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> idleService.stopCultivation(adventurerKey))
                .thenApply(getControllerHelper()::toResponse);
    }

    @JWTRequired
    public CompletableFuture<Result> startSpawnRecovery
            (Http.Request request, String adventurerKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> idleService.startSpawnRecovery(adventurerKey))
                .thenApply(getControllerHelper()::toResponse);
    }

    @JWTRequired
    public CompletableFuture<Result> stopSpawnRecovery
            (Http.Request request, String adventurerKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> idleService.stopSpawnRecovery(adventurerKey))
                .thenApply(getControllerHelper()::toResponse);
    }

    @JWTRequired
    public CompletableFuture<Result> startTraining
            (Http.Request request, String adventurerKey, String weaponTypeName) {
        Optional<WeaponType> weaponType = Enums.getIfPresent(WeaponType.class, weaponTypeName.toUpperCase());
        if (!weaponType.isPresent()) {
            throw NotFoundException.completionException();
        }

        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> idleService.startTraining(adventurerKey, weaponType.get()))
                .thenApply(getControllerHelper()::toResponse);
    }

    @JWTRequired
    public CompletableFuture<Result> stopTraining
            (Http.Request request, String adventurerKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> idleService.stopTraining(adventurerKey))
                .thenApply(getControllerHelper()::toResponse);
    }

    @JWTRequired
    public CompletableFuture<Result> startSkillPractice
            (Http.Request request, String adventurerKey, int slotIndex) {

        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> idleService.startSkillPractice(adventurerKey, slotIndex))
                .thenApply(getControllerHelper()::toResponse);
    }

    @JWTRequired
    public CompletableFuture<Result> stopSkillPractice(Http.Request request, String adventurerKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> idleService.stopSkillPractice(adventurerKey))
                .thenApply(getControllerHelper()::toResponse);
    }
}
