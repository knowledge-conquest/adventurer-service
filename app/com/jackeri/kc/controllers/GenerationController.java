package com.jackeri.kc.controllers;

import com.jackeri.kc.generation.RealmGenerator;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.services.ArangoAsyncService;
import com.jackeri.kc.services.ControllerHelper;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.services.jwt.JWTRequired;
import com.jackeri.kc.utils.Attrs;
import com.jackeri.kc.utils.exceptions.NoUserIdException;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletableFuture;

public class GenerationController extends Controller {

    private final AdventurerService adventurerService;
    private final RealmGenerator realmGenerator;
    private final ArangoAsyncService arango;

    @Inject
    public GenerationController(ControllerHelper controllerHelper,
                                AdventurerService adventurerService,
                                RealmGenerator realmGenerator,
                                ArangoAsyncService arango) {
        super(controllerHelper);
        this.adventurerService = adventurerService;
        this.realmGenerator = realmGenerator;
        this.arango = arango;
    }

    @JWTRequired
    public CompletableFuture<Result> createRealm(Http.Request request, String adventurerKey) {
        return getControllerHelper().userAuthorizationGuard(request, adventurerKey)
                .thenCompose(ignoredVoid -> adventurerService.leaveRealm(adventurerKey))
                .thenCompose(ignoredVoid -> realmGenerator.createRealm(adventurerKey))
                .thenApply(getControllerHelper()::toJson);
    }

    public CompletableFuture<Result> truncate() {
        CompletableFuture<Void> last = null;
        for (CollectionType collection : CollectionType.values()) {
            CompletableFuture<Void> current = arango.getDatabase()
                    .collection(collection.getName())
                    .truncate()
                    .thenApply(ignored -> {
                        System.out.println(collection.getName() + " Truncated");
                        return null;
                    });

            if (last == null) {
                last = current;
            } else {
                last.thenCompose(ignoredVoid -> current);
            }
        }

        return last != null ?
                last.thenApply(ignoredVoid -> ok()) :
                CompletableFuture.completedFuture(badRequest());
    }
}
