package com.jackeri.kc.controllers;

import com.jackeri.kc.services.ControllerHelper;
import lombok.Getter;

public abstract class Controller extends play.mvc.Controller {

    @Getter
    private final ControllerHelper controllerHelper;

    public Controller(ControllerHelper controllerHelper) {
        this.controllerHelper = controllerHelper;
    }
}
