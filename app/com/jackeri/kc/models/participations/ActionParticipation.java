package com.jackeri.kc.models.participations;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.Edge;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.entities.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public abstract class ActionParticipation extends Edge {

    @Getter @Setter
    @JsonProperty("_from")
    private Reference<Entity> entity;

    public ActionParticipation(String entityId) {
        this.entity = new Reference<>(entityId);
    }

    @JsonGetter
    public abstract ActionState getActionState();

    /* Serialization helpers fro API responses */

    @Override
    public ActionParticipationResponse toResponse() {
        return new ActionParticipationResponse();
    }

    public class ActionParticipationResponse extends EdgeResponse {

        @Getter
        private String entity;

        @Getter
        private ActionState actionState;

        protected ActionParticipationResponse() {
            entity = ActionParticipation.this.getEntity().getKey();
            actionState = ActionParticipation.this.getActionState();
        }
    }
}
