package com.jackeri.kc.models.participations;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum ActionState {
    FIGHT(false),
    CULTIVATION(true),
    TRAINING(true),
    SKILL_PRACTICE(true),
    SPAWN_RECOVERY(true),
    IDLE(false),
    DEAD(false),
    NO_REALM(false);

    @Getter
    private final boolean idle;
}
