package com.jackeri.kc.models.participations;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.fights.Fight;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class FightParticipation extends ActionParticipation {

    @Getter @Setter
    @JsonProperty("_to")
    private Reference<Fight> fight;

    @Getter @Setter
    private boolean attacker;

    public FightParticipation(String entityId, String fightId, boolean isAttacker) {
        super(entityId);
        this.fight = new Reference<>(fightId);
        this.attacker = isAttacker;
    }

    @Override
    public ActionState getActionState() {
        return ActionState.FIGHT;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.FIGHT_PARTICIPATION;
    }

    /* Serialization helpers fro API responses */

    @Override
    public FightParticipationResponse toResponse() {
        return new FightParticipationResponse();
    }

    public class FightParticipationResponse extends ActionParticipationResponse {

        @Getter
        private String fight;

        @Getter
        private boolean attacker;

        protected FightParticipationResponse() {
            fight = FightParticipation.this.getFight().getKey();
            attacker = FightParticipation.this.isAttacker();
        }
    }
}
