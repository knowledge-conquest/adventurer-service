package com.jackeri.kc.models.participations.idle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.areas.SpawnArea;
import com.jackeri.kc.models.participations.ActionState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class SpawnRecoveryParticipation extends IdleActionParticipation {

    @Getter @Setter
    @JsonProperty("_to")
    private Reference<SpawnArea> area;

    public SpawnRecoveryParticipation(String entityId, String area) {
        super(entityId);
        this.area = new Reference<>(area);
    }

    @Override
    public ActionState getActionState() {
        return ActionState.SPAWN_RECOVERY;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.SPAWN_RECOVERY_PARTICIPATION;
    }

    /* Serialization helpers fro API responses */

    @Override
    public SpawnRecoveryParticipationResponse toResponse() {
        return new SpawnRecoveryParticipationResponse();
    }

    public class SpawnRecoveryParticipationResponse extends IdleActionParticipationResponse {

        @Getter
        private final SpawnArea.SpawnAreaResponse area;

        protected SpawnRecoveryParticipationResponse() {
            Reference<SpawnArea> areaReference = SpawnRecoveryParticipation.this.getArea();
            area = areaReference.isPopulated() ? areaReference.getDocument().toResponse() : null;
        }
    }
}
