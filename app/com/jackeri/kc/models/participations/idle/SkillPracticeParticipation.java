package com.jackeri.kc.models.participations.idle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.areas.SkillPracticeArea;
import com.jackeri.kc.models.areas.TrainingArea;
import com.jackeri.kc.models.itemdata.WeaponType;
import com.jackeri.kc.models.participations.ActionState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class SkillPracticeParticipation extends IdleActionParticipation {

    @Getter @Setter
    @JsonProperty("_to")
    private Reference<SkillPracticeArea> area;

    @Getter @Setter
    private int slotIndex;

    public SkillPracticeParticipation(String entityId, String area, int slotIndex) {
        super(entityId);
        this.area = new Reference<>(area);
        this.slotIndex = slotIndex;
    }

    @Override
    public ActionState getActionState() {
        return ActionState.SKILL_PRACTICE;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.SKILL_PRACTICE_PARTICIPATION;
    }

    /* Serialization helpers fro API responses */

    @Override
    public SkillPracticeParticipationResponse toResponse() {
        return new SkillPracticeParticipationResponse();
    }

    public class SkillPracticeParticipationResponse extends IdleActionParticipationResponse {

        @Getter
        private final SkillPracticeArea.SkillPracticeAreaResponse area;

        @Getter
        private final int slotIndex;

        protected SkillPracticeParticipationResponse() {
            slotIndex = SkillPracticeParticipation.this.getSlotIndex();

            Reference<SkillPracticeArea> areaReference = SkillPracticeParticipation.this.getArea();
            area = areaReference.isPopulated() ? areaReference.getDocument().toResponse() : null;
        }
    }
}
