package com.jackeri.kc.models.participations.idle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.areas.Area;
import com.jackeri.kc.models.areas.CultivationArea;
import com.jackeri.kc.models.participations.ActionState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class CultivationParticipation extends IdleActionParticipation {

    @Getter @Setter
    @JsonProperty("_to")
    private Reference<CultivationArea> area;

    public CultivationParticipation(String entityId, String area) {
        super(entityId);
        this.area = new Reference<>(area);
    }

    @Override
    public ActionState getActionState() {
        return ActionState.CULTIVATION;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.CULTIVATION_PARTICIPATION;
    }

    /* Serialization helpers fro API responses */

    @Override
    public CultivationParticipationResponse toResponse() {
        return new CultivationParticipationResponse();
    }

    public class CultivationParticipationResponse extends IdleActionParticipationResponse {

        @Getter
        private final Area.AreaResponse area;

        protected CultivationParticipationResponse() {
            Reference<CultivationArea> areaReference = CultivationParticipation.this.getArea();
            area = areaReference.isPopulated() ? areaReference.getDocument().toResponse() : null;
        }
    }
}
