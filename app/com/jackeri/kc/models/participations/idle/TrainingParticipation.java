package com.jackeri.kc.models.participations.idle;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.areas.TrainingArea;
import com.jackeri.kc.models.itemdata.WeaponType;
import com.jackeri.kc.models.participations.ActionState;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class TrainingParticipation extends IdleActionParticipation {

    @Getter @Setter
    @JsonProperty("_to")
    private Reference<TrainingArea> area;

    @Getter @Setter
    private WeaponType weaponType;

    public TrainingParticipation(String entityId, String area, WeaponType weaponType) {
        super(entityId);
        this.area = new Reference<>(area);
        this.weaponType = weaponType;
    }

    @Override
    public ActionState getActionState() {
        return ActionState.TRAINING;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.TRAINING_PARTICIPATION;
    }

    /* Serialization helpers fro API responses */

    @Override
    public TrainingParticipationResponse toResponse() {
        return new TrainingParticipationResponse();
    }

    public class TrainingParticipationResponse extends IdleActionParticipationResponse {

        @Getter
        private final TrainingArea.TrainingAreaResponse area;

        @Getter
        private final WeaponType weaponType;

        protected TrainingParticipationResponse() {
            weaponType = TrainingParticipation.this.getWeaponType();

            Reference<TrainingArea> areaReference = TrainingParticipation.this.getArea();
            area = areaReference.isPopulated() ? areaReference.getDocument().toResponse() : null;
        }
    }
}
