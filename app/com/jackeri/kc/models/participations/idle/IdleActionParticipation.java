package com.jackeri.kc.models.participations.idle;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.jackeri.kc.models.participations.ActionParticipation;
import com.jackeri.kc.models.participations.idle.reward.IdleReward;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
public abstract class IdleActionParticipation extends ActionParticipation {

    @Getter @Setter
    private Date startedOn;

    @JsonIgnore
    private long duration;

    @JsonIgnore
    private IdleReward reward;

    public IdleActionParticipation(String entityId) {
        super(entityId);
    }

    @JsonIgnore
    public long getDuration() {
        return duration;
    }

    @JsonSetter
    public void setDuration(long duration) {
        this.duration = duration;
    }

    @JsonIgnore
    public IdleReward getReward() {
        return reward;
    }

    @JsonIgnore
    public void setReward(IdleReward reward) {
        this.reward = reward;
    }

    /* Serialization helpers fro API responses */

    @Override
    public IdleActionParticipationResponse toResponse() {
        return new IdleActionParticipationResponse();
    }

    public class IdleActionParticipationResponse extends ActionParticipationResponse {

        @Getter
        private final Date startedOn;

        @Getter
        private final long duration;

        @Getter
        private final IdleReward reward;

        protected IdleActionParticipationResponse() {
            startedOn = IdleActionParticipation.this.getStartedOn();
            duration = IdleActionParticipation.this.getDuration();
            reward = IdleActionParticipation.this.getReward();
        }
    }
}
