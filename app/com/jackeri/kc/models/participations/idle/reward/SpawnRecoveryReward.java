package com.jackeri.kc.models.participations.idle.reward;

import com.jackeri.kc.models.utils.calculators.AttributeCalculator;
import lombok.Getter;

public class SpawnRecoveryReward extends IdleReward {

    @Getter
    private final long level;

    @Getter
    private final long health;

    public SpawnRecoveryReward(long level, long duration) {
        this.level = level;
        this.health = AttributeCalculator.calculateSpawnRecoveryHealth(level, duration);
    }
}
