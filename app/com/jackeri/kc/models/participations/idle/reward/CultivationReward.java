package com.jackeri.kc.models.participations.idle.reward;

import com.jackeri.kc.models.utils.calculators.AttributeCalculator;
import lombok.Getter;

public class CultivationReward extends IdleReward {

    @Getter
    private final long level;

    @Getter
    private final long experience;

    public CultivationReward(long level, long duration) {
        this.level = level;
        this.experience = AttributeCalculator.calculateCultivationExperience(level, duration);
    }
}
