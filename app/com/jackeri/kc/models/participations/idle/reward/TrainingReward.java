package com.jackeri.kc.models.participations.idle.reward;

import com.jackeri.kc.models.utils.calculators.AttributeCalculator;
import lombok.Getter;

public class TrainingReward extends IdleReward {

    @Getter
    private final long level;

    @Getter
    private final double weaponProficiency;

    public TrainingReward(long level, long duration) {
        this.level = level;
        this.weaponProficiency = AttributeCalculator.calculateTrainingProficiency(level, duration);
    }
}
