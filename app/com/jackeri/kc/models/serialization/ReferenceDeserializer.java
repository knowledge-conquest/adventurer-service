package com.jackeri.kc.models.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.Reference;
import play.libs.Json;

import java.io.IOException;

/**
 * Custom deserializer for ArangoDB references.
 * @param <T> The type of the referenced document.
 */
public class ReferenceDeserializer<T extends Document> extends JsonDeserializer<Reference<T>> {

    @Override
    public Reference<T> deserialize(JsonParser parser, DeserializationContext context) throws IOException {

        JsonNode json = parser.getCodec().readTree(parser);

        if (json.isTextual()) return new Reference<>(json.asText());

        Document document = Json.fromJson(json, Document.class);
        return document != null ? new Reference<>((T) document) : null;
    }
}
