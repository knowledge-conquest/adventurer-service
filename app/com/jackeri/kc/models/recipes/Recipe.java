package com.jackeri.kc.models.recipes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.itemdata.ItemData;
import com.jackeri.kc.models.itemdata.edges.RecipeComponent;
import com.jackeri.kc.models.realms.Realm;
import com.jackeri.kc.utils.Pair;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
public class Recipe extends Document {

    @Getter
    @Setter
    private long level;

    @Getter
    @Setter
    private Reference<Realm> realm;

    @JsonIgnore
    private Vector<Pair<ItemData, Long>> ingredients;

    @JsonIgnore
    private Vector<Pair<ItemData, Long>> results;

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.RECIPE;
    }

    public Recipe(long level, String realm) {
        this.level = level;
        this.realm = new Reference<>(realm);
        this.ingredients = new Vector<>();
        this.results = new Vector<>();
    }

    @JsonIgnore
    public Vector<Pair<ItemData, Long>> getIngredients() {
        return ingredients;
    }

    @JsonSetter
    public void setIngredients(Vector<Pair<ItemData, Long>> ingredients) {
        this.ingredients = ingredients;
    }

    @JsonIgnore
    public Vector<Pair<ItemData, Long>> getResults() {
        return results;
    }

    @JsonSetter
    public void setResults(Vector<Pair<ItemData, Long>> results) {
        this.results = results;
    }

    public Vector<RecipeComponent> extractRecipeComponents() {
        Vector<RecipeComponent> components = new Vector<>();
        for (Pair<ItemData, Long> pair : getIngredients()) {
            components.add(new RecipeComponent(
                    getId(), pair.getA().getId(), pair.getB(), RecipeComponent.ComponentType.INGREDIENT));
        }
        for (Pair<ItemData, Long> pair : getResults()) {
            components.add(new RecipeComponent(
                    getId(), pair.getA().getId(), pair.getB(), RecipeComponent.ComponentType.RESULT));
        }
        return components;
    }

    @Override
    public RecipeResponse toResponse() {
        return new RecipeResponse();
    }

    public class RecipeResponse extends DocumentResponse {
        @Getter
        private final long level;

        @Getter
        private final Vector<Pair<ItemData.ItemDataResponse, Long>> ingredients;

        @Getter
        private final Vector<Pair<ItemData.ItemDataResponse, Long>> results;

        @Getter
        private final String realm;

        public RecipeResponse() {
            this.level = Recipe.this.getLevel();
            this.ingredients = toResponses(Recipe.this.getIngredients());
            this.results = toResponses(Recipe.this.getResults());
            this.realm = Recipe.this.getRealm().getKey();
        }

        private Vector<Pair<ItemData.ItemDataResponse, Long>> toResponses(Vector<Pair<ItemData, Long>> pairs) {
            Vector<Pair<ItemData.ItemDataResponse, Long>> responses = new Vector<>();
            for (Pair<ItemData, Long> pair : pairs) {
                responses.add(new Pair<>(pair.getA().toResponse(), pair.getB()));
            }
            return responses;
        }
    }
}
