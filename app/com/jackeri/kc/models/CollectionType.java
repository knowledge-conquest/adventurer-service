package com.jackeri.kc.models;

import lombok.Getter;

public enum CollectionType {
    ENTITIES("Entities"),
    WEAPON_PROFICIENCIES("WeaponProficiencies"),
    MONSTER_DATA("MonsterData"),

    ITEM_DATA("ItemData"),
    ITEM_DATA_EDGES("ItemDataEdges"),
    ITEMS("Items"),
    ITEM_OWNERSHIPS("ItemOwnerships"),
    WORN_EQUIPMENTS("WornEquipments"),

    SKILLS("Skills"),
    SKILL_OWNERSHIPS("SkillOwnerships"),

    REALMS("Realms"),
    REGIONS("Regions"),
    AREAS("Areas"),
    AREA_PATHS("AreaPaths"),
    BOSS_TELEPORTATIONS("BossTeleportations"),

    ACTION_PARTICIPATION("ActionParticipation"),

    FIGHTS("Fights"),

    RESOURCE_TYPES("ResourceTypes"),
    WEAPON_TYPES("WeaponTypes"),
    EQUIPMENT_SLOTS("EquipmentSlots"),

    RECIPES("Recipes");

    @Getter
    private final String name;

    CollectionType(String name) {
        this.name = name;
    }

    public String getId(String key) {
        return getName() + "/" + key;
    }
}
