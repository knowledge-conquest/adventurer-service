package com.jackeri.kc.models;

import com.fasterxml.jackson.annotation.*;
import com.jackeri.kc.models.areas.*;
import com.jackeri.kc.models.areas.monster.MonsterBossArea;
import com.jackeri.kc.models.areas.monster.MonsterSpawnerArea;
import com.jackeri.kc.models.areas.treasure.LootTreasureArea;
import com.jackeri.kc.models.areas.treasure.SkillTreasureArea;
import com.jackeri.kc.models.areas.treasure.TreasureArea;
import com.jackeri.kc.models.entities.Adventurer;
import com.jackeri.kc.models.entities.Monster;
import com.jackeri.kc.models.entities.MonsterData;
import com.jackeri.kc.models.entities.WeaponProficiency;
import com.jackeri.kc.models.fights.Fight;
import com.jackeri.kc.models.fights.FightEnded;
import com.jackeri.kc.models.participations.idle.CultivationParticipation;
import com.jackeri.kc.models.participations.FightParticipation;
import com.jackeri.kc.models.itemdata.ArmourData;
import com.jackeri.kc.models.itemdata.ResourceData;
import com.jackeri.kc.models.itemdata.WeaponData;
import com.jackeri.kc.models.itemdata.edges.GatheringLoot;
import com.jackeri.kc.models.itemdata.edges.MonsterLoot;
import com.jackeri.kc.models.itemdata.edges.RecipeComponent;
import com.jackeri.kc.models.items.Armour;
import com.jackeri.kc.models.items.Resource;
import com.jackeri.kc.models.items.Weapon;
import com.jackeri.kc.models.participations.idle.SkillPracticeParticipation;
import com.jackeri.kc.models.participations.idle.SpawnRecoveryParticipation;
import com.jackeri.kc.models.participations.idle.TrainingParticipation;
import com.jackeri.kc.models.realms.Realm;
import com.jackeri.kc.models.recipes.Recipe;
import com.jackeri.kc.models.regions.Region;
import com.jackeri.kc.models.skills.Skill;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A single document of an ArangoDB Collection.
 */
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        // Entities
        @JsonSubTypes.Type(name = "ADVENTURER", value = Adventurer.class),
        @JsonSubTypes.Type(name = "WEAPON_PROFICIENCY", value = WeaponProficiency.class),
        @JsonSubTypes.Type(name = "MONSTER", value = Monster.class),
        @JsonSubTypes.Type(name = "MONSTER_DATA", value = MonsterData.class),
        @JsonSubTypes.Type(name = "MONSTER_LOOT", value = MonsterLoot.class),

        // Items
        @JsonSubTypes.Type(name = "RESOURCE", value = Resource.class),
        @JsonSubTypes.Type(name = "RESOURCE_DATA", value = ResourceData.class),
        @JsonSubTypes.Type(name = "WEAPON", value = Weapon.class),
        @JsonSubTypes.Type(name = "WEAPON_DATA", value = WeaponData.class),
        @JsonSubTypes.Type(name = "ARMOUR", value = Armour.class),
        @JsonSubTypes.Type(name = "ARMOUR_DATA", value = ArmourData.class),

        // Skills
        @JsonSubTypes.Type(name = "SKILL", value = Skill.class),

        // Realms
        @JsonSubTypes.Type(name = "REALM", value = Realm.class),

        // Regions
        @JsonSubTypes.Type(name = "REGION", value = Region.class),

        // Areas
        @JsonSubTypes.Type(name = "SPAWN_AREA", value = SpawnArea.class),
        @JsonSubTypes.Type(name = "CRAFTING_AREA", value = CraftingArea.class),
        @JsonSubTypes.Type(name = "GATHERING_AREA", value = GatheringArea.class),
        @JsonSubTypes.Type(name = "TRAINING_AREA", value = TrainingArea.class),
        @JsonSubTypes.Type(name = "SKILL_PRACTICE_AREA", value = SkillPracticeArea.class),
        @JsonSubTypes.Type(name = "CULTIVATION_AREA", value = CultivationArea.class),
        @JsonSubTypes.Type(name = "LOOT_TREASURE_AREA", value = LootTreasureArea.class),
        @JsonSubTypes.Type(name = "SKILL_TREASURE_AREA", value = SkillTreasureArea.class),
        @JsonSubTypes.Type(name = "TELEPORTATION_AREA", value = TeleportationArea.class),
        @JsonSubTypes.Type(name = "MONSTER_SPAWNER_AREA", value = MonsterSpawnerArea.class),
        @JsonSubTypes.Type(name = "MONSTER_BOSS_AREA", value = MonsterBossArea.class),
        @JsonSubTypes.Type(name = "AREA_PATH", value = AreaPath.class),

        // Fights
        @JsonSubTypes.Type(name = "FIGHT", value = Fight.class),
        @JsonSubTypes.Type(name = "FIGHT_ENDED", value = FightEnded.class),
        @JsonSubTypes.Type(name = "FIGHT_PARTICIPATION", value = FightParticipation.class),

        // Recipes
        @JsonSubTypes.Type(name = "RECIPE", value = Recipe.class),
        @JsonSubTypes.Type(name = "RECIPE_COMPONENT", value = RecipeComponent.class),

        // Gathering
        @JsonSubTypes.Type(name = "GATHERING_LOOT", value = GatheringLoot.class),

        // Treasure
        @JsonSubTypes.Type(name = "TREASURE_LOOT", value = TreasureArea.class),

        // Action Participation
        @JsonSubTypes.Type(name = "CULTIVATION_PARTICIPATION", value = CultivationParticipation.class),
        @JsonSubTypes.Type(name = "TRAINING_PARTICIPATION", value = TrainingParticipation.class),
        @JsonSubTypes.Type(name = "SKILL_PRACTICE_PARTICIPATION", value = SkillPracticeParticipation.class),
        @JsonSubTypes.Type(name = "SPAWN_RECOVERY_PARTICIPATION", value = SpawnRecoveryParticipation.class),
})
public abstract class Document {

    @Getter
    @Setter
    @JsonProperty("_key")
    private String key;

    @Getter
    @Setter
    @JsonProperty("_rev")
    private String revision;

    @JsonGetter("_id")
    public String getId() {
        return key != null ? getCollection().getName() + "/" + key : null;
    }

    @JsonGetter("type")
    public abstract DocumentType getDocumentType();

    @JsonIgnore
    public CollectionType getCollection() {
        return getDocumentType().getCollectionType();
    }

    /* Serialization helpers for API responses */

    public abstract DocumentResponse toResponse();

    public abstract class DocumentResponse {

        @Getter
        private String key;

        @Getter
        private String type;

        protected DocumentResponse() {
            key = Document.this.key;
            type = Document.this.getDocumentType().name();
        }
    }
}
