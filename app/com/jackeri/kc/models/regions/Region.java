package com.jackeri.kc.models.regions;

import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.realms.Realm;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Region extends Document {

    @Getter @Setter
    private Reference<Realm> realm;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private long level;

    @Getter @Setter
    private int width;

    @Getter @Setter
    private int height;

    public Region(String realmId, String name, long level, int width, int height) {
        this.realm = new Reference<>(realmId);
        this.name = name;
        this.level = level;
        this.width = width;
        this.height = height;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.REGION;
    }

    @Override
    public RegionResponse toResponse() {
        return new RegionResponse();
    }

    public class RegionResponse extends DocumentResponse {

        @Getter
        private String realmKey;

        @Getter
        private String name;

        @Getter
        private long level;

        @Getter
        private int width;

        @Getter
        private int height;

        protected RegionResponse() {
            realmKey = Region.this.getRealm().getKey();
            name = Region.this.getName();
            level = Region.this.getLevel();
            width = Region.this.getWidth();
            height = Region.this.getHeight();
        }
    }
}
