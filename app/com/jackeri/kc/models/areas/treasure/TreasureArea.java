package com.jackeri.kc.models.areas.treasure;

import com.jackeri.kc.models.areas.Area;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public abstract class TreasureArea extends Area {

    @Getter @Setter
    private boolean opened;

    public TreasureArea(String regionId) {
        super(regionId);
        this.opened = false;
    }

    public abstract TreasureAreaResponse toTreasureResponse();

    @Override
    public TreasureAreaResponse toResponse() {
        return new TreasureAreaResponse();
    }

    public class TreasureAreaResponse extends AreaResponse {

        @Getter
        private final boolean opened;

        protected TreasureAreaResponse() {
            opened = TreasureArea.this.isOpened();
        }
    }

}
