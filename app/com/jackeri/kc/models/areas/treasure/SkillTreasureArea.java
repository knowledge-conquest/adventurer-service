package com.jackeri.kc.models.areas.treasure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.areas.Area;
import com.jackeri.kc.models.itemdata.ItemData;
import com.jackeri.kc.models.itemdata.edges.TreasureLoot;
import com.jackeri.kc.models.skills.Skill;
import com.jackeri.kc.utils.Pair;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
public class SkillTreasureArea extends TreasureArea {

    @Getter @Setter
    private Skill skill;

    public SkillTreasureArea(String regionId, Skill skill) {
        super(regionId);
        this.skill = skill;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.SKILL_TREASURE_AREA;
    }

    @Override
    public TreasureAreaResponse toResponse() {
        return new SkillTreasureAreaResponse();
    }

    public TreasureAreaResponse toTreasureResponse() {
        return toResponse();
    }

    public class SkillTreasureAreaResponse extends TreasureAreaResponse {

        @Getter
        private final Skill.SkillResponse skill;

        protected SkillTreasureAreaResponse() {
            skill = SkillTreasureArea.this.getSkill().toResponse();
        }
    }
}
