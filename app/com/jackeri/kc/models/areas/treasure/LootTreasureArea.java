package com.jackeri.kc.models.areas.treasure;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.areas.Area;
import com.jackeri.kc.models.itemdata.ItemData;
import com.jackeri.kc.models.itemdata.edges.TreasureLoot;
import com.jackeri.kc.utils.Pair;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
public class LootTreasureArea extends TreasureArea {

    @JsonIgnore
    private Vector<Pair<ItemData, Long>> loots;

    public LootTreasureArea(String regionId) {
        super(regionId);
        this.loots = new Vector<>();
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.LOOT_TREASURE_AREA;
    }

    @JsonIgnore
    public Vector<TreasureLoot> getLootEdges() {
        Vector<TreasureLoot> extract = new Vector<>();
        for (Pair<ItemData, Long> loot : loots) {
            extract.add(new TreasureLoot(loot.getA().getId(), getId(), loot.getB()));
        }
        return extract;
    }

    @JsonIgnore
    public Vector<Pair<ItemData, Long>> getLoots() {
        return loots;
    }

    @JsonSetter
    public void setLoots(Vector<Pair<ItemData, Long>> loots) {
        this.loots = loots;
    }

    @Override
    public LootTreasureAreaResponse toResponse() {
        return new LootTreasureAreaResponse();
    }

    public class LootTreasureAreaResponse extends TreasureAreaResponse {

        @Getter
        private final boolean opened;

        protected LootTreasureAreaResponse() {
            opened = LootTreasureArea.this.isOpened();
        }
    }

    public LootTreasureResponse toTreasureResponse() {
        return new LootTreasureResponse();
    }

    public class LootTreasureResponse extends TreasureAreaResponse {

        @Getter
        private final Vector<Pair<ItemData.ItemDataResponse, Long>> loots;

        protected LootTreasureResponse() {
            loots = new Vector<>();
            for (Pair<ItemData, Long> loot : LootTreasureArea.this.getLoots()) {
                loots.add(new Pair<>(loot.getA().toResponse(), loot.getB()));
            }
        }
    }
}
