package com.jackeri.kc.models.areas.monster;

import com.jackeri.kc.models.DocumentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class MonsterBossArea extends MonsterArea {

    @Getter @Setter
    private boolean defeated;

    public MonsterBossArea(String regionId, String monsterDataId) {
        super(regionId, monsterDataId);
        this.defeated = false;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.MONSTER_BOSS_AREA;
    }

    @Override
    public boolean canSpawnMonster() {
        return !isDefeated();
    }

    @Override
    public MonsterBossAreaResponse toResponse() {
        return new MonsterBossAreaResponse();
    }

    public class MonsterBossAreaResponse extends MonsterAreaResponse {

        @Getter
        private boolean defeated;

        protected MonsterBossAreaResponse() {
            defeated = MonsterBossArea.this.isDefeated();
        }
    }
}
