package com.jackeri.kc.models.areas.monster;

import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.areas.Area;
import com.jackeri.kc.models.entities.MonsterData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public abstract class MonsterArea extends Area {

    @Getter @Setter
    private Reference<MonsterData> monsterData;

    public MonsterArea(String regionId, String monsterDataId) {
        super(regionId);
        this.monsterData = new Reference<>(monsterDataId);
    }

    public abstract boolean canSpawnMonster();

    /* Serialization helpers for API responses */

    public abstract MonsterAreaResponse toResponse();

    public abstract class MonsterAreaResponse extends AreaResponse {

        @Getter
        private MonsterData.MonsterDataResponse monsterData;

        protected MonsterAreaResponse() {
            monsterData = MonsterArea.this.getMonsterData().isPopulated()
                ? MonsterArea.this.getMonsterData().getDocument().toResponse() : null;
        }
    }
}
