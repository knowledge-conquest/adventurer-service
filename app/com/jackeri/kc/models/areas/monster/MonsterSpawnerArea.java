package com.jackeri.kc.models.areas.monster;

import com.jackeri.kc.models.DocumentType;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class MonsterSpawnerArea extends MonsterArea {

    public MonsterSpawnerArea(String regionId, String monsterDataId) {
        super(regionId, monsterDataId);
    }

    @Override
    public boolean canSpawnMonster() {
        return true;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.MONSTER_SPAWNER_AREA;
    }

    @Override
    public MonsterSpawnerAreaResponse toResponse() {
        return new MonsterSpawnerAreaResponse();
    }

    public class MonsterSpawnerAreaResponse extends MonsterAreaResponse {}
}
