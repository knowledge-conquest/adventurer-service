package com.jackeri.kc.models.areas;

import com.jackeri.kc.models.DocumentType;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TrainingArea extends Area {

    public TrainingArea(String regionId) {
        super(regionId);
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.TRAINING_AREA;
    }

    @Override
    public TrainingAreaResponse toResponse() {
        return new TrainingAreaResponse();
    }

    public class TrainingAreaResponse extends AreaResponse {}
}
