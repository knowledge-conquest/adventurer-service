package com.jackeri.kc.models.areas;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class TeleportationArea extends Area {

    public enum Direction {
        FORWARD, BACKWARD
    }

    @Getter @Setter
    private Reference<TeleportationArea> destination;

    @Getter @Setter
    private Direction direction;

    @Getter @Setter
    private boolean opened;

    public TeleportationArea(String regionId, boolean opened, Direction direction) {
        super(regionId);
        this.destination = null;
        this.opened = opened;
        this.direction = direction;
    }

    public TeleportationArea(String regionId, String destinationKey, boolean opened, Direction direction) {
        super(regionId);
        this.destination = new Reference<>(destinationKey);
        this.opened = opened;
        this.direction = direction;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.TELEPORTATION_AREA;
    }

    @Override
    public TeleportationAreaResponse toResponse() {
        return new TeleportationAreaResponse();
    }

    public class TeleportationAreaResponse extends AreaResponse {

        @Getter
        private final String destinationKey;

        @Getter
        private final Direction direction;

        @Getter
        private final boolean opened;

        protected TeleportationAreaResponse() {
            Reference<TeleportationArea> destination = TeleportationArea.this.destination;
            destinationKey = destination != null ? destination.getKey() : null;
            direction = TeleportationArea.this.getDirection();
            opened = TeleportationArea.this.isOpened();
        }
    }
}
