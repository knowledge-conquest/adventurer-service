package com.jackeri.kc.models.areas;

import com.jackeri.kc.models.DocumentType;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class SpawnArea extends Area {

    public SpawnArea(String regionId) {
        super(regionId);
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.SPAWN_AREA;
    }

    @Override
    public SpawnAreaResponse toResponse() {
        return new SpawnAreaResponse();
    }

    public class SpawnAreaResponse extends AreaResponse {}
}
