package com.jackeri.kc.models.areas;

import com.jackeri.kc.models.DocumentType;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class SkillPracticeArea extends Area {

    public SkillPracticeArea(String regionId) {
        super(regionId);
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.SKILL_PRACTICE_AREA;
    }

    @Override
    public SkillPracticeAreaResponse toResponse() {
        return new SkillPracticeAreaResponse();
    }

    public class SkillPracticeAreaResponse extends AreaResponse {}
}
