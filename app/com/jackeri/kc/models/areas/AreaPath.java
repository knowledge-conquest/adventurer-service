package com.jackeri.kc.models.areas;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.generation.map.Movement;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Edge;
import com.jackeri.kc.models.Reference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
public class AreaPath extends Edge {

    @Getter @Setter
    @JsonProperty("_from")
    private Reference<Area> a;

    @Getter @Setter
    @JsonProperty("_to")
    private Reference<Area> b;

    @Getter @Setter
    private Vector<Movement> path;

    public AreaPath(String aId, String bId, Vector<Movement> path) {
        this.a = new Reference<>(aId);
        this.b = new Reference<>(bId);
        this.path = path;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.AREA_PATH;
    }

    @Override
    public AreaPathResponse toResponse() {
        return new AreaPathResponse();
    }

    public class AreaPathResponse extends EdgeResponse {

        @Getter
        private String aKey;

        @Getter
        private String bKey;

        @Getter
        private Vector<Movement> path;

        protected AreaPathResponse() {
            aKey = AreaPath.this.getA().getKey();
            bKey = AreaPath.this.getB().getKey();
            path = AreaPath.this.getPath();
        }
    }
}
