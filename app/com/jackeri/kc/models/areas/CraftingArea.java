package com.jackeri.kc.models.areas;

import com.jackeri.kc.models.DocumentType;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CraftingArea extends Area {

    public CraftingArea(String regionId) {
        super(regionId);
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.CRAFTING_AREA;
    }

    @Override
    public CraftingAreaResponse toResponse() {
        return new CraftingAreaResponse();
    }

    public class CraftingAreaResponse extends AreaResponse {}
}
