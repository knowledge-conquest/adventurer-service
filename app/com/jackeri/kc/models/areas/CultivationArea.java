package com.jackeri.kc.models.areas;

import com.jackeri.kc.models.DocumentType;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CultivationArea extends Area {

    public CultivationArea(String regionId) {
        super(regionId);
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.CULTIVATION_AREA;
    }

    @Override
    public CultivationAreaResponse toResponse() {
        return new CultivationAreaResponse();
    }

    public class CultivationAreaResponse extends AreaResponse {}
}
