package com.jackeri.kc.models.areas;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Edge;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.areas.monster.MonsterBossArea;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class BossTeleportation extends Edge {

    @Getter @Setter
    @JsonProperty("_from")
    private Reference<MonsterBossArea> monsterBossArea;

    @Getter @Setter
    @JsonProperty("_to")
    private Reference<TeleportationArea> teleportationArea;

    public BossTeleportation(String monsterBossAreaId, String teleportationAreaId) {
        this.monsterBossArea = new Reference<>(monsterBossAreaId);
        this.teleportationArea = new Reference<>(teleportationAreaId);
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.BOSS_TELEPORTATION;
    }

    @Override
    public AreaPathResponse toResponse() {
        return new AreaPathResponse();
    }

    public class AreaPathResponse extends EdgeResponse {

        @Getter
        private final String monsterBossArea;

        @Getter
        private final String teleportationArea;

        protected AreaPathResponse() {
            monsterBossArea = BossTeleportation.this.getMonsterBossArea().getKey();
            teleportationArea = BossTeleportation.this.getTeleportationArea().getKey();
        }
    }
}
