package com.jackeri.kc.models.areas;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.itemdata.ItemData;
import com.jackeri.kc.models.itemdata.edges.GatheringLoot;
import com.jackeri.kc.utils.Pair;
import com.jackeri.kc.utils.Range;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
public class GatheringArea extends Area {

    @Getter @Setter
    private double duration;

    @JsonIgnore
    private Vector<Pair<ItemData, Range<Long>>> loots;

    public GatheringArea(String regionId, double duration) {
        super(regionId);
        this.duration = duration;
        this.loots = new Vector<>();
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.GATHERING_AREA;
    }

    @JsonIgnore
    public Vector<GatheringLoot> getLootEdges() {
        Vector<GatheringLoot> extract = new Vector<>();
        for (Pair<ItemData, Range<Long>> loot : loots) {
            extract.add(new GatheringLoot(loot.getA().getId(), getId(), loot.getB()));
        }
        return extract;
    }

    @JsonIgnore
    public Vector<Pair<ItemData, Range<Long>>> getLoots() {
        return loots;
    }

    @JsonSetter
    public void setLoots(Vector<Pair<ItemData, Range<Long>>> loots) {
        this.loots = loots;
    }

    @Override
    public GatheringAreaResponse toResponse() {
        return new GatheringAreaResponse();
    }

    public class GatheringAreaResponse extends AreaResponse {

        @Getter
        private double duration;

        protected GatheringAreaResponse() {
            duration = GatheringArea.this.getDuration();
        }
    }

    public GatheringResponse toGatheringResponse() {
        return new GatheringResponse();
    }

    public class GatheringResponse extends AreaResponse {

        @Getter
        private double duration;

        @Getter
        private Vector<Pair<ItemData.ItemDataResponse, Range<Long>>> loots;

        protected GatheringResponse() {
            duration = GatheringArea.this.getDuration();

            loots = new Vector<>();
            for (Pair<ItemData, Range<Long>> loot : GatheringArea.this.getLoots()) {
                loots.add(new Pair<>(loot.getA().toResponse(), loot.getB()));
            }
        }
    }
}
