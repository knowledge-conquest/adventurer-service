package com.jackeri.kc.models.areas;

import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.regions.Region;
import com.jackeri.kc.utils.V2Int;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public abstract class Area extends Document {

    @Getter @Setter
    private V2Int position;

    @Getter @Setter
    private Reference<Region> region;

    public Area(String regionId) {
        this.region = new Reference<>(regionId);
    }

    /* Serialization helpers for API responses */

    @Override
    public abstract AreaResponse toResponse();

    public abstract class AreaResponse extends DocumentResponse {

        @Getter
        private V2Int position;

        @Getter
        private String regionKey;

        @Getter
        private Region.RegionResponse region;

        protected AreaResponse() {
            position = Area.this.getPosition();

            Reference<Region> regionReference = Area.this.getRegion();
            regionKey = regionReference.getKey();
            region = regionReference.isPopulated() ? regionReference.getDocument().toResponse() : null;
        }
    }
}
