package com.jackeri.kc.models.itemdata;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum ItemTier {
    COMMON("Common", 0),
    RARE("Rare", 10),
    EPIC("Epic", 35),
    LEGENDARY("Legendary", 60);

    @Getter
    private final String name;

    @Getter
    private final long bonus;
}
