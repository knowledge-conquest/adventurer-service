package com.jackeri.kc.models.itemdata;

public enum WeaponType {
    SWORD,
    DAGGER,
    AXE,
    HAMMER,
    SPEAR,
    STAFF,
    SCYTHE,
    BOW
}
