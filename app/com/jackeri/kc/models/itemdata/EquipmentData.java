package com.jackeri.kc.models.itemdata;

import com.jackeri.kc.models.items.Equipment;
import com.jackeri.kc.utils.Range;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
public abstract class EquipmentData extends ItemData {

    @Getter
    @Setter
    private EquipmentSlot equipmentSlot;

    @Getter
    @Setter
    private Range<Long> health;

    @Getter
    @Setter
    private Range<Long> speed;

    public EquipmentData(ItemTier itemTier, long level, String realm,
                         EquipmentSlot equipmentSlot, Range<Long> health, Range<Long> speed) {
        super(itemTier, level, realm);
        this.equipmentSlot = equipmentSlot;
        this.health = health;
        this.speed = speed;
    }

    @Override
    public ItemType getItemType() {
        return ItemType.EQUIPMENT;
    }

    @Override
    public abstract Vector<? extends Equipment> createItem(long quantity);

    @Override
    public abstract EquipmentDataResponse toResponse();

    public abstract class EquipmentDataResponse extends ItemDataResponse {

        @Getter
        private final EquipmentSlot equipmentSlot;

        @Getter
        private final Range<Long> health;

        @Getter
        private final Range<Long> speed;

        public EquipmentDataResponse() {
            equipmentSlot = EquipmentData.this.getEquipmentSlot();
            health = EquipmentData.this.getHealth();
            speed = EquipmentData.this.getSpeed();
        }
    }
}
