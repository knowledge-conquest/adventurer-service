package com.jackeri.kc.models.itemdata;

public enum ItemType {
    RESOURCE, EQUIPMENT
}
