package com.jackeri.kc.models.itemdata;

public enum EquipmentSlot {
    WEAPON,
    HELMET,
    CHEST_PLATE,
    PANTS,
    GLOVES,
    BOOTS,
}
