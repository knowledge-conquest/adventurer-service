package com.jackeri.kc.models.itemdata;

import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.items.Armour;
import com.jackeri.kc.models.utils.calculators.AttributeCalculator;
import com.jackeri.kc.utils.Random;
import com.jackeri.kc.utils.Range;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
public class ArmourData extends EquipmentData {

    @Getter
    @Setter
    private Range<Long> defense;

    public ArmourData(ItemTier itemTier, long level, String realm, EquipmentSlot equipmentSlot,
                      Range<Long> health, Range<Long> speed, Range<Long> defense) {
        super(itemTier, level, realm, equipmentSlot, health, speed);
        this.defense = defense;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.ARMOUR_DATA;
    }

    @Override
    public Vector<Armour> createItem(long quantity) {
        Vector<Armour> armours = new Vector<>();
        for (int i = 0; i < quantity; i += 1) {
            armours.add(new Armour(
                    getId(),
                    Random.getLong(getHealth()),
                    Random.getLong(getSpeed()),
                    Random.getLong(getDefense())));
        }
        return armours;
    }

    public static ArmourData generate(ItemTier itemTier, long level, EquipmentSlot slot, String realm) {
        return new ArmourData(
                itemTier, level, realm, slot,
                Range.generateAttributeRange(AttributeCalculator.calculateHealth(level), itemTier.getBonus()),
                Range.generateAttributeRange(AttributeCalculator.calculateSpeed(level), itemTier.getBonus()),
                Range.generateAttributeRange(AttributeCalculator.calculateDefense(level), itemTier.getBonus())
        );
    }

    @Override
    public ArmourDataResponse toResponse() {
        return new ArmourDataResponse();
    }

    public class ArmourDataResponse extends EquipmentDataResponse {

        @Getter
        private final Range<Long> defense;

        public ArmourDataResponse() {
            defense = ArmourData.this.getDefense();
        }
    }
}
