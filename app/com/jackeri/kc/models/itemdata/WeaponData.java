package com.jackeri.kc.models.itemdata;

import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.items.Weapon;
import com.jackeri.kc.models.utils.calculators.AttributeCalculator;
import com.jackeri.kc.utils.Random;
import com.jackeri.kc.utils.Range;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
public class WeaponData extends EquipmentData {

    @Getter
    @Setter
    private Range<Long> attack;

    @Getter
    @Setter
    private WeaponType weaponType;

    public WeaponData(ItemTier iteTier, long level, String realm, Range<Long> health,
                      Range<Long> speed, Range<Long> attack, WeaponType weaponType) {
        super(iteTier, level, realm, EquipmentSlot.WEAPON, health, speed);
        this.attack = attack;
        this.weaponType = weaponType;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.WEAPON_DATA;
    }

    @Override
    public Vector<Weapon> createItem(long quantity) {
        Vector<Weapon> weapons = new Vector<>();
        for (int i = 0; i < quantity; i += 1) {
            weapons.add(new Weapon(
                    getId(),
                    Random.getLong(getHealth()),
                    Random.getLong(getSpeed()),
                    Random.getLong(getAttack())));
        }
        return weapons;
    }

    public static WeaponData generate(ItemTier itemTier, long level, WeaponType weaponType, String realm) {
        return new WeaponData(
                itemTier, level, realm,
                Range.generateAttributeRange(AttributeCalculator.calculateHealth(level), itemTier.getBonus()),
                Range.generateAttributeRange(AttributeCalculator.calculateSpeed(level), itemTier.getBonus()),
                Range.generateAttributeRange(AttributeCalculator.calculateAttack(level), itemTier.getBonus()),
                weaponType
        );
    }

    @Override
    public WeaponDataResponse toResponse() {
        return new WeaponDataResponse();
    }

    public class WeaponDataResponse extends EquipmentDataResponse {

        @Getter
        private final Range<Long> attack;

        @Getter
        private final WeaponType weaponType;

        public WeaponDataResponse() {
            attack = WeaponData.this.getAttack();
            weaponType = WeaponData.this.getWeaponType();
        }
    }
}
