package com.jackeri.kc.models.itemdata;

public enum ResourceType {
    WOOD,
    ROCK,
    METAL,
    PLANT
}
