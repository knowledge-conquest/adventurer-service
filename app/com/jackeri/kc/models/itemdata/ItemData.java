package com.jackeri.kc.models.itemdata;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.models.realms.Realm;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
public abstract class ItemData extends Document {

    @Getter
    @Setter
    private ItemTier itemTier;

    @Getter
    @Setter
    private long level;

    @Getter
    @Setter
    private Reference<Realm> realm;

    public ItemData(ItemTier itemTier, long level, String realm) {
        this.itemTier = itemTier;
        this.level = level;
        this.realm = new Reference<>(realm);
    }

    @JsonGetter
    public abstract ItemType getItemType();

    @Override
    public abstract ItemDataResponse toResponse();

    public abstract Vector<? extends Item> createItem(long quantity);

    public Vector<? extends Item> createItem(long quantity, boolean setItemData) {
        Vector<? extends Item> items = createItem(quantity);
        if (setItemData) {
            for (Item item : items) {
                item.getItemData().setDocument(this);
            }
        }
        return items;
    }

    public abstract class ItemDataResponse extends DocumentResponse {

        @Getter
        private final ItemTier itemTier;

        @Getter
        private final long level;

        @Getter
        private final String realm;

        public ItemDataResponse() {
            itemTier = ItemData.this.getItemTier();
            level = ItemData.this.getLevel();
            realm = ItemData.this.getKey();
        }
    }
}
