package com.jackeri.kc.models.itemdata.edges;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.recipes.Recipe;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class RecipeComponent extends ItemDataEdge {

    public enum ComponentType {
        INGREDIENT, RESULT
    }

    @Getter
    @Setter
    @JsonProperty("_to")
    private Reference<Recipe> recipe;

    @Getter
    @Setter
    private long quantity;

    @Getter
    @Setter
    private ComponentType componentType;

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.RECIPE_COMPONENT;
    }

    public RecipeComponent(String itemData, String recipe, long quantity, ComponentType componentType) {
        super(itemData);
        this.recipe = new Reference<>(recipe);
        this.quantity = quantity;
        this.componentType = componentType;
    }

    @Override
    public RecipeComponentResponse toResponse() {
        return new RecipeComponentResponse();
    }

    public class RecipeComponentResponse extends ItemDataEdgeResponse {
        @Getter
        private final Recipe.RecipeResponse recipe;

        @Getter
        private final long quantity;

        @Getter
        private final ComponentType componentType;

        public RecipeComponentResponse() {
            recipe = RecipeComponent.this.getRecipe().getDocument().toResponse();
            quantity = RecipeComponent.this.getQuantity();
            componentType = RecipeComponent.this.getComponentType();
        }
    }
}
