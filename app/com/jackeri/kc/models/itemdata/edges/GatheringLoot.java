package com.jackeri.kc.models.itemdata.edges;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.areas.GatheringArea;
import com.jackeri.kc.utils.Range;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class GatheringLoot extends ItemDataEdge {

    @Getter
    @Setter
    @JsonProperty("_to")
    private Reference<GatheringArea> area;

    @Getter
    @Setter
    private Range<Long> quantity;

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.GATHERING_LOOT;
    }

    public GatheringLoot(String itemData, String area, Range<Long> quantity) {
        super(itemData);
        this.area = new Reference<>(area);
        this.quantity = quantity;
    }

    @Override
    public GatheringLootResponse toResponse() {
        return new GatheringLootResponse();
    }

    public class GatheringLootResponse extends ItemDataEdgeResponse {
        @Getter
        private final GatheringArea.GatheringAreaResponse area;

        @Getter
        private final Range<Long> quantity;

        public GatheringLootResponse() {
            area = GatheringLoot.this.getArea().getDocument().toResponse();
            quantity = GatheringLoot.this.getQuantity();
        }
    }
}
