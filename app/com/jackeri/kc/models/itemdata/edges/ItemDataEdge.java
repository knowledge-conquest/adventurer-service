package com.jackeri.kc.models.itemdata.edges;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.Edge;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.itemdata.ItemData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public abstract class ItemDataEdge extends Edge {

    @Getter
    @Setter
    @JsonProperty("_from")
    private Reference<ItemData> itemData;

    public ItemDataEdge(String itemData) {
        this.itemData = new Reference<>(itemData);
    }

    @Override
    public abstract ItemDataEdgeResponse toResponse();

    public class ItemDataEdgeResponse extends EdgeResponse {

        @Getter
        private final ItemData.ItemDataResponse itemData;

        public ItemDataEdgeResponse() {
            itemData = ItemDataEdge.this.getItemData().getDocument().toResponse();
        }
    }
}
