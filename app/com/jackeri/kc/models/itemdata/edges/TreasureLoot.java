package com.jackeri.kc.models.itemdata.edges;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.areas.treasure.TreasureArea;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class TreasureLoot extends ItemDataEdge {

    @Getter
    @Setter
    @JsonProperty("_to")
    private Reference<TreasureArea> area;

    @Getter
    @Setter
    private long quantity;

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.TREASURE_LOOT;
    }

    public TreasureLoot(String itemData, String area, long quantity) {
        super(itemData);
        this.area = new Reference<>(area);
        this.quantity = quantity;
    }

    @Override
    public TreasureLootResponse toResponse() {
        return new TreasureLootResponse();
    }

    public class TreasureLootResponse extends ItemDataEdgeResponse {
        @Getter
        private final TreasureArea.TreasureAreaResponse area;

        @Getter
        private final long quantity;

        public TreasureLootResponse() {
            area = TreasureLoot.this.getArea().getDocument().toResponse();
            quantity = TreasureLoot.this.getQuantity();
        }
    }
}
