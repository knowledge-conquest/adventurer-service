package com.jackeri.kc.models.itemdata.edges;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.entities.MonsterData;
import com.jackeri.kc.utils.Range;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class MonsterLoot extends ItemDataEdge {

    @Getter
    @Setter
    @JsonProperty("_to")
    private Reference<MonsterData> monsterData;

    @Getter
    @Setter
    private Range<Long> quantity;

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.MONSTER_LOOT;
    }

    public MonsterLoot(String itemData, String monsterData, Range<Long> quantity) {
        super(itemData);
        this.monsterData = new Reference<>(monsterData);
        this.quantity = quantity;
    }

    @Override
    public GatheringLootResponse toResponse() {
        return new GatheringLootResponse();
    }

    public class GatheringLootResponse extends ItemDataEdgeResponse {
        @Getter
        private final String monsterData;

        @Getter
        private final Range<Long> quantity;

        public GatheringLootResponse() {
            monsterData = MonsterLoot.this.getMonsterData().getKey();
            quantity = MonsterLoot.this.getQuantity();
        }
    }
}
