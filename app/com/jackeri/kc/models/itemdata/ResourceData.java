package com.jackeri.kc.models.itemdata;

import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.items.Resource;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
public class ResourceData extends ItemData {

    @Getter
    @Setter
    private ResourceType resourceType;

    public ResourceData(ItemTier itemTier, long level, String realm, ResourceType resourceType) {
        super(itemTier, level, realm);
        this.resourceType = resourceType;
    }

    @Override
    public ItemType getItemType() {
        return ItemType.RESOURCE;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.RESOURCE_DATA;
    }

    @Override
    public Vector<Resource> createItem(long quantity) {
        Vector<Resource> resources = new Vector<>();
        resources.add(new Resource(getId(), quantity));
        return resources;
    }

    @Override
    public ResourceDataResponse toResponse() {
        return new ResourceDataResponse();
    }

    public class ResourceDataResponse extends ItemDataResponse {

        @Getter
        private final ResourceType resourceType;

        public ResourceDataResponse() {
            resourceType = ResourceData.this.getResourceType();
        }
    }
}
