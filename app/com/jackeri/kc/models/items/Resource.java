package com.jackeri.kc.models.items;

import com.jackeri.kc.models.DocumentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Resource extends Item {

    @Getter
    @Setter
    private long quantity;

    public Resource(String itemData, long quantity) {
        super(itemData);
        this.quantity = quantity;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.RESOURCE;
    }

    @Override
    public ResourceResponse toResponse() {
        return new ResourceResponse();
    }

    public class ResourceResponse extends ItemResponse {

        @Getter
        private final long quantity;

        public ResourceResponse() {
            quantity = Resource.this.quantity;
        }
    }
}
