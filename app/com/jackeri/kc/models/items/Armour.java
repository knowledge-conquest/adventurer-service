package com.jackeri.kc.models.items;

import com.jackeri.kc.models.DocumentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Armour extends Equipment {

    @Getter
    @Setter
    private long defense;

    public Armour(String itemData, long health, long speed, long defense) {
        super(itemData, health, speed);
        this.defense = defense;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.ARMOUR;
    }

    @Override
    public ArmourResponse toResponse() {
        return new ArmourResponse();
    }

    public class ArmourResponse extends EquipmentResponse {

        @Getter
        private final long defense;

        public ArmourResponse() {
            defense = Armour.this.getDefense();
        }
    }
}
