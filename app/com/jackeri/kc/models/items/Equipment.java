package com.jackeri.kc.models.items;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public abstract class Equipment extends Item {

    @Getter
    @Setter
    private long health;

    @Getter
    @Setter
    private long speed;

    public Equipment(String itemData, long health, long speed) {
        super(itemData);
        this.health = health;
        this.speed = speed;
    }

    @Override
    public abstract EquipmentResponse toResponse();

    public abstract class EquipmentResponse extends ItemResponse {

        @Getter
        private final long health;

        @Getter
        private final long speed;

        public EquipmentResponse() {
            health = Equipment.this.getHealth();
            speed = Equipment.this.getSpeed();
        }
    }
}
