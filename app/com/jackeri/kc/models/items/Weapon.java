package com.jackeri.kc.models.items;

import com.jackeri.kc.models.DocumentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Weapon extends Equipment {

    @Getter
    @Setter
    private long attack;

    public Weapon(String itemData, long health, long speed, long attack) {
        super(itemData, health, speed);
        this.attack = attack;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.WEAPON;
    }

    @Override
    public WeaponResponse toResponse() {
        return new WeaponResponse();
    }

    public class WeaponResponse extends EquipmentResponse {

        @Getter
        private final long attack;

        public WeaponResponse() {
            attack = Weapon.this.getAttack();
        }
    }
}
