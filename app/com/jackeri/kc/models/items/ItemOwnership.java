package com.jackeri.kc.models.items;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Edge;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.entities.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class ItemOwnership extends Edge {

    @Getter
    @Setter
    @JsonProperty("_from")
    private Reference<Entity> entity;

    @Getter @Setter
    @JsonProperty("_to")
    private Reference<Item> item;

    public ItemOwnership(String entity, String item) {
        this.entity = new Reference<>(entity);
        this.item = new Reference<>(item);
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.ITEM_OWNERSHIP;
    }

    @Override
    public ItemOwnershipResponse toResponse() {
        return new ItemOwnershipResponse();
    }

    public class ItemOwnershipResponse extends EdgeResponse {
        private final Entity.EntityResponse entity;
        private final Item.ItemResponse item;

        public ItemOwnershipResponse() {
            entity = ItemOwnership.this.getEntity().getDocument().toResponse();
            item = ItemOwnership.this.getItem().getDocument().toResponse();
        }
    }
}
