package com.jackeri.kc.models.items;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Edge;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.entities.Entity;
import com.jackeri.kc.models.itemdata.EquipmentSlot;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class WornEquipment extends Edge {

    @Getter
    @Setter
    @JsonProperty("_from")
    private Reference<Entity> entity;

    @Getter
    @Setter
    @JsonProperty("_to")
    private Reference<Equipment> equipment;

    @Getter
    @Setter
    private EquipmentSlot equipmentSlot;

    public WornEquipment(String entityId, String equipmentId, EquipmentSlot equipmentSlot) {
        this.entity = new Reference<>(entityId);
        this.equipment = new Reference<>(equipmentId);
        this.equipmentSlot = equipmentSlot;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.WORN_EQUIPMENT;
    }

    @Override
    public ItemOwnershipResponse toResponse() {
        return new ItemOwnershipResponse();
    }

    public class ItemOwnershipResponse extends EdgeResponse {

        @Getter
        private final Entity.EntityResponse entity;

        @Getter
        private final Equipment.EquipmentResponse equipment;

        @Getter
        private final String equipmentSlot;

        public ItemOwnershipResponse() {
            entity = WornEquipment.this.getEntity().getDocument().toResponse();
            equipment = WornEquipment.this.getEquipment().getDocument().toResponse();
            equipmentSlot = WornEquipment.this.getEquipmentSlot().name();
        }
    }
}
