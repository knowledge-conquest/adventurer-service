package com.jackeri.kc.models.items;

import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.itemdata.ItemData;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public abstract class Item extends Document {

    @Getter
    @Setter
    private Reference<ItemData> itemData;

    public Item(String itemData) {
        this.itemData = new Reference<>(itemData);
    }

    @Override
    public abstract ItemResponse toResponse();

    public abstract class ItemResponse extends DocumentResponse {

        @Getter
        private final ItemData.ItemDataResponse itemData;

        public ItemResponse() {
            itemData = Item.this.getItemData().getDocument().toResponse();
        }
    }
}
