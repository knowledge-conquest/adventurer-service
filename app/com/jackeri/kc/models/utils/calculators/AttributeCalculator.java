package com.jackeri.kc.models.utils.calculators;

import com.jackeri.kc.models.skills.SkillTier;

public class AttributeCalculator {

    public static long calculateHealth(long level) {
        return level * 30;
    }

    public static long calculateSkillHeal(long level, SkillTier skillTier) {
        return (long) (level * 3 * skillTier.getModifier());
    }

    public static long calculateStamina(long level) {
        return level * 10;
    }

    public static long calculateSpeed(long level) {
        return level;
    }

    public static long calculateAttack(long level) {
        return level * 5;
    }

    public static long calculateSkillAttack(long level, SkillTier skillTier) {
        return (long) (level * 5 * skillTier.getModifier());
    }

    public static long calculateDefense(long level) {
        return level * 2;
    }

    public static long calculatePlayerExperience(long level) {
        return level * 100;
    }

    public static long calculateSkillExperience(long level) {
        return level * 10;
    }

    public static long calculateExperienceDrop(long level) {
        return level * 10;
    }

    public static long calculateCultivationExperience(long level, long duration) {
        return (long) (0.1 * level * duration);
    }

    public static long calculateSkillPracticeExperience(long level, long duration) {
        return (long) (0.1 * level * duration);
    }

    public static double calculateTrainingProficiency(long level, long duration) {
        return 0.005 * level * (duration / 60.0);
    }

    public static long calculateSpawnRecoveryHealth(long level, long duration) {
        return (long) (0.5 * level * duration);
    }
}
