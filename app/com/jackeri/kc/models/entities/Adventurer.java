package com.jackeri.kc.models.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.areas.Area;
import com.jackeri.kc.models.entities.attributes.CappedAttribute;
import com.jackeri.kc.models.entities.attributes.Experiencable;
import com.jackeri.kc.models.entities.attributes.ExperienceAttribute;
import com.jackeri.kc.models.entities.attributes.StaminaAttribute;
import com.jackeri.kc.models.itemdata.WeaponData;
import com.jackeri.kc.models.items.Weapon;
import com.jackeri.kc.models.skills.Skill;
import com.jackeri.kc.models.skills.SkillEffect;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;
import java.util.Vector;

@NoArgsConstructor
public class Adventurer extends Entity implements Experiencable {

    @JsonIgnore
    @Getter(AccessLevel.PROTECTED)
    private ExperienceAttribute experienceAttribute;

    @JsonIgnore
    @Getter
    private Vector<Skill> skills;

    @JsonIgnore
    @Getter
    private Weapon weapon;

    @JsonIgnore
    @Getter
    private double weaponProficiency;

    @Getter
    @Setter
    private long userId;

    @Getter
    @Setter
    private Reference<Area> location;

    @Getter
    @Setter
    private long highestLevel;

    public Adventurer(String name, long userId) {
        super(name);
        this.experienceAttribute = new ExperienceAttribute(getLevel(), 0);
        this.userId = userId;
        this.highestLevel = 1L;
    }

    @JsonCreator
    private Adventurer(@JsonProperty("name") String name, @JsonProperty("level") long level,
                       @JsonProperty("health") long health, @JsonProperty("experience") long experience,
                       @JsonProperty("userId") long userId, @JsonProperty("location") Reference<Area> location,
                       @JsonProperty("healthBonus") long healthBonus, @JsonProperty("speedBonus") long speedBonus,
                       @JsonProperty("attackBonus") long attackBonus, @JsonProperty("defenseBonus") long defenseBonus,
                       @JsonProperty("highestLevel") long highestLevel, @JsonProperty("skills") Vector<Skill> skills,
                       @JsonProperty("weapon") Weapon weapon, @JsonProperty("weaponProficiency") Double weaponProficiency ) {
        super(name, level, health, healthBonus, speedBonus, attackBonus, defenseBonus);
        this.userId = userId;
        this.location = location;
        this.experienceAttribute = new ExperienceAttribute(getLevel(), experience);
        this.highestLevel = highestLevel;
        this.skills = skills != null ? skills : new Vector<>();
        this.weapon = weapon;
        this.weaponProficiency = weaponProficiency != null ? weaponProficiency : 1.0;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.ADVENTURER;
    }

    /* Game logic helpers */

    @JsonGetter
    public long getExperience() {
        return experienceAttribute.getValue();
    }

    @JsonIgnore
    public long getMaxExperience() {
        return experienceAttribute.getMax();
    }

    public void gainExperience(long amount) {
        getExperienceAttribute().gainExperience(amount, this);
    }

    public void levelUp() {
        setLevel(getLevel() + 1);
    }

    @Override
    public long launchAttack(Entity target, Skill skill) {
        long damage = 0;
        double modifier = 1.0;

        if (skill == null) {
            if (getWeapon() != null) {
                damage = getWeapon().getAttack();
                modifier = weaponProficiency;
            }
        } else if (skill.getSkillEffect() == SkillEffect.DAMAGE) {
            damage = skill.getValue();
            modifier = 0.5;
            if (getWeapon() != null) {
                WeaponData weaponData = (WeaponData) getWeapon().getItemData().getDocument();
                if (skill.getWeaponType() == weaponData.getWeaponType()) {
                    modifier = getWeaponProficiency();
                }
            }
        }

        return target.receiveAttack((long) ((getAttack() + damage) * modifier));
    }

    public void reset() {
        setLocation(null);

        setHighestLevel(getLevel());
        setLevel(1L);

        getHealthAttribute().update(getLevel());
        getHealthAttribute().fill();

        getExperienceAttribute().update(getLevel());
        getExperienceAttribute().empty();
    }

    /* Serialization helpers for API responses */

    @Override
    public AdventurerResponse toResponse() {
        return new AdventurerResponse();
    }

    public boolean isSkillReady(int skillSlotIndex) {
        if (skillSlotIndex < 0) {
            return true;
        }

        Skill skill = getSkill(skillSlotIndex);
        return skill != null && skill.getCooldown() <= 1;
    }

    @JsonIgnore
    public Skill getSkill(int skillSlotIndex) {
        for (Skill skill : getSkills()) {
            if (skill.getSlotIndex() != null && skill.getSlotIndex() == skillSlotIndex) {
                return skill;
            }
        }

        return null;
    }

    public class AdventurerResponse extends EntityResponse {

        @Getter
        private final Vector<Skill.SkillResponse> skills;

        @Getter
        private final Weapon.WeaponResponse weapon;

        @Getter
        private final Double weaponProficiency;

        @Getter
        private final CappedAttribute experience;

        @Getter
        private final long userId;

        protected AdventurerResponse() {
            skills = new Vector<>();
            for (Skill skill : Adventurer.this.getSkills()) {
                skills.add(skill.toResponse());
            }

            weapon = Adventurer.this.getWeapon() != null ? Adventurer.this.getWeapon().toResponse() : null;
            weaponProficiency = Adventurer.this.getWeaponProficiency();
            experience = Adventurer.this.getExperienceAttribute();
            userId = Adventurer.this.getUserId();
        }
    }
}
