package com.jackeri.kc.models.entities;

import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.DocumentType;
import lombok.Getter;
import lombok.Setter;

public class MonsterData extends Document {

    public enum MonsterType {
        MINION, BOSS
    }

    @Getter @Setter
    private String name;

    @Getter @Setter
    private long level;

    @Getter @Setter
    private MonsterType monsterType;

    public MonsterData(String name, long level, MonsterType monsterType) {
        this.name = name;
        this.level = level;
        this.monsterType = monsterType;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.MONSTER_DATA;
    }

    @Override
    public MonsterDataResponse toResponse() {
        return new MonsterDataResponse();
    }

    public class MonsterDataResponse extends DocumentResponse {

        @Getter
        private String name;

        @Getter
        private long level;

        protected MonsterDataResponse() {
            name = MonsterData.this.getName();
            level = MonsterData.this.getLevel();
        }
    }
}
