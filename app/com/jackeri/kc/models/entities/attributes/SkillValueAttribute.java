package com.jackeri.kc.models.entities.attributes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jackeri.kc.models.skills.SkillEffect;
import com.jackeri.kc.models.skills.SkillTier;
import com.jackeri.kc.models.utils.calculators.AttributeCalculator;

public class SkillValueAttribute extends Attribute {

    @JsonIgnore
    private final SkillEffect skillEffect;

    @JsonIgnore
    private final SkillTier skillTier;

    public SkillValueAttribute(SkillEffect skillEffect, SkillTier skillTier, long level) {
        super(level);
        this.skillEffect = skillEffect;
        this.skillTier = skillTier;
        update(level);
    }

    @Override
    protected long calculateFromLevel(long level) {
        if (skillEffect == null || skillTier == null) {
            return 0;
        }

        switch (skillEffect) {
            case DAMAGE:
                return AttributeCalculator.calculateSkillAttack(level, skillTier);
            case HEAL:
                return AttributeCalculator.calculateSkillHeal(level, skillTier);
            default:
                return 0;
        }
    }
}
