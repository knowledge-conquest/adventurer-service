package com.jackeri.kc.models.entities.attributes;

public interface Experiencable {
    void gainExperience(long amount);
    long getLevel();
    void levelUp();
}
