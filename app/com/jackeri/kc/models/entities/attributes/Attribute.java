package com.jackeri.kc.models.entities.attributes;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public abstract class Attribute {

    @Getter @Setter(AccessLevel.PROTECTED)
    private long value;

    public Attribute(long level) {
        update(level);
    }

    public Attribute(long level, long bonus) {
        update(level);
        expand(bonus);
    }

    public void update(long level) {
        setValue(calculateFromLevel(level));
    }

    protected abstract long calculateFromLevel(long level);

    public void expand(long amount) {
        if (amount <= 0) return;
        this.value += amount;
    }

    public void shrink(long amount) {
        if (amount <= 0) return;
        this.value -= amount;
    }
}
