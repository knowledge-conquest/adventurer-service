package com.jackeri.kc.models.entities.attributes;

import com.jackeri.kc.models.utils.calculators.AttributeCalculator;

public class StaminaAttribute extends CappedAttribute {

    public StaminaAttribute(long level) {
        super(level);
    }

    public StaminaAttribute(long level, long value) {
        super(level, value);
    }

    public StaminaAttribute(long level, long value, long bonus) {
        super(level, value, bonus);
    }

    @Override
    protected long calculateFromLevel(long level) {
        return AttributeCalculator.calculateStamina(level);
    }
}
