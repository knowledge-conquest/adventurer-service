package com.jackeri.kc.models.entities.attributes;

import com.jackeri.kc.models.utils.calculators.AttributeCalculator;

public class SpeedAttribute extends Attribute {

    public SpeedAttribute(long level) {
        super(level);
    }

    public SpeedAttribute(long level, long bonus) {
        super(level, bonus);
    }

    @Override
    protected long calculateFromLevel(long level) {
        return AttributeCalculator.calculateSpeed(level);
    }
}
