package com.jackeri.kc.models.entities.attributes;

import com.jackeri.kc.models.utils.calculators.AttributeCalculator;

public class DefenseAttribute extends Attribute {

    public DefenseAttribute(long level) {
        super(level);
    }

    public DefenseAttribute(long level, long bonus) {
        super(level, bonus);
    }

    @Override
    protected long calculateFromLevel(long level) {
        return AttributeCalculator.calculateDefense(level);
    }
}
