package com.jackeri.kc.models.entities.attributes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

public abstract class CappedAttribute extends Attribute {

    @Getter @Setter(AccessLevel.PROTECTED)
    private long max;

    public CappedAttribute(long level) {
        update(level);
        fill();
    }

    public CappedAttribute(long level, long value) {
        update(level);
        setValue(value);
    }

    public CappedAttribute(long level, long value, long bonus) {
        update(level);
        expand(bonus);
        setValue(value);
    }

    public void add(long amount) {
        if (amount > 0) {
            setValue(getValue() + amount);
        }
    }

    public void sub(long amount) {
        if (amount > 0) {
            setValue(getValue() - amount);
        }
    }

    public void fill() {
        setValue(getMax());
    }

    public void empty() {
        setValue(0);
    }

    @Override
    public void update(long level) {
        setMax(calculateFromLevel(level));
        setValue(getValue());
    }

    @JsonIgnore
    public boolean isEmpty() {
        return getValue() == 0;
    }

    @JsonIgnore
    public long getMissing() {
        return getMax() - getValue();
    }

    @Override
    protected void setValue(long value) {
        super.setValue(Math.min(Math.max(0, value), getMax()));
    }

    @Override
    public void expand(long amount) {
        if (amount <= 0) return;
        max += amount;
    }

    @Override
    public void shrink(long amount) {
        if (amount <= 0) return;
        max -= amount;
    }
}
