package com.jackeri.kc.models.entities.attributes;

import com.jackeri.kc.models.utils.calculators.AttributeCalculator;

public class ExperienceAttribute extends CappedAttribute {

    public ExperienceAttribute(long level) {
        super(level);
    }

    public ExperienceAttribute(long level, long value, long bonus) {
        super(level, value, bonus);
    }

    public ExperienceAttribute(long level, long value) {
        super(level, value);
    }

    @Override
    protected long calculateFromLevel(long level) {
        return AttributeCalculator.calculatePlayerExperience(level);
    }

    public void gainExperience(long amount, Experiencable experiencable) {
        do {
            long value = Math.min(amount, getMissing());
            add(value);

            amount -= value;

            if (getMissing() == 0) {
                experiencable.levelUp();

                update(experiencable.getLevel());
                empty();
            }

        } while (amount > 0);
    }
}
