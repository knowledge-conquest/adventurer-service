package com.jackeri.kc.models.entities.attributes;

import com.jackeri.kc.models.utils.calculators.AttributeCalculator;

public class AttackAttribute extends Attribute {

    public AttackAttribute(long level) {
        super(level);
    }

    public AttackAttribute(long level, long bonus) {
        super(level, bonus);
    }

    @Override
    protected long calculateFromLevel(long level) {
        return AttributeCalculator.calculateAttack(level);
    }
}
