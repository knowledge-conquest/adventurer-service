package com.jackeri.kc.models.entities.attributes;

import com.jackeri.kc.models.utils.calculators.AttributeCalculator;

public class HealthAttribute extends CappedAttribute {

    public HealthAttribute(long level) {
        super(level);
    }

    public HealthAttribute(long level, long value) {
        super(level, value);
    }

    public HealthAttribute(long level, long value, long bonus) {
        super(level, value, bonus);
    }

    @Override
    protected long calculateFromLevel(long level) {
        return AttributeCalculator.calculateHealth(level);
    }
}
