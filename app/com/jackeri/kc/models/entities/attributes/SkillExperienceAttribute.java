package com.jackeri.kc.models.entities.attributes;

import com.jackeri.kc.models.utils.calculators.AttributeCalculator;

public class SkillExperienceAttribute extends ExperienceAttribute {

    public SkillExperienceAttribute(long level) {
        super(level);
    }

    public SkillExperienceAttribute(long level, long value, long bonus) {
        super(level, value, bonus);
    }

    public SkillExperienceAttribute(long level, long value) {
        super(level, value);
    }

    @Override
    protected long calculateFromLevel(long level) {
        return AttributeCalculator.calculateSkillExperience(level);
    }
}
