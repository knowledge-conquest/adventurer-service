package com.jackeri.kc.models.entities;

import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.itemdata.WeaponType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class WeaponProficiency extends Document {

    private static final double DEFAULT_VALUE = 0.5;

    @Getter @Setter
    private Reference<Adventurer> adventurer;

    @Getter @Setter
    private WeaponType weaponType;

    @Getter @Setter
    private double value;

    public WeaponProficiency(String adventurerId, WeaponType weaponType) {
        this(adventurerId, weaponType, 0.0);
    }

    public WeaponProficiency(String adventurerId, WeaponType weaponType, double value) {
        this.adventurer = new Reference<>(adventurerId);
        this.weaponType = weaponType;
        this.value = DEFAULT_VALUE + value;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.WEAPON_PROFICIENCY;
    }

    /* Serialization helpers for API responses */

    @Override
    public WeaponProficiencyResponse toResponse() {
        return new WeaponProficiencyResponse();
    }

    public class WeaponProficiencyResponse extends DocumentResponse {

        @Getter @Setter
        private String adventurer;

        @Getter @Setter
        private WeaponType weaponType;

        @Getter @Setter
        private double value;

        protected WeaponProficiencyResponse() {
            adventurer = WeaponProficiency.this.getAdventurer().getKey();
            weaponType = WeaponProficiency.this.getWeaponType();
            value = WeaponProficiency.this.getValue();
        }
    }
}
