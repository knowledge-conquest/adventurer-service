package com.jackeri.kc.models.entities;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.entities.attributes.*;
import com.jackeri.kc.models.skills.Skill;
import com.jackeri.kc.models.utils.calculators.AttributeCalculator;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public abstract class Entity extends Document {

    @Getter
    @Setter
    protected String name;

    @Getter
    @Setter
    private long level;

    @JsonIgnore
    @Getter(AccessLevel.PROTECTED)
    private HealthAttribute healthAttribute;

    @JsonIgnore
    @Getter(AccessLevel.PROTECTED)
    private SpeedAttribute speedAttribute;

    @JsonIgnore
    @Getter(AccessLevel.PROTECTED)
    private AttackAttribute attackAttribute;

    @JsonIgnore
    @Getter(AccessLevel.PROTECTED)
    private DefenseAttribute defenseAttribute;

    public Entity(String name) {
        this(name, 1L);
    }

    public Entity(String name, long level) {
        this.name = name;
        this.level = level;
        this.healthAttribute = new HealthAttribute(level);
        this.speedAttribute = new SpeedAttribute(level);
        this.attackAttribute = new AttackAttribute(level);
        this.defenseAttribute = new DefenseAttribute(level);
    }

    public Entity(String name, long level, long health) {
        this(name, level, health, 0L, 0L, 0L, 0L);
    }

    public Entity(String name, long level, long health, long healthBonus,
                  long speedBonus, long attackBonus,  long defenseBonus) {
        this.name = name;
        this.level = level;
        this.healthAttribute = new HealthAttribute(level, health, healthBonus);
        this.speedAttribute = new SpeedAttribute(level, speedBonus);
        this.attackAttribute = new AttackAttribute(level, attackBonus);
        this.defenseAttribute = new DefenseAttribute(level, defenseBonus);
    }

    /* Game logic helpers */

    @JsonIgnore
    public boolean isAlive() {
        return !healthAttribute.isEmpty();
    }

    public boolean isQuickerThan(Entity entity) {
        return getSpeed() >= entity.getSpeed();
    }

    public abstract long launchAttack(Entity target, Skill skill);

    protected long receiveAttack(long damage) {
        long effectiveDamage = Math.max(1, damage - getDefense());
        healthAttribute.sub(effectiveDamage);
        return effectiveDamage;
    }

    @JsonIgnore
    public void heal() {
        healthAttribute.fill();
    }

    @JsonIgnore
    public void heal(long health) {
        healthAttribute.add(health);
    }

    @JsonGetter
    public long getHealth() {
        return healthAttribute.getValue();
    }

    @JsonIgnore
    public long getMaxHealth() {
        return healthAttribute.getMax();
    }

    @JsonIgnore
    public long getSpeed() {
        return speedAttribute.getValue();
    }

    @JsonIgnore
    public long getAttack() {
        return attackAttribute.getValue();
    }

    @JsonIgnore
    public long getDefense() {
        return defenseAttribute.getValue();
    }

    @JsonIgnore
    public long getExperienceDrop() {
        return AttributeCalculator.calculateExperienceDrop(getLevel());
    }

    /* Serialization helpers for API responses */

    @Override
    public abstract EntityResponse toResponse();

    public abstract class EntityResponse extends DocumentResponse {

        @Getter
        private final String name;

        @Getter
        private final long level;

        @Getter
        private final CappedAttribute health;

        @Getter
        private final long speed;

        @Getter
        private final long attack;

        @Getter
        private final long defense;

        protected EntityResponse() {
            name = Entity.this.getName();
            level = Entity.this.getLevel();
            health = Entity.this.getHealthAttribute();
            speed = Entity.this.getSpeed();
            attack = Entity.this.getAttack();
            defense = Entity.this.getDefense();
        }
    }
}