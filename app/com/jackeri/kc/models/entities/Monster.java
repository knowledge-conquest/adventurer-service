package com.jackeri.kc.models.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.skills.Skill;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Monster extends Entity {

    @Getter @Setter
    private Reference<MonsterData> monsterData;

    public Monster(MonsterData monsterData) {
        super(monsterData.getName(), monsterData.getLevel());
        this.monsterData = new Reference<>(monsterData.getId());
    }

    @JsonCreator
    private Monster(@JsonProperty("name") String name, @JsonProperty("level") long level,
                    @JsonProperty("health") long health,
                    @JsonProperty("monsterData") Reference<MonsterData> monsterData) {
        super(name, level, health);
        this.monsterData = monsterData;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.MONSTER;
    }

    @Override
    public long launchAttack(Entity target, Skill skill) {
        return target.receiveAttack(getAttack());
    }

    /* Serialization helpers for API responses */

    @Override
    public MonsterResponse toResponse() {
        return new MonsterResponse();
    }

    public class MonsterResponse extends EntityResponse {

        @Getter
        private final String monsterDataKey;

        protected MonsterResponse() {
            monsterDataKey = Monster.this.getMonsterData().getKey();
        }
    }

}
