package com.jackeri.kc.models.skills;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum SkillTier {
    LOW(1, 1.2), MEDIUM(3, 1.5), HIGH(5, 2.0);

    @Getter
    private int cooldown;

    @Getter
    private double modifier;
}
