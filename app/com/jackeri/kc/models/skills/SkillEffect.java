package com.jackeri.kc.models.skills;

public enum SkillEffect {
    HEAL, DAMAGE
}
