package com.jackeri.kc.models.skills;

import com.fasterxml.jackson.annotation.*;
import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.entities.attributes.Experiencable;
import com.jackeri.kc.models.entities.attributes.SkillExperienceAttribute;
import com.jackeri.kc.models.entities.attributes.SkillValueAttribute;
import com.jackeri.kc.models.itemdata.WeaponType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Skill extends Document implements Experiencable {

    @Getter @Setter
    private long level;

    @JsonIgnore
    @Getter(AccessLevel.PROTECTED)
    private SkillExperienceAttribute experienceAttribute;

    @JsonIgnore
    @Getter(AccessLevel.PROTECTED)
    private SkillValueAttribute valueAttribute;

    @Getter @Setter
    private SkillEffect skillEffect;

    @Getter @Setter
    private SkillTier skillTier;

    @Getter @Setter
    private WeaponType weaponType;

    @Getter @Setter
    private int cooldown;

    @JsonIgnore
    private Integer slotIndex;

    public Skill(SkillEffect skillEffect, SkillTier skillTier, WeaponType weaponType) {
        this.skillEffect = skillEffect;
        this.skillTier = skillTier;
        this.weaponType = weaponType;
        this.level = 1L;
        this.experienceAttribute = new SkillExperienceAttribute(this.level, 0L);
        this.valueAttribute = new SkillValueAttribute(skillEffect, skillTier, this.level);
        this.cooldown = 0;
        this.slotIndex = null;
    }

    @JsonCreator
    public Skill(@JsonProperty("level") long level, @JsonProperty("experience") long experience,
                 @JsonProperty("skillEffect") SkillEffect skillEffect, @JsonProperty("skillTier") SkillTier skillTier,
                 @JsonProperty("weaponType") WeaponType weaponType, @JsonProperty("cooldown") int cooldown,
                 @JsonProperty("slotIndex") Integer slotIndex) {
        this.level = level;
        this.experienceAttribute = new SkillExperienceAttribute(level, experience);
        this.valueAttribute = new SkillValueAttribute(skillEffect, skillTier, this.level);
        this.skillEffect = skillEffect;
        this.skillTier = skillTier;
        this.weaponType = weaponType;
        this.cooldown = cooldown;
        this.slotIndex = slotIndex;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.SKILL;
    }

    @JsonGetter
    public long getExperience() {
        return experienceAttribute.getValue();
    }

    @JsonIgnore
    public long getMaxExperience() {
        return experienceAttribute.getMax();
    }

    public void gainExperience(long amount) {
        getExperienceAttribute().gainExperience(amount, this);
    }

    @Override
    public void levelUp() {
        setLevel(getLevel() + 1);
    }

    @JsonIgnore
    public long getValue() {
        return getValueAttribute().getValue();
    }

    @JsonIgnore
    public Integer getSlotIndex() {
        return slotIndex;
    }

    @JsonSetter
    public void setSlotIndex(Integer slotIndex) {
        this.slotIndex = slotIndex;
    }

    @Override
    public SkillResponse toResponse() {
        return new SkillResponse();
    }

    public class SkillResponse extends DocumentResponse {

        @Getter
        private final long level;

        @Getter
        private final SkillExperienceAttribute experience;

        @Getter
        private final long value;

        @Getter
        private final SkillEffect skillEffect;

        @Getter
        private final SkillTier skillTier;

        @Getter
        private final WeaponType weaponType;

        @Getter
        private final int cooldown;

        @Getter
        private final int currentCooldown;

        @Getter
        private final Integer slotIndex;

        public SkillResponse() {
            this.level = Skill.this.getLevel();
            this.experience = Skill.this.getExperienceAttribute();
            this.value = Skill.this.getValue();
            this.skillEffect = Skill.this.getSkillEffect();
            this.skillTier = Skill.this.getSkillTier();
            this.weaponType = Skill.this.getWeaponType();
            this.cooldown = Skill.this.getSkillTier().getCooldown();
            this.currentCooldown = Skill.this.getCooldown();
            this.slotIndex = Skill.this.getSlotIndex();
        }
    }
}
