package com.jackeri.kc.models.skills;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Edge;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.entities.Adventurer;
import com.jackeri.kc.models.entities.Entity;
import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.models.items.ItemOwnership;
import lombok.Getter;
import lombok.Setter;

public class SkillOwnership extends Edge {

    @Getter @Setter
    @JsonProperty("_from")
    private Reference<Adventurer> adventurer;

    @Getter @Setter
    @JsonProperty("_to")
    private Reference<Skill> skill;

    @Getter @Setter
    private boolean equipped;

    @Getter @Setter
    private Integer slotIndex;

    public SkillOwnership(String adventurer, String skill) {
        this.adventurer = new Reference<>(adventurer);
        this.skill = new Reference<>(skill);
        this.equipped = false;
        this.slotIndex = null;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.SKILL_OWNERSHIP;
    }

    @Override
    public SkillOwnershipResponse toResponse() {
        return new SkillOwnershipResponse();
    }

    public class SkillOwnershipResponse extends EdgeResponse {
        @Getter
        private final String adventurerKey;

        @Getter
        private final Skill.SkillResponse skill;

        @Getter
        private final boolean equipped;

        @Getter
        private final Integer slotIndex;

        public SkillOwnershipResponse() {
            adventurerKey = SkillOwnership.this.getAdventurer().getKey();
            skill = SkillOwnership.this.getSkill().getDocument().toResponse();
            equipped = SkillOwnership.this.isEquipped();
            slotIndex = SkillOwnership.this.getSlotIndex();
        }
    }
}
