package com.jackeri.kc.models.fights.fightlogs;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Join extends FightLog {

    @Getter
    private final String entity;

    @Getter
    private final boolean attacker;

    @Override
    public Type getType() {
        return Type.JOIN;
    }
}
