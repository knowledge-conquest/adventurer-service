package com.jackeri.kc.models.fights.fightlogs;

import com.jackeri.kc.models.items.Item;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Vector;

@AllArgsConstructor
public class Loot extends FightLog {

    @Getter
    private final String entity;

    @Getter
    private final Vector<Item.ItemResponse> loots;

    @Override
    public Type getType() {
        return Type.LOOT;
    }
}
