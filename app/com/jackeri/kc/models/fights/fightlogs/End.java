package com.jackeri.kc.models.fights.fightlogs;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class End extends FightLog {

    @Getter
    private final int winner;

    @Override
    public Type getType() {
        return Type.END;
    }
}
