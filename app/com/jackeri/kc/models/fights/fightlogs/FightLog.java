package com.jackeri.kc.models.fights.fightlogs;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(name = "START", value = Start.class),
        @JsonSubTypes.Type(name = "END", value = End.class),
        @JsonSubTypes.Type(name = "JOIN", value = Join.class),
        @JsonSubTypes.Type(name = "ATTACK", value = Attack.class),
        @JsonSubTypes.Type(name = "SKILL", value = Skill.class),
        @JsonSubTypes.Type(name = "ESCAPE", value = Escape.class),
        @JsonSubTypes.Type(name = "DEATH", value = Death.class),
        @JsonSubTypes.Type(name = "LOOT", value = Loot.class),
        @JsonSubTypes.Type(name = "EXPERIENCE", value = Experience.class),
        @JsonSubTypes.Type(name = "NEXT_TURN", value = NextTurn.class),
})
public abstract class FightLog {

    public enum Type {
        START, END, JOIN, ATTACK, SKILL, ESCAPE, DEATH, LOOT, EXPERIENCE, NEXT_TURN
    }

    @JsonGetter
    public abstract Type getType();
}
