package com.jackeri.kc.models.fights.fightlogs;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Experience extends FightLog {

    @Getter
    private final String entity;

    @Getter
    private final long experience;

    @Override
    public Type getType() {
        return Type.EXPERIENCE;
    }
}
