package com.jackeri.kc.models.fights.fightlogs;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Attack extends FightLog {

    @Getter
    private final String entity;

    @Getter
    private final String target;

    @Getter
    private final long damage;

    @Override
    public Type getType() {
        return Type.ATTACK;
    }
}
