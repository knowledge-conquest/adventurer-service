package com.jackeri.kc.models.fights.fightlogs;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class NextTurn extends FightLog {

    @Override
    public Type getType() {
        return Type.NEXT_TURN;
    }
}
