package com.jackeri.kc.models.fights.fightlogs;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Skill extends FightLog {

    @Getter
    private final String entity;

    @Getter
    private final String target;

    @Getter
    private final String skill;

    @Getter
    private final long value;

    @Override
    public Type getType() {
        return Type.SKILL;
    }
}
