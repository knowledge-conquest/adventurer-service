package com.jackeri.kc.models.fights.fightlogs;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Death extends FightLog {

    @Getter
    private final String entity;

    @Override
    public Type getType() {
        return Type.DEATH;
    }
}
