package com.jackeri.kc.models.fights.fightlogs;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Start extends FightLog {

    @Override
    public Type getType() {
        return Type.START;
    }
}
