package com.jackeri.kc.models.fights;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.areas.Area;
import com.jackeri.kc.models.entities.Adventurer;
import com.jackeri.kc.models.entities.Entity;
import com.jackeri.kc.models.fights.fightlogs.Experience;
import com.jackeri.kc.models.fights.fightlogs.FightLog;
import com.jackeri.kc.models.fights.fightlogs.Join;
import com.jackeri.kc.models.fights.fightlogs.Start;
import com.jackeri.kc.models.participations.FightParticipation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
public class Fight extends Document {

    public enum FightType {
        MINION, BOSS
    }

    @Getter
    private Vector<FightLog> logs;

    @Getter @Setter
    @JsonSetter
    private Vector<Reference<Entity>> attackers;

    @Getter @Setter
    @JsonSetter
    private Vector<Reference<Entity>> defenders;

    @Getter @Setter
    private Reference<Area> area;

    @Getter @Setter
    private FightType fightType;

    public Fight(Vector<String> attackers, Vector<String> defenders, String area, FightType fightType) {
        this.logs = new Vector<>();

        this.attackers = new Vector<>();
        for (String attacker : attackers) {
            this.attackers.add(new Reference<>(attacker));
            logs.add(new Join(attacker.split("/")[1], true));
        }

        this.defenders = new Vector<>();
        for (String defender : defenders) {
            this.defenders.add(new Reference<>(defender));
            logs.add(new Join(defender.split("/")[1], false));
        }

        logs.add(new Start());

        this.area = new Reference<>(area);
        this.fightType = fightType;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.FIGHT;
    }

    /* Game logic helpers */

    public Vector<FightParticipation> extractParticipation() {
        Vector<FightParticipation> participation = new Vector<>();
        for (Reference<Entity> entity : getAttackers()) {
            participation.add(new FightParticipation(entity.getId(), getId(), true));
        }

        for (Reference<Entity> entity : getDefenders()) {
            participation.add(new FightParticipation(entity.getId(), getId(), false));
        }

        return participation;
    }

    @JsonIgnore
    public Vector<Reference<Entity>> getEntities() {
        Vector<Reference<Entity>> entities = new Vector<>();
        entities.addAll(getAttackers());
        entities.addAll(getDefenders());
        return entities;
    }

    public boolean hasEntity(String key) {
        return hasEntity(key, getAttackers()) || hasEntity(key, getDefenders());
    }

    public boolean onSameTeam(String keyA, String keyB) {
        return hasEntity(keyA, getAttackers()) && hasEntity(keyB, getAttackers())
                || hasEntity(keyA, getDefenders()) && hasEntity(keyB, getDefenders());
    }

    @JsonIgnore
    public boolean hasEnded() {
        boolean attackersDead = true;
        for (Reference<Entity> entity : getAttackers()) {
            if (entity.getDocument().isAlive()) {
                attackersDead = false;
                break;
            }
        }

        if (attackersDead) {
            return true;
        }

        for (Reference<Entity> entity : getDefenders()) {
            if (entity.getDocument().isAlive()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Return the winner.
     * @return 1 if attackers won, -1 if defenders won or 0 otherwise.
     */
    @JsonIgnore
    public int getWinner() {
        boolean attackersAlive = false;
        for (Reference<Entity> entity : getAttackers()) {
            if (entity.getDocument().isAlive()) {
                attackersAlive = true;
                break;
            }
        }

        boolean defendersAlive = false;
        for (Reference<Entity> entity : getDefenders()) {
            if (entity.getDocument().isAlive()) {
                defendersAlive = true;
                break;
            }
        }

        if (attackersAlive == defendersAlive) {
            // return 0 if they both died or are both alive
            return 0;
        } else if (attackersAlive) {
            // attackers won
            return 1;
        } else {
            //defenders won
            return -1;
        }
    }

    public Vector<Adventurer> gainExperience() {
        int winner = getWinner();
        if (winner == 0) return new Vector<>();

        Vector<Reference<Entity>> winners = winner > 0 ? getAttackers() : getDefenders();
        Vector<Reference<Entity>> looser = winner > 0 ? getDefenders() : getAttackers();

        Vector<Adventurer> adventurers = new Vector<>();
        for (Reference<Entity> entity : winners) {
            if (entity.getDocument().getDocumentType() == DocumentType.ADVENTURER) {
                adventurers.add((Adventurer) entity.getDocument());
            }
        }

        if (!adventurers.isEmpty()) {
            long experience = 0;
            for (Reference<Entity> entity : looser) {
                experience += entity.getDocument().getExperienceDrop();
            }

            experience /= adventurers.size();

            for (Adventurer adventurer : adventurers) {
                adventurer.gainExperience(experience);
                getLogs().add(new Experience(adventurer.getKey(), experience));
            }
        }

        return adventurers;
    }

    public Reference<Entity> getEntity(String key) {
        String id = CollectionType.ENTITIES.getId(key);

        for (Reference<Entity> entity : getAttackers()) {
            if (id.equals(entity.getId())) {
                return entity;
            }
        }

        for (Reference<Entity> entity : getDefenders()) {
            if (id.equals(entity.getId())) {
                return entity;
            }
        }

        return null;
    }

    private boolean hasEntity(String key, Vector<Reference<Entity>> entities) {
        for (Reference<Entity> entity : entities) {
            if (CollectionType.ENTITIES.getId(key).equals(entity.getId())) {
                return true;
            }
        }

        return false;
    }

    public FightEnded toFightEnded() {
        Vector<Entity> attackers = new Vector<>();
        getAttackers().forEach(attacker -> attackers.add(attacker.getDocument()));

        Vector<Entity> defenders = new Vector<>();
        getAttackers().forEach(attacker -> attackers.add(attacker.getDocument()));

        return new FightEnded(getLogs(), attackers, defenders, getFightType());
    }

    /* Serialization helpers for API responses */

    @Override
    public FightResponse toResponse() {
        return new FightResponse();
    }

    @JsonIgnore
    public Vector<Reference<Entity>> getWinners() {
        int winner = getWinner();

        if (winner > 0) {
            return getAttackers();
        }

        if (winner < 0) {
            return getDefenders();
        }

        return new Vector<>();
    }

    public class FightResponse extends DocumentResponse {

        @Getter
        private final FightLog[] logs;

        @Getter
        private final Entity.EntityResponse[] attackers;

        @Getter
        private final Entity.EntityResponse[] defenders;

        @Getter
        private final String area;

        @Getter
        private final FightType fightType;

        public FightResponse() {
            logs = Fight.this.getLogs().toArray(new FightLog[0]);
            attackers = createEntityResponseArray(Fight.this.getAttackers());
            defenders = createEntityResponseArray(Fight.this.getDefenders());
            area = Fight.this.getId();
            fightType = Fight.this.getFightType();
        }

        private Entity.EntityResponse[] createEntityResponseArray(Vector<Reference<Entity>> entities) {
            Entity.EntityResponse[] array = new Entity.EntityResponse[entities.size()];
            for (int i = 0; i < array.length; ++i) {
                // TODO: Implement error for missing documents (currently throws null pointer exception)
                array[i] = entities.get(i).getDocument().toResponse();
            }
            return array;
        }
    }
}
