package com.jackeri.kc.models.fights;

import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.entities.Entity;
import com.jackeri.kc.models.fights.fightlogs.FightLog;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Vector;

@NoArgsConstructor
public class FightEnded extends Document {

    @Getter
    private Vector<FightLog> logs;

    @Getter @Setter
    private Vector<Entity> attackers;

    @Getter @Setter
    private Vector<Entity> defenders;

    @Getter @Setter
    private Fight.FightType fightType;

    public FightEnded(Vector<FightLog> logs, Vector<Entity> attackers, Vector<Entity> defenders, Fight.FightType fightType) {
        this.logs = logs;
        this.attackers = attackers;
        this.defenders = defenders;
        this.fightType = fightType;
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.FIGHT_ENDED;
    }

    /* Serialization helpers for API responses */

    @Override
    public FightEndedResponse toResponse() {
        return new FightEndedResponse();
    }

    public class FightEndedResponse extends DocumentResponse {
        @Getter
        private FightLog[] logs;

        @Getter
        private Vector<Entity.EntityResponse> attackers;

        @Getter
        private Vector<Entity.EntityResponse> defenders;

        @Getter
        private final Fight.FightType fightType;

        public FightEndedResponse() {
            logs = FightEnded.this.getLogs().toArray(new FightLog[0]);
            attackers = createEntityResponseArray(FightEnded.this.getAttackers());
            defenders = createEntityResponseArray(FightEnded.this.getDefenders());
            fightType = FightEnded.this.getFightType();
        }

        private Vector<Entity.EntityResponse> createEntityResponseArray(Vector<Entity> entities) {
            Vector<Entity.EntityResponse> responses = new Vector<>(entities.size());
            entities.forEach(entity -> responses.add(entity.toResponse()));
            return responses;
        }
    }
}
