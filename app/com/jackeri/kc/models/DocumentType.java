package com.jackeri.kc.models;

import lombok.Getter;

public enum DocumentType {
    // Entities
    ADVENTURER(CollectionType.ENTITIES),
    WEAPON_PROFICIENCY(CollectionType.WEAPON_PROFICIENCIES),
    MONSTER(CollectionType.ENTITIES),
    MONSTER_DATA(CollectionType.MONSTER_DATA),
    MONSTER_LOOT(CollectionType.ITEM_DATA_EDGES),

    // Item Data
    RESOURCE_DATA(CollectionType.ITEM_DATA),
    WEAPON_DATA(CollectionType.ITEM_DATA),
    ARMOUR_DATA(CollectionType.ITEM_DATA),

    // Items
    RESOURCE(CollectionType.ITEMS),
    WEAPON(CollectionType.ITEMS),
    ARMOUR(CollectionType.ITEMS),
    ITEM_OWNERSHIP(CollectionType.ITEM_OWNERSHIPS),
    WORN_EQUIPMENT(CollectionType.WORN_EQUIPMENTS),

    // Skills
    SKILL(CollectionType.SKILLS),
    SKILL_OWNERSHIP(CollectionType.SKILL_OWNERSHIPS),

    // Realms
    REALM(CollectionType.REALMS),

    // Regions
    REGION(CollectionType.REGIONS),

    // Areas
    SPAWN_AREA(CollectionType.AREAS),
    CRAFTING_AREA(CollectionType.AREAS),
    GATHERING_AREA(CollectionType.AREAS),
    TRAINING_AREA(CollectionType.AREAS),
    SKILL_PRACTICE_AREA(CollectionType.AREAS),
    CULTIVATION_AREA(CollectionType.AREAS),
    LOOT_TREASURE_AREA(CollectionType.AREAS),
    SKILL_TREASURE_AREA(CollectionType.AREAS),
    TELEPORTATION_AREA(CollectionType.AREAS),
    MONSTER_SPAWNER_AREA(CollectionType.AREAS),
    MONSTER_BOSS_AREA(CollectionType.AREAS),
    AREA_PATH(CollectionType.AREA_PATHS),
    BOSS_TELEPORTATION(CollectionType.BOSS_TELEPORTATIONS),

    // Fights
    FIGHT(CollectionType.FIGHTS),
    FIGHT_ENDED(CollectionType.FIGHTS),
    FIGHT_PARTICIPATION(CollectionType.ACTION_PARTICIPATION),

    // Recipes
    RECIPE(CollectionType.RECIPES),
    RECIPE_COMPONENT(CollectionType.ITEM_DATA_EDGES),

    // Gathering
    GATHERING_LOOT(CollectionType.ITEM_DATA_EDGES),

    // Treasure
    TREASURE_LOOT(CollectionType.ITEM_DATA_EDGES),

    // Idle Actions
    CULTIVATION_PARTICIPATION(CollectionType.ACTION_PARTICIPATION),
    TRAINING_PARTICIPATION(CollectionType.ACTION_PARTICIPATION),
    SKILL_PRACTICE_PARTICIPATION(CollectionType.ACTION_PARTICIPATION),
    SPAWN_RECOVERY_PARTICIPATION(CollectionType.ACTION_PARTICIPATION),
    ;

    @Getter
    private final CollectionType collectionType;

    DocumentType(CollectionType collectionType) {
        this.collectionType = collectionType;
    }
}