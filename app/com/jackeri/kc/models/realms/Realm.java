package com.jackeri.kc.models.realms;

import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.entities.Adventurer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
public class Realm extends Document {

    @Getter @Setter
    private Reference<Adventurer> owner;

    @Getter @Setter
    private Date creationDate;

    public Realm(String ownerId) {
        this.owner = new Reference<>(ownerId);
        this.creationDate = new Date();
    }

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.REALM;
    }

    /* Serialization helpers for API responses */

    @Override
    public RealmResponse toResponse() {
        return new RealmResponse();
    }

    public class RealmResponse extends DocumentResponse {

        @Getter
        private String ownerKey;

        @Getter
        private Date creationDate;

        protected RealmResponse() {
            ownerKey = Realm.this.getOwner().getKey();
            creationDate = Realm.this.getCreationDate();
        }
    }
}
