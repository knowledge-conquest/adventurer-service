package com.jackeri.kc.repositories;

import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.participations.ActionState;
import com.jackeri.kc.models.participations.idle.IdleActionParticipation;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class IdleRepositiory extends Repository {

    @Inject
    public IdleRepositiory(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.ACTION_PARTICIPATION;
    }

    public CompletableFuture<IdleActionParticipation> get(String adventurerKey, ActionState actionState) {
        return getDatabase().query(
                "FOR action IN @@actions " +
                        "FILTER action._from == @adventurerId && action.actionState == @actionState LIMIT 1 " +
                        "LET area = DOCUMENT(action._to) " +
                        "LET region = DOCUMENT(area.region) " +
                        "RETURN merge(action, {duration: DATE_DIFF(action.startedOn, DATE_NOW(), 's'), _to: merge(area, {region})}) ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("actionState", actionState)
                        .put("@actions", getCollectionType().getName())
                        .get(),
                IdleActionParticipation.class
        ).thenApply(result -> {
            IdleActionParticipation actionParticipation = result.hasNext() ? result.next() : null;
            return actionParticipation;
        });
    }

    public CompletableFuture<IdleActionParticipation> start(IdleActionParticipation action, DocumentType areaType) {
        return getDatabase().query(
                "FOR adventurer IN @@entities " +
                        "FILTER adventurer._id == @action._from " +
                        "FILTER adventurer.location " +
                        "LET area = DOCUMENT(adventurer.location) " +
                        "LET region = DOCUMENT(area.region) " +
                        "FILTER area.type == @areaType " +
                        "INSERT merge(@action, {_to: area._id, startedOn: DATE_NOW()}) INTO @@actions " +
                        "RETURN merge(NEW, {_to: merge(area, {region})}) ",
                new MapBuilder()
                        .put("areaType", areaType)
                        .put("action", action)
                        .put("@entities", CollectionType.ENTITIES.getName())
                        .put("@actions", getCollectionType().getName())
                        .get(),
                IdleActionParticipation.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<IdleActionParticipation> stop(String adventurerKey, ActionState actionState) {
        return getDatabase().query(
                "FOR area, action IN 1..1 OUTBOUND @adventurerId @@actions " +
                        "FILTER action.actionState == @actionState LIMIT 1 " +
                        "LET region = DOCUMENT(area.region) " +
                        "REMOVE action._key IN @@actions " +
                        "RETURN merge(OLD, {_to: merge(area, {region}), duration: DATE_DIFF(OLD.startedOn, DATE_NOW(), 's')}) ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("actionState", actionState)
                        .put("@actions", getCollectionType().getName())
                        .get(),
                IdleActionParticipation.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }
}
