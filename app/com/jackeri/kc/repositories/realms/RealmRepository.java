package com.jackeri.kc.repositories.realms;

import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;

public class RealmRepository extends Repository {

    @Inject
    public RealmRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.REALMS;
    }
}
