package com.jackeri.kc.repositories.areas;

import com.arangodb.util.MapBuilder;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.concurrent.CompletableFuture;

public class TeleportationAreaRepository extends AreaRepository {

    @Inject
    public TeleportationAreaRepository(ArangoAsyncService arango) {
        super(arango);
    }

    public CompletableFuture<Boolean> setDestination(String areaId, String destinationId) {
        return getDatabase().query(
                "FOR area IN Areas FILTER area._id == @areaId LIMIT 1" +
                        "UPDATE area._key WITH { destination: @destinationId } IN Areas " +
                        "RETURN true",
                new MapBuilder()
                        .put("areaId", areaId)
                        .put("destinationId", destinationId)
                        .get(),
                Boolean.class
        ).thenApply(Iterator::hasNext);
    }
}
