package com.jackeri.kc.repositories.areas;

import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.util.MapBuilder;
import com.fasterxml.jackson.databind.JsonNode;
import com.jackeri.kc.generation.map.MapGraph;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;
import com.jackeri.kc.utils.V2Int;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;

public class AreaRepository extends Repository {

    @Inject
    public AreaRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.AREAS;
    }

    public CompletableFuture<Void> setPositions(Vector<MapGraph.Vertex> vertices) {
        return getDatabase().query(
                "FOR d IN @data " +
                        "UPDATE PARSE_IDENTIFIER(d.id).key WITH {position: d.position} IN @@areas ",
                new MapBuilder()
                        .put("data", vertices)
                        .put("@areas", CollectionType.AREAS.getName())
                        .get(),
                String.class
        ).thenApply(result -> null);
    }
}
