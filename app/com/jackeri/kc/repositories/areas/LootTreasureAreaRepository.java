package com.jackeri.kc.repositories.areas;

import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.areas.treasure.LootTreasureArea;
import com.jackeri.kc.models.areas.treasure.TreasureArea;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class LootTreasureAreaRepository extends AreaRepository {

    @Inject
    public LootTreasureAreaRepository(ArangoAsyncService arango) {
        super(arango);
    }

    public CompletableFuture<LootTreasureArea> getAreaForAdventurer(String adventurerKey) {
        return getDatabase().query(
                "FOR adventurer IN @@entities " +
                        "FILTER adventurer._key == @adventurerKey && adventurer.type == @entityType " +
                        "FOR area IN @@areas " +
                        "FILTER area._id == adventurer.location && area.type == @areaType LIMIT 1 " +
                        "LET loots = (FOR i, e IN 1..1 INBOUND area._id @@edges " +
                        "   RETURN {a: i, b: e.quantity}) " +
                        "RETURN merge(area, {loots}) ",
                new MapBuilder()
                        .put("@entities", CollectionType.ENTITIES.getName())
                        .put("@areas", CollectionType.AREAS.getName())
                        .put("adventurerKey", adventurerKey)
                        .put("entityType", DocumentType.ADVENTURER)
                        .put("areaType", DocumentType.LOOT_TREASURE_AREA)
                        .put("@edges", CollectionType.ITEM_DATA_EDGES.getName())
                        .get(),
                LootTreasureArea.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<LootTreasureArea> openAndGetAreaForAdventurer(String adventurerKey) {
        return getDatabase().query(
                "FOR adventurer IN @@entities " +
                        "FILTER adventurer._key == @adventurerKey && adventurer.type == @entityType LIMIT 1" +
                        "FOR area IN @@areas " +
                        "FILTER area._id == adventurer.location && area.type == @areaType LIMIT 1" +
                        "FILTER area.opened == false " +
                        "LET loots = (FOR i, e IN 1..1 INBOUND area._id @@edges " +
                        "   RETURN {a: i, b: e.quantity}) " +
                        "UPDATE area._key WITH {opened: true} IN @@areas " +
                        "let openedArea = NEW " +
                        "RETURN merge(openedArea, {loots}) ",
                new MapBuilder()
                        .put("@entities", CollectionType.ENTITIES.getName())
                        .put("@areas", CollectionType.AREAS.getName())
                        .put("adventurerKey", adventurerKey)
                        .put("entityType", DocumentType.ADVENTURER)
                        .put("areaType", DocumentType.LOOT_TREASURE_AREA)
                        .put("@edges", CollectionType.ITEM_DATA_EDGES.getName())
                        .get(),
                LootTreasureArea.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }
}
