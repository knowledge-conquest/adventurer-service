package com.jackeri.kc.repositories.areas;

import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.areas.treasure.SkillTreasureArea;
import com.jackeri.kc.models.skills.SkillOwnership;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class SkillTreasureAreaRepository extends AreaRepository {

    @Inject
    public SkillTreasureAreaRepository(ArangoAsyncService arango) {
        super(arango);
    }

    public CompletableFuture<SkillTreasureArea> getAreaForAdventurer(String adventurerKey) {
        return getDatabase().query(
                "LET adventurer = DOCUMENT(@adventurerId) " +
                        "FILTER adventurer && adventurer.type == @entityType " +
                        "LET area = DOCUMENT(adventurer.location) " +
                        "FILTER area && area.type == @areaType " +
                        "RETURN area ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("entityType", DocumentType.ADVENTURER)
                        .put("areaType", DocumentType.SKILL_TREASURE_AREA)
                        .get(),
                SkillTreasureArea.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<SkillTreasureArea> openAndGetAreaForAdventurer(String adventurerKey) {
        String adventurerId = CollectionType.ENTITIES.getId(adventurerKey);
        SkillOwnership skillOwnership = new SkillOwnership(adventurerId, null);
        return getDatabase().query(
                "LET adventurer = DOCUMENT(@adventurerId) " +
                        "FILTER adventurer && adventurer.type == @entityType " +
                        "LET area = DOCUMENT(adventurer.location) " +
                        "FILTER area && area.type == @areaType " +
                        "FILTER area.opened == false " +
                        "INSERT area.skill INTO @@skills LET skill = NEW " +
                        "INSERT merge(@ownership, {_to: skill._id}) INTO @@ownerships " +
                        "UPDATE area._key WITH {opened: true} IN @@areas " +
                        "let openedArea = NEW " +
                        "RETURN merge(openedArea, {skill}) ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("ownership", skillOwnership)
                        .put("@ownerships", CollectionType.SKILL_OWNERSHIPS.getName())
                        .put("@areas", CollectionType.AREAS.getName())
                        .put("@skills", CollectionType.SKILLS.getName())
                        .put("entityType", DocumentType.ADVENTURER)
                        .put("areaType", DocumentType.SKILL_TREASURE_AREA)
                        .get(),
                SkillTreasureArea.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }
}
