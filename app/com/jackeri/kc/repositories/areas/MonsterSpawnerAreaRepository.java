package com.jackeri.kc.repositories.areas;

import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.areas.monster.MonsterArea;
import com.jackeri.kc.models.areas.monster.MonsterSpawnerArea;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class MonsterSpawnerAreaRepository extends AreaRepository {

    @Inject
    public MonsterSpawnerAreaRepository(ArangoAsyncService arango) {
        super(arango);
    }

    public CompletableFuture<MonsterArea> fetchMonsterAreaForAdventurer(String adventurerKey) {
        return getDatabase().query(
                "FOR adventurer IN Entities " +
                        "    FILTER adventurer._id == @adventurerId && adventurer.type == 'ADVENTURER' " +
                        "    FOR area IN Areas " +
                        "        FILTER area._id == adventurer.location " +
                        "        FILTER area.type == 'MONSTER_SPAWNER_AREA' " +
                        "           || area.type == 'MONSTER_BOSS_AREA' " +
                        "        RETURN merge(area, {monsterData: DOCUMENT(area.monsterData)})",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .get(),
                MonsterArea.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }
}
