package com.jackeri.kc.repositories.areas;

import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;

public class AreaPathRepository extends Repository {

    @Inject
    public AreaPathRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.AREA_PATHS;
    }
}
