package com.jackeri.kc.repositories.areas;

import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.areas.GatheringArea;
import com.jackeri.kc.models.itemdata.edges.GatheringLoot;
import com.jackeri.kc.services.ArangoAsyncService;
import com.jackeri.kc.utils.Pair;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class GatheringAreaRepository extends AreaRepository {

    @Inject
    public GatheringAreaRepository(ArangoAsyncService arango) {
        super(arango);
    }

    public CompletableFuture<GatheringArea> getAreaForAdventurer(String adventurerKey) {
        return getDatabase().query(
                "FOR adventurer IN @@entities " +
                        "FILTER adventurer._key == @adventurerKey && adventurer.type == @entityType " +
                        "FOR area IN @@areas " +
                        "FILTER area._id == adventurer.location && area.type == @areaType LIMIT 1 " +
                        "LET loots = (FOR i, e IN 1..1 INBOUND area._id @@edges " +
                        "   RETURN {a: i, b: e.quantity}) " +
                        "RETURN merge(area, {loots}) ",
                new MapBuilder()
                        .put("@entities", CollectionType.ENTITIES.getName())
                        .put("@areas", CollectionType.AREAS.getName())
                        .put("adventurerKey", adventurerKey)
                        .put("entityType", DocumentType.ADVENTURER)
                        .put("areaType", DocumentType.GATHERING_AREA)
                        .put("@edges", CollectionType.ITEM_DATA_EDGES.getName())
                        .get(),
                GatheringArea.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<Stream<String>> storeGenerations(Vector<Pair<GatheringArea, Vector<GatheringLoot>>> areas) {
        return getDatabase().query(
                "FOR d IN @data " +
                        "INSERT d.a INTO @@collection " +
                        "LET id = NEW._id " +
                        "LET bs = (FOR b IN d.b " +
                        "   INSERT merge(b, {_to: id}) INTO @@edges) " +
                        "RETURN id ",
                new MapBuilder()
                        .put("data", areas)
                        .put("@collection", CollectionType.AREAS.getName())
                        .put("@edges", CollectionType.ITEM_DATA_EDGES.getName())
                        .get(),
                String.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }
}
