package com.jackeri.kc.repositories.areas;

import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.areas.monster.MonsterArea;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class MonsterBossAreaRepository extends AreaRepository {

    @Inject
    public MonsterBossAreaRepository(ArangoAsyncService arango) {
        super(arango);
    }

    public CompletableFuture<Void> defeatBoss(String areaKey) {
        return getDatabase().query(
                "UPDATE @areaKey WITH {defeated: true} IN @@areas ",
                new MapBuilder()
                        .put("areaKey", areaKey)
                        .put("@areas", getCollectionType().getName())
                        .get(),
                String.class
        ).thenApply(result -> null);
    }

    public CompletableFuture<Void> openTpAreas(String areaKey) {
        return getDatabase().query(
                "FOR a IN 1..1 OUTBOUND @areaId @@bossTps " +
                        "UPDATE a._key WITH {opened: true} IN @@areas",
                new MapBuilder()
                        .put("areaId", getCollectionType().getId(areaKey))
                        .put("@areas", getCollectionType().getName())
                        .put("@bossTps", CollectionType.BOSS_TELEPORTATIONS.getName())
                        .get(),
                String.class
        ).thenApply(result -> null);
    }
}
