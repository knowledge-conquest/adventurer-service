package com.jackeri.kc.repositories.items;

import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.itemdata.EquipmentSlot;
import com.jackeri.kc.models.itemdata.ItemType;
import com.jackeri.kc.models.items.Equipment;
import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.models.items.WornEquipment;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;

public class EquipmentRepository extends Repository {

    @Inject
    public EquipmentRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.ITEMS;
    }

    public CompletableFuture<Boolean> unEquip(String entityId, EquipmentSlot slot) {
        return getDatabase().query(
                "FOR i, e IN 1..1 OUTBOUND @entityId @@edges1 " +
                        "FILTER e.equipmentSlot == @slot LIMIT 1 " +
                        "REMOVE e IN @@edges1 " +
                        "INSERT {_from: @entityId, _to: i._id} INTO @@edges2 ",
                new MapBuilder()
                        .put("entityId", entityId)
                        .put("slot", slot.name())
                        .put("@edges1", CollectionType.WORN_EQUIPMENTS.getName())
                        .put("@edges2", CollectionType.ITEM_OWNERSHIPS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Boolean> unEquipAll(String entityKey) {
        return getDatabase().query(
                "FOR i, e IN 1..1 OUTBOUND @entityId @@edges1 " +
                        "REMOVE e IN @@edges1 " +
                        "INSERT {_from: @entityId, _to: i._id} INTO @@edges2 " +
                        "RETURN true",
                new MapBuilder()
                        .put("entityId", CollectionType.ENTITIES.getId(entityKey))
                        .put("@edges1", CollectionType.WORN_EQUIPMENTS.getName())
                        .put("@edges2", CollectionType.ITEM_OWNERSHIPS.getName())
                        .get(),
                Boolean.class
        ).thenApply(Iterator::hasNext);
    }

    public CompletableFuture<Boolean> equip(String entityId, String itemId, EquipmentSlot slot) {
        return getDatabase().query(
                "FOR i, e IN 1..1 OUTBOUND @entityId @@edges1 " +
                        "FILTER i._id == @itemId LIMIT 1 " +
                        "FILTER DOCUMENT(i.itemData).equipmentSlot == @slot " +
                        "REMOVE e IN @@edges1 " +
                        "INSERT @worn INTO @@edges2 ",
                new MapBuilder()
                        .put("entityId", entityId)
                        .put("itemId", itemId)
                        .put("slot", slot.name())
                        .put("worn", new WornEquipment(entityId, itemId, slot))
                        .put("@edges1", CollectionType.ITEM_OWNERSHIPS.getName())
                        .put("@edges2", CollectionType.WORN_EQUIPMENTS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Void> addEquipment(Equipment equipment, String adventurerKey) {
        return getDatabase().query(
                "INSERT merge(@equipment, {itemData: @itemDataId}) INTO @@items " +
                        "LET id = NEW._id " +
                        "INSERT {_from: @adventurerId, _to: id}" +
                        "INTO @ownerships OPTIONS { ignoreErrors: true } ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("equipment", equipment)
                        .put("itemDataId", equipment.getItemData().getId())
                        .put("@items", CollectionType.ITEMS.getName())
                        .put("@ownerships", CollectionType.ITEM_OWNERSHIPS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Void> addEquipments(Vector<Item> equipments, String adventurerKey) {
        return getDatabase().query(
                "FOR equipment in @equipments " +
                        "FILTER equipment.itemData.itemType == @itemType " +
                        "INSERT merge(equipment, {itemData: equipment.itemData._id}) INTO @@items " +
                        "LET id = NEW._id " +
                        "INSERT {_from: @adventurerId, _to: id} " +
                        "INTO @@ownerships OPTIONS { ignoreErrors: true } ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("equipments", equipments)
                        .put("itemType", ItemType.EQUIPMENT)
                        .put("@items", CollectionType.ITEMS.getName())
                        .put("@ownerships", CollectionType.ITEM_OWNERSHIPS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }
}
