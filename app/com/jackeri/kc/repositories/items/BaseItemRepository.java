package com.jackeri.kc.repositories.items;

import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class BaseItemRepository extends Repository {

    @Inject
    public BaseItemRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.ITEMS;
    }

    public CompletableFuture<Stream<Item>> createItemsForAdventurer(Vector<Item> items, String adventurerKey) {
        return getDatabase().query(
                "FOR item in @items " +
                        "LET itemData = DOCUMENT(item.itemData) " +
                        "INSERT item IN @@items " +
                        "LET newItem = NEW " +
                        "INSERT {_from: @advId, _to: newItem._id} IN @@ownerships " +
                        "RETURN merge(newItem, {itemData})",
                new MapBuilder()
                        .put("items", items)
                        .put("@items", CollectionType.ITEMS.getName())
                        .put("@ownerships", CollectionType.ITEM_OWNERSHIPS.getName())
                        .put("advId", CollectionType.ENTITIES.getId(adventurerKey))
                        .get(),
                Item.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }
}
