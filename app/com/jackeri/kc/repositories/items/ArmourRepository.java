package com.jackeri.kc.repositories.items;

import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.items.Armour;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class ArmourRepository extends ItemRepository<Armour> {

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.ARMOUR;
    }

    @Override
    public Class<Armour> getDocumentClass() {
        return Armour.class;
    }

    @Inject
    public ArmourRepository(ArangoAsyncService arango) {
        super(arango, new String[] {
                "itemData.level DESC, itemData.equipmentSlots",
                "itemData.equipmentSlots, itemData.level DESC"
        });
    }

    public CompletableFuture<Stream<Armour>> fetchWorn(String entityId) {
        return getDatabase().query(
                "FOR item IN 1..1 OUTBOUND @entityId @@edges " +
                        "FILTER item.type == @type " +
                        "RETURN merge(item, {itemData: DOCUMENT(item.itemData)})",
                new MapBuilder()
                        .put("entityId", entityId)
                        .put("@edges", CollectionType.WORN_EQUIPMENTS.getName())
                        .put("type", getDocumentType().name())
                        .get(),
                getDocumentClass()
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }
}
