package com.jackeri.kc.repositories.items;

import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.models.items.Resource;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;

public class ResourceRepository extends ItemRepository<Resource> {

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.RESOURCE;
    }

    @Override
    public Class<Resource> getDocumentClass() {
        return Resource.class;
    }

    @Inject
    public ResourceRepository(ArangoAsyncService arango) {
        super(arango, new String[] {
                "itemData.level DESC, itemData.resourceType",
                "itemData.resourceType, itemData.level DESC"
        });
    }

    public CompletableFuture<Void> addResource(Resource resource, String adventurerKey) {
        return getDatabase().query(
                "UPSERT { itemData: @itemDataId } " +
                        "INSERT merge(@resource, {itemData: @itemDataId}) " +
                        "UPDATE { quantity: OLD.quantity + @quantity } IN @@items " +
                        "LET id = NEW._id " +
                        "INSERT {_from: @adventurerId, _to: id} " +
                        "INTO @ownerships OPTIONS { ignoreErrors: true } ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("quantity", resource.getQuantity())
                        .put("resource", resource)
                        .put("itemDataId", resource.getItemData().getId())
                        .put("@items", CollectionType.ITEMS.getName())
                        .put("@ownerships", CollectionType.ITEM_OWNERSHIPS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Void> addResources(Vector<Item> resources, String adventurerKey) {
        return getDatabase().query(
                "FOR resource in @resources " +
                        "FILTER resource.type == @resourceType " +
                        "UPSERT { itemData: resource.itemData._id } " +
                        "INSERT merge(resource, {itemData: resource.itemData._id}) " +
                        "UPDATE { quantity: OLD.quantity + resource.quantity } IN @@items " +
                        "LET id = NEW._id " +
                        "INSERT {_from: @adventurerId, _to: id} " +
                        "INTO @@ownerships OPTIONS { ignoreErrors: true } ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("resources", resources)
                        .put("resourceType", DocumentType.RESOURCE)
                        .put("@items", CollectionType.ITEMS.getName())
                        .put("@ownerships", CollectionType.ITEM_OWNERSHIPS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }
}
