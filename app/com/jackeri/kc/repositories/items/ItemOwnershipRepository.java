package com.jackeri.kc.repositories.items;

import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;

public class ItemOwnershipRepository extends Repository {

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.ITEM_OWNERSHIPS;
    }

    @Inject
    public ItemOwnershipRepository(ArangoAsyncService arango) {
        super(arango);
    }
}
