package com.jackeri.kc.repositories.items;

import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;


public abstract class ItemRepository<T extends Item> extends Repository {

    private String[] sorts;

    public ItemRepository(ArangoAsyncService arango, String[] sorts) {
        super(arango);
        this.sorts = sorts;
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.ITEMS;
    }

    public abstract DocumentType getDocumentType();

    public abstract Class<T> getDocumentClass();

    public String getSortFields(int sort) {
        return sort >= 0 && sort < sorts.length ? sorts[sort] : sorts[0];
    }

    public CompletableFuture<Stream<T>> fetch(String entityId, int offset, int count, int sort) {
        return getDatabase().query(
                "FOR item IN 1..1 OUTBOUND @entityId @@edges " +
                        "FILTER item.type == @type " +
                        "LET itemData = DOCUMENT(item.itemData) " +
                        "SORT " + getSortFields(sort) + " " +
                        "LIMIT @offset, @count " +
                        "RETURN merge(item, {itemData})",
                new MapBuilder()
                        .put("entityId", entityId)
                        .put("@edges", CollectionType.ITEM_OWNERSHIPS.getName())
                        .put("type", getDocumentType().name())
                        .put("offset", offset)
                        .put("count", count)
                        .get(),
                getDocumentClass()
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }
}
