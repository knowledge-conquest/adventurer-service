package com.jackeri.kc.repositories.items;

import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.items.Weapon;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class WeaponRepository extends ItemRepository<Weapon> {

    @Override
    public DocumentType getDocumentType() {
        return DocumentType.WEAPON;
    }

    @Override
    public Class<Weapon> getDocumentClass() {
        return Weapon.class;
    }

    @Inject
    public WeaponRepository(ArangoAsyncService arango) {
        super(arango, new String[]{
                "itemData.level DESC, itemData.weaponType",
                "itemData.weaponType, itemData.level DESC"
        });
    }

    public CompletableFuture<Weapon> fetchWorn(String entityId) {
        return getDatabase().query(
                "FOR item IN 1..1 OUTBOUND @entityId @@edges " +
                        "FILTER item.type == @type LIMIT 1 " +
                        "RETURN merge(item, {itemData: DOCUMENT(item.itemData)})",
                new MapBuilder()
                        .put("entityId", entityId)
                        .put("@edges", CollectionType.WORN_EQUIPMENTS.getName())
                        .put("type", getDocumentType().name())
                        .get(),
                getDocumentClass()
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }
}
