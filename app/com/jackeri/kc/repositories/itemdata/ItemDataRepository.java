package com.jackeri.kc.repositories.itemdata;

import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.areas.treasure.LootTreasureArea;
import com.jackeri.kc.models.areas.treasure.TreasureArea;
import com.jackeri.kc.models.itemdata.ItemData;
import com.jackeri.kc.models.itemdata.edges.TreasureLoot;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;
import com.jackeri.kc.utils.Pair;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class ItemDataRepository extends Repository {

    @Inject
    public ItemDataRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.ITEM_DATA;
    }

    public CompletableFuture<Stream<String>> storeLootTreasureGenerations
            (Vector<Pair<ItemData, Pair<LootTreasureArea, TreasureLoot>>> treasures) {
        return getDatabase().query(
                "FOR d IN @data " +
                        "INSERT d.a INTO @@items " +
                        "LET itemId = NEW._id " +
                        "INSERT d.b.a INTO @@areas " +
                        "LET areaId = NEW._id " +
                        "INSERT merge(d.b.b, {_from: itemId, _to: areaId}) INTO @@edges " +
                        "RETURN areaId ",
                new MapBuilder()
                        .put("data", treasures)
                        .put("@items", CollectionType.ITEM_DATA.getName())
                        .put("@areas", CollectionType.AREAS.getName())
                        .put("@edges", CollectionType.ITEM_DATA_EDGES.getName())
                        .get(),
                String.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }
}
