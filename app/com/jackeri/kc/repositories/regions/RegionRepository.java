package com.jackeri.kc.repositories.regions;

import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;

public class RegionRepository extends Repository {

    @Inject
    public RegionRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.REGIONS;
    }
}
