package com.jackeri.kc.repositories;

import com.arangodb.async.ArangoCollectionAsync;
import com.arangodb.async.ArangoDatabaseAsync;
import com.arangodb.entity.DocumentCreateEntity;
import com.arangodb.model.DocumentCreateOptions;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.Document;
import lombok.Getter;

import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public abstract class CollectionRepository<T extends Document> {

    @Getter
    private ArangoDatabaseAsync database;

    @Getter
    private CollectionType collectionType;

    @Getter
    private Class<T> documentClass;

    public CollectionRepository(ArangoDatabaseAsync database, CollectionType collectionType, Class<T> documentClass) {
        this.database = database;
        this.collectionType = collectionType;
        this.documentClass = documentClass;
    }

    public CompletableFuture<T> create(T document) {
        return getCollection().insertDocument(document, new DocumentCreateOptions().returnNew(true))
                .thenApply(DocumentCreateEntity::getNew);
    }

    public CompletableFuture<Stream<DocumentCreateEntity<T>>> create(Vector<T> documents) {
        return getCollection().insertDocuments(documents, new DocumentCreateOptions().returnNew(true))
                .thenApply(result -> result.getDocuments().stream());
    }

    public ArangoCollectionAsync getCollection() {
        return getDatabase().collection(getCollectionType().getName());
    }
}
