package com.jackeri.kc.repositories;

import com.arangodb.async.ArangoDatabaseAsync;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.DocumentType;
import lombok.Getter;

public abstract class DocumentRepository<T extends Document> extends CollectionRepository<T> {

    @Getter
    private DocumentType documentType;

    public DocumentRepository(ArangoDatabaseAsync database, CollectionType collectionType,
                              Class<T> documentClass, DocumentType documentType) {
        super(database, collectionType, documentClass);
        this.documentType = documentType;
    }
}
