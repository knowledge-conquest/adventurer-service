package com.jackeri.kc.repositories.recipes;

import com.arangodb.ArangoIterable;
import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.itemdata.ItemType;
import com.jackeri.kc.models.itemdata.edges.RecipeComponent;
import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.models.recipes.Recipe;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;
import com.jackeri.kc.utils.Pair;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class RecipeRepository extends Repository {

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.RECIPES;
    }

    @Inject
    public RecipeRepository(ArangoAsyncService arango) {
        super(arango);
    }

    public CompletableFuture<Stream<Recipe>> fetchRecipes(String realmId, long level, int offset, int count) {
        return getDatabase().query(
                "FOR recipe IN @@collection " +
                        "FILTER recipe.realm == @realmId && recipe.level == @level " +
                        "SORT recipe.level DESC  LIMIT @offset, @count " +
                        "LET ingredients = (FOR i, e IN 1..1 INBOUND recipe._id @@edges " +
                        "   FILTER e.type == @edgeType && e.componentType == @ingredient " +
                        "   RETURN {a: i, b: e.quantity}) " +
                        "LET results = (FOR i, e IN 1..1 INBOUND recipe._id @@edges " +
                        "   FILTER e.type == @edgeType && e.componentType == @result " +
                        "   RETURN {a: i, b: e.quantity}) " +
                        "RETURN merge(recipe, {ingredients, results}) ",
                new MapBuilder()
                        .put("realmId", realmId)
                        .put("level", level)
                        .put("@collection", CollectionType.RECIPES.getName())
                        .put("@edges", CollectionType.ITEM_DATA_EDGES.getName())
                        .put("edgeType", DocumentType.RECIPE_COMPONENT)
                        .put("ingredient", RecipeComponent.ComponentType.INGREDIENT)
                        .put("result", RecipeComponent.ComponentType.RESULT)
                        .put("offset", offset)
                        .put("count", count)
                        .get(),
                Recipe.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }

    public CompletableFuture<Recipe> fetchRecipe(String recipeKey) {
        return getDatabase().query(
                "FOR recipe IN @@collection " +
                        "FILTER recipe._key == @recipeKey " +
                        "LET ingredients = (FOR i, e IN 1..1 INBOUND recipe._id @@edges " +
                        "   FILTER e.type == @edgeType && e.componentType == @ingredient " +
                        "   RETURN {a: i, b: e.quantity}) " +
                        "LET results = (FOR i, e IN 1..1 INBOUND recipe._id @@edges " +
                        "   FILTER e.type == @edgeType && e.componentType == @result " +
                        "   RETURN {a: i, b: e.quantity}) " +
                        "RETURN merge(recipe, {ingredients, results}) ",
                new MapBuilder()
                        .put("@collection", CollectionType.RECIPES.getName())
                        .put("@edges", CollectionType.ITEM_DATA_EDGES.getName())
                        .put("edgeType", DocumentType.RECIPE_COMPONENT)
                        .put("ingredient", RecipeComponent.ComponentType.INGREDIENT)
                        .put("result", RecipeComponent.ComponentType.RESULT)
                        .put("recipeKey", recipeKey)
                        .get(),
                Recipe.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<Long> craftable(String adventurerKey, String recipeKey, int count) {
        return getDatabase().query(
                "FOR v, e IN 1..1 INBOUND @recipe @@edges1 " +
                        "FILTER e.componentType == @ingredient && v.itemType == @itemType " +
                        "FOR item IN 1..1 OUTBOUND @advId @@edges2 " +
                        "FILTER item.itemData == v._id && item.quantity >= e.quantity * @count " +
                        "RETURN true ",
                new MapBuilder()
                        .put("recipe", CollectionType.RECIPES.getId(recipeKey))
                        .put("itemType", ItemType.RESOURCE)
                        .put("@edges1", CollectionType.ITEM_DATA_EDGES.getName())
                        .put("@edges2", CollectionType.ITEM_OWNERSHIPS.getName())
                        .put("ingredient", RecipeComponent.ComponentType.INGREDIENT)
                        .put("advId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("count", count)
                        .get(),
                Boolean.class
        ).thenApply(ArangoIterable::count);
    }

    public CompletableFuture<Void> craft(String adventurerKey, String recipeKey, int count) {
        return getDatabase().query(
                "FOR v, e IN 1..1 INBOUND @recipeId @@edges1 " +
                        "FILTER e.componentType == @ingredient && v.itemType == @itemType " +
                        "FOR item IN 1..1 OUTBOUND @advId @@edges2 " +
                        "FILTER item.itemData == v._id " +
                        "UPDATE item._key WITH {quantity: item.quantity - e.quantity * @count} IN @@collection",
                new MapBuilder()
                        .put("recipeId", CollectionType.RECIPES.getId(recipeKey))
                        .put("advId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("count", count)
                        .put("itemType", ItemType.RESOURCE)
                        .put("@edges1", CollectionType.ITEM_DATA_EDGES.getName())
                        .put("@edges2", CollectionType.ITEM_OWNERSHIPS.getName())
                        .put("ingredient", RecipeComponent.ComponentType.INGREDIENT)
                        .put("@collection", CollectionType.ITEMS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Stream<Item>> getAdventurerIngredients
            (String adventurerKey, String recipeKey) {
        return getDatabase().query(
                "FOR i, e IN 1..1 INBOUND @recipeId @@edges1 " +
                        "FILTER e.type == @edgeType && e.componentType == @ingredient " +
                        "FOR item IN 1..1 OUTBOUND @advId @@edges2 " +
                        "FILTER item.itemData == i._id " +
                        "RETURN merge(item, {itemData: DOCUMENT(item.itemData)}) ",
                new MapBuilder()
                        .put("advId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("recipeId", CollectionType.RECIPES.getId(recipeKey))
                        .put("@edges1", CollectionType.ITEM_DATA_EDGES.getName())
                        .put("@edges2", CollectionType.ITEM_OWNERSHIPS.getName())
                        .put("edgeType", DocumentType.RECIPE_COMPONENT)
                        .put("ingredient", RecipeComponent.ComponentType.INGREDIENT)
                        .get(),
                Item.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }

    public CompletableFuture<Void> createRecipes(Vector<Pair<Recipe, Vector<RecipeComponent>>> recipes) {
        return getDatabase().query(
                "FOR d IN @data " +
                        "INSERT d.a INTO @@collection " +
                        "LET id = NEW._id " +
                        "FOR b IN d.b " +
                        "INSERT merge(b, {_to: id}) INTO @@edges ",
                new MapBuilder()
                        .put("data", recipes)
                        .put("@collection", CollectionType.RECIPES.getName())
                        .put("@edges", CollectionType.ITEM_DATA_EDGES.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }
}
