package com.jackeri.kc.repositories.recipes;

import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;

public class RecipeComponentRepository extends Repository {

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.ITEM_DATA_EDGES;
    }

    @Inject
    public RecipeComponentRepository(ArangoAsyncService arango) {
        super(arango);
    }
}
