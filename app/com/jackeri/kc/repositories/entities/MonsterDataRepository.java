package com.jackeri.kc.repositories.entities;

import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.areas.TeleportationArea;
import com.jackeri.kc.models.areas.monster.MonsterBossArea;
import com.jackeri.kc.models.areas.monster.MonsterSpawnerArea;
import com.jackeri.kc.models.entities.MonsterData;
import com.jackeri.kc.models.itemdata.edges.MonsterLoot;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;
import com.jackeri.kc.utils.Pair;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class MonsterDataRepository extends Repository {

    @Inject
    public MonsterDataRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.MONSTER_DATA;
    }

    public CompletableFuture<Stream<String>> storeGenerations
            (Vector<Pair<Pair<MonsterData, MonsterSpawnerArea>, Vector<MonsterLoot>>> monsters) {
        return getDatabase().query(
                "FOR d IN @data " +
                        "INSERT d.a.a INTO @@monsters " +
                        "LET id = NEW._id " +
                        "INSERT merge(d.a.b, {monsterData: id}) INTO @@areas " +
                        "LET area = NEW._id " +
                        "let bs = (FOR b IN d.b " +
                        "INSERT merge(b, {_to: id}) INTO @@edges) " +
                        "RETURN area ",
                new MapBuilder()
                        .put("data", monsters)
                        .put("@monsters", CollectionType.MONSTER_DATA.getName())
                        .put("@areas", CollectionType.AREAS.getName())
                        .put("@edges", CollectionType.ITEM_DATA_EDGES.getName())
                        .get(),
                String.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }

    public CompletableFuture<Stream<String>> storeBossGeneration
            (Pair<MonsterData, Pair<MonsterBossArea, TeleportationArea>> boss) {
        return getDatabase().query(
                "LET data = @data " +
                        "INSERT data.a INTO @@monsters " +
                        "LET monsterData = NEW._id " +
                        "LET bossArea = merge(data.b.a, {monsterData}) " +
                        "LET tpArea = data.b.b " +
                        "LET ids = (FOR area IN [bossArea, tpArea] " +
                        "   INSERT area INTO @@areas " +
                        "   RETURN NEW._id) " +
                        "INSERT {_from: ids[0], _to: ids[1]} INTO @@edges " +
                        "FOR id IN ids RETURN id ",
                new MapBuilder()
                        .put("data", boss)
                        .put("@monsters", CollectionType.MONSTER_DATA.getName())
                        .put("@areas", CollectionType.AREAS.getName())
                        .put("@edges", CollectionType.BOSS_TELEPORTATIONS.getName())
                        .get(),
                String.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }
}
