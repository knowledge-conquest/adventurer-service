package com.jackeri.kc.repositories.entities;


import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.concurrent.CompletableFuture;

public class EntityRepository extends Repository {

    @Inject
    public EntityRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.ENTITIES;
    }

    public CompletableFuture<Boolean> updateHealth(String key, long health) {
        return getDatabase().query(
                "FOR e in @@collection " +
                        "FILTER e._key == @key " +
                        "LIMIT 1 " +
                        "UPDATE e._key WITH { health: @health } IN @@collection RETURN true",
                new MapBuilder()
                        .put("key", key)
                        .put("health", health)
                        .put("@collection", getCollectionType().getName())
                        .get(),
                Boolean.class
        ).thenApply(Iterator::hasNext);
    }

    public CompletableFuture<Boolean> participatesInAction(String key) {
        return getDatabase().query(
                "FOR action IN 1..1 OUTBOUND @entityId @@edges LIMIT 1 RETURN true",
                new MapBuilder()
                        .put("entityId", CollectionType.ENTITIES.getId(key))
                        .put("@edges", CollectionType.ACTION_PARTICIPATION.getName())
                        .get(),
                Boolean.class
        ).thenApply(ArangoCursorAsync::hasNext);
    }

//    public CompletableFuture<Boolean> heal(Vector<Entity> entities) {
//        Vector<Pair<String, Long>> data = new Vector<>();
//        for (Entity entity : entities) {
//            data.add(new Pair<>(entity.getKey(), entity.getHealth().getMax()));
//        }
//
//        return getDatabase().query(
//                "FOR d in @data " +
//                        "UPDATE d.a WITH { currentHealth: d.b } IN @@entities " +
//                        "RETURN true",
//                new MapBuilder()
//                        .put("data", data)
//                        .put("@entities", CollectionType.ENTITIES.getName())
//                        .get(),
//                Boolean.class
//        ).thenApply(Iterator::hasNext);
//    }
}
