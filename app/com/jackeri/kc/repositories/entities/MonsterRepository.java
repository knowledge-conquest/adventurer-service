package com.jackeri.kc.repositories.entities;

import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;

public class MonsterRepository extends EntityRepository {

    private static final DocumentType DOCUMENT_TYPE = DocumentType.MONSTER;

    @Inject
    public MonsterRepository(ArangoAsyncService arango) {
        super(arango);
    }
}
