package com.jackeri.kc.repositories.entities;

import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.util.MapBuilder;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.areas.Area;
import com.jackeri.kc.models.entities.Adventurer;
import com.jackeri.kc.models.entities.WeaponProficiency;
import com.jackeri.kc.models.itemdata.EquipmentSlot;
import com.jackeri.kc.models.itemdata.WeaponType;
import com.jackeri.kc.models.participations.ActionState;
import com.jackeri.kc.models.regions.Region;
import com.jackeri.kc.models.utils.calculators.AttributeCalculator;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class AdventurerRepository extends EntityRepository {

    private static final DocumentType DOCUMENT_TYPE = DocumentType.ADVENTURER;

    @Inject
    public AdventurerRepository(ArangoAsyncService arango) {
        super(arango);
    }

    public CompletableFuture<Adventurer> createAdventurer
            (Adventurer adventurer, Vector<WeaponProficiency> proficiencies) {
        return getDatabase().query(
                "INSERT @adventurer IN @@entities " +
                        "LET adventurer = NEW " +
                        "LET wps = (FOR wp IN @wps INSERT merge(wp, {adventurer: adventurer._id}) INTO @@wps) " +
                        "RETURN adventurer",
                new MapBuilder()
                        .put("adventurer", adventurer)
                        .put("@entities", CollectionType.ENTITIES.getName())
                        .put("wps", proficiencies)
                        .put("@wps", CollectionType.WEAPON_PROFICIENCIES.getName())
                        .get(),
                Adventurer.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<Adventurer> fetchAdventurer(String adventurerKey) {
        return getDatabase().query(
                "LET adventurer = DOCUMENT(@adventurerId) FILTER adventurer " +
                        "LET attrBonuses = (FOR item, edge IN 1..1 OUTBOUND adventurer._id @@worn " +
                        "   RETURN {health: item.health, speed: item.speed, " +
                        "          attack: 0, defense: 0 + item.defense}) " +
//                        "          attack: 0 + item.attack, defense: 0 + item.defense}) " +
                        "LET weapon = (FOR item, edge IN 1..1 OUTBOUND adventurer._id @@worn " +
                        "   FILTER edge.equipmentSlot == @weaponSlot LIMIT 1 " +
                        "   RETURN merge(item, {itemData: DOCUMENT(item.itemData)}))[0] " +
                        "LET weaponProficiency = (FILTER weapon FOR wp IN @@wps" +
                        "   FILTER wp.adventurer == adventurer._id && wp.weaponType == weapon.itemData.weaponType" +
                        "   RETURN wp.value)[0] " +
                        "LET skills = (FOR s, e IN 1..1 OUTBOUND adventurer._id @@skillOwnerships " +
                        "   FILTER e.equipped && e.slotIndex != null " +
                        "   SORT e.slotIndex RETURN merge(s, {slotIndex: e.slotIndex})) " +
                        "RETURN merge(adventurer, { skills, weapon, weaponProficiency, " +
                        "healthBonus: sum(attrBonuses[*].health), speedBonus: sum(attrBonuses[*].speed), " +
                        "attackBonus: sum(attrBonuses[*].attack), defenseBonus: sum(attrBonuses[*].defense)}) ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("weaponSlot", EquipmentSlot.WEAPON)
                        .put("@worn", CollectionType.WORN_EQUIPMENTS.getName())
                        .put("@wps", CollectionType.WEAPON_PROFICIENCIES.getName())
                        .put("@skillOwnerships", CollectionType.SKILL_OWNERSHIPS.getName())
                        .get(),
                Adventurer.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<Stream<Adventurer>> adventurersOf(long userId) {
        return getDatabase().query(
                "FOR a IN @@entities FILTER a.type == @type && a.userId == @userId " +
                        "LET attrBonuses = (FOR item, edge IN 1..1 OUTBOUND a._id @@worn " +
                        "RETURN {health: item.health, speed: item.speed, attack: 0 + item.attack, defense: 0 + item.defense}) " +
                        "RETURN merge(a, { " +
                        "healthBonus: sum(attrBonuses[*].health), speedBonus: sum(attrBonuses[*].speed), " +
                        "attackBonus: sum(attrBonuses[*].attack), defenseBonus: sum(attrBonuses[*].defense)}) ",
                new MapBuilder()
                        .put("type", DOCUMENT_TYPE.name())
                        .put("userId", userId)
                        .put("@entities", CollectionType.ENTITIES.getName())
                        .put("@worn", CollectionType.WORN_EQUIPMENTS.getName())
                        .get(),
                Adventurer.class
        ).thenApplyAsync(ArangoCursorAsync::streamRemaining);
    }

    public CompletableFuture<Boolean> ownedBy(String adventurer, long userId) {
        return getDatabase().query(
                "FOR e IN Entities FILTER e.userId == @userId && e._id == @id LIMIT 1 RETURN true",
                new MapBuilder().put("userId", userId).put("id", CollectionType.ENTITIES.getId(adventurer)).get(),
                Boolean.class
        ).thenApplyAsync(Iterator::hasNext);
    }

    public CompletableFuture<Area> getLocation(String adventurerKey) {
        return getDatabase().query(
                "LET location = DOCUMENT(DOCUMENT(@adv).location) " +
                        "RETURN merge(location, {region: DOCUMENT(location.region)}) ",
                new MapBuilder()
                        .put("adv", getCollectionType().getId(adventurerKey))
                        .get(),
                Area.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<Long> getLevel(String adventurerKey) {
        return getDatabase().query(
                "FOR e IN @@collection FILTER e._key == @adv RETURN e.level",
                new MapBuilder()
                        .put("adv", adventurerKey)
                        .put("@collection", getCollectionType().getName())
                        .get(),
                Long.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<Boolean> teleportToRegion(String adventurerId, String regionId) {
        return getDatabase().query(
                "FOR area IN Areas FILTER area.region == @regionId && area.type == @type " +
                        "UPDATE PARSE_IDENTIFIER(@advId).key WITH {location: area._id} IN Entities",
                new MapBuilder()
                        .put("advId", adventurerId)
                        .put("regionId", regionId)
                        .put("type", DocumentType.SPAWN_AREA.name())
                        .get(),
                Boolean.class
        ).thenApply(ArangoCursorAsync::hasNext);
    }

    public CompletableFuture<Void> afterFightUpdates(Vector<Adventurer> adventurers) {
        Vector<AfterFightUpdate> updates = new Vector<>();
        for (Adventurer adventurer : adventurers) {
            updates.add(new AfterFightUpdate(adventurer));
        }

        return getDatabase().query(
                "FOR d IN @data " +
                        "UPDATE d._key WITH d IN @@entities ",
                new MapBuilder()
                        .put("data", updates)
                        .put("@entities", CollectionType.ENTITIES.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<String> getRealmId(String adventurerKey) {
        return getDatabase().query(
                "RETURN DOCUMENT(DOCUMENT(DOCUMENT(@id).location).region).realm",
                new MapBuilder()
                        .put("id", CollectionType.ENTITIES.getId(adventurerKey))
                        .get(),
                String.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<Boolean> setLocation(String adventurerKey, String destinationId) {
        return getDatabase().query(
                "UPDATE @key WITH { location: @location } IN Entities RETURN true",
                new MapBuilder()
                        .put("key", adventurerKey)
                        .put("location", destinationId)
                        .get(),
                Boolean.class
        ).thenApply(Iterator::hasNext);
    }

    public CompletableFuture<Region> getRegion(String adventurerKey) {
        return getDatabase().query(
                "FOR e IN @@entities FILTER e._key == @key LIMIT 1 " +
                        "FOR a IN @@areas FILTER a._id == e.location LIMIT 1 " +
                        "FOR r IN @@regions FILTER r._id == a.region LIMIT 1 " +
                        "RETURN r ",
                new MapBuilder()
                        .put("key", adventurerKey)
                        .put("@entities", CollectionType.ENTITIES.getName())
                        .put("@areas", CollectionType.AREAS.getName())
                        .put("@regions", CollectionType.REGIONS.getName())
                        .get(),
                Region.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<ActionState> getActionState(String key) {
        return getDatabase().query(
                "FOR a, p IN 1..1 OUTBOUND @advId @@edges RETURN p.actionState",
                new MapBuilder()
                        .put("advId", CollectionType.ENTITIES.getId(key))
                        .put("@edges", CollectionType.ACTION_PARTICIPATION.getName())
                        .get(),
                ActionState.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<Void> cultivationUpdate(String adventurerKey, long level, long experience) {
        return getDatabase().query(
                "UPDATE @key WITH {experience: @experience, level: @level} IN @@entities",
                new MapBuilder()
                        .put("key", adventurerKey)
                        .put("experience", experience)
                        .put("level", level)
                        .put("@entities", getCollectionType().getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Void> spawnRecoveryUpdate(String adventurerKey, long health) {
        return getDatabase().query(
                "UPDATE @key WITH {health: @health} IN @@entities",
                new MapBuilder()
                        .put("key", adventurerKey)
                        .put("health", health)
                        .put("@entities", getCollectionType().getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Void> trainingUpdate(String adventurerKey, double value, WeaponType weaponType) {
        WeaponProficiency proficiency = new WeaponProficiency(
                getCollectionType().getId(adventurerKey), weaponType, value);
        return getDatabase().query(
                "UPSERT {adventurer: @proficiency.adventurer, weaponType: @proficiency.weaponType} " +
                        "INSERT @proficiency UPDATE {value: OLD.value + @value} IN @@proficiencies",
                new MapBuilder()
                        .put("proficiency", proficiency)
                        .put("value", value)
                        .put("@proficiencies", CollectionType.WEAPON_PROFICIENCIES.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Boolean> hasRealm(String key) {
        return getDatabase().query(
                "RETURN DOCUMENT(@id).location != null",
                new MapBuilder()
                        .put("id", CollectionType.ENTITIES.getId(key))
                        .get(),
                Boolean.class
        ).thenApply(result -> result.hasNext() ? result.next() : false);
    }

    public CompletableFuture<Boolean> isDead(String key) {
        return getDatabase().query(
                "FOR e IN @@entities FILTER e._key == @key LIMIT 1 RETURN e.health == 0",
                new MapBuilder().put("key", key).put("@entities", CollectionType.ENTITIES.getName()).get(),
                Boolean.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletionStage<Void> removeItems(String adventurerKey) {
        return getDatabase().query(
                "FOR i, e IN 1..1 OUTBOUND @advId @@ownerships " +
                        "REMOVE i IN @@items REMOVE e IN @@ownerships ",
                new MapBuilder()
                        .put("advId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("@items", CollectionType.ITEMS.getName())
                        .put("@ownerships", CollectionType.ITEM_OWNERSHIPS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletionStage<Stream<WeaponProficiency>> getWeaponProficiencies(String adventurerKey) {
        return getDatabase().query(
                "FOR wp IN @@wps FILTER wp.adventurer == @advId " +
                        "SORT wp.value DESC, wp.weaponType RETURN wp",
                new MapBuilder()
                        .put("advId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("@wps", CollectionType.WEAPON_PROFICIENCIES.getName())
                        .get(),
                WeaponProficiency.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }

    private static class AfterFightUpdate {

        @JsonProperty("_key")
        private String key;

        private long level;

        private long health;

        private long experience;

        public AfterFightUpdate(Adventurer adventurer) {
            this.key = adventurer.getKey();
            this.level = adventurer.getLevel();
            this.health = adventurer.getHealth();
            this.experience = adventurer.getExperience();
        }
    }
}
