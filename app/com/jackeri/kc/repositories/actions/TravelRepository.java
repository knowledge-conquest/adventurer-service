package com.jackeri.kc.repositories.actions;

import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.util.MapBuilder;
import com.jackeri.kc.exceptions.TeleportationAreaClosedException;
import com.jackeri.kc.generation.RealmGenerator;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.areas.Area;
import com.jackeri.kc.models.areas.TeleportationArea;
import com.jackeri.kc.models.regions.Region;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;
import com.jackeri.kc.services.FutureService;
import com.jackeri.kc.services.entities.AdventurerService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class TravelRepository extends Repository {

    private static final Class<Area> DOCUMENT_CLASS = Area.class;

    @Inject
    private FutureService futureService;

    @Inject
    private RealmGenerator realmGenerator;

    @Inject
    private AdventurerService adventurerService;

    @Inject
    public TravelRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.AREAS;
    }

    public CompletableFuture<TravelMap> getAdventurerRegionMap(String adventurerKey) {
        return getDatabase().query(
                        "LET region = DOCUMENT(DOCUMENT(DOCUMENT(@adventurerId).location).region) " +
                        "LET areas = (FOR area IN Areas FILTER area.region == region._id RETURN area) " +
                        "LET edges = (FOR edge IN AreaPaths FILTER DOCUMENT(edge._from).region == region._id RETURN edge) " +
                        "RETURN {region, areas, edges}",
                new MapBuilder().put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey)).get(),
                TravelMap.class
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<Area> travelTo(String adventurerKey, String areaKey) {
        return getDatabase().query(
                "LET adv = DOCUMENT(@advId) "
                        + "FOR area IN 1..1 ANY adv.location AreaPaths FILTER area._id == @areaId LIMIT 1 "
                        + "UPDATE { _key: adv._key, location: area._id } IN Entities RETURN area",
                new MapBuilder()
                        .put("advId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("areaId", CollectionType.AREAS.getId(areaKey))
                        .get(),
                DOCUMENT_CLASS
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<Boolean> teleport(String adventurerKey) {
        return getDatabase().query(
                "LET location = DOCUMENT(@adventurerId).location " +
                        "FOR area IN @@collection " +
                        "FILTER area.type == @type && area._id == location LIMIT 1 " +
                        "RETURN merge(area, {region: DOCUMENT(area.region)}) ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("@collection", getCollectionType().getName())
                        .put("type", DocumentType.TELEPORTATION_AREA)
                        .get(),
                TeleportationArea.class
        ).thenCompose(result -> {
            if (!result.hasNext())
                return CompletableFuture.completedFuture(false);

            TeleportationArea area = result.next();
            if (!area.isOpened())
                throw TeleportationAreaClosedException.completionException();

            if (area.getDestination() == null) {
                return adventurerService.getAdventurerRealmId(adventurerKey)
                        .thenCompose(realmId -> adventurerService.getAdventurer(adventurerKey)
                                .thenCompose(adventurer -> realmGenerator
                                        .createRegion(realmId, area.getId(), Math.max(
                                                        adventurer.getLevel(),
                                                        area.getRegion().getDocument().getLevel() + 1
                                        ), 5, 5))
                                .thenCompose(ignored -> teleport(adventurerKey))
                        );
            }

            return adventurerService.setAdventurerLocation(adventurerKey, area.getDestination().getId());
        });
    }

    public CompletionStage<Stream<Region>> getRealmMap(String adventurerKey, int radius) {
        return getDatabase().query(
                "LET a = DOCUMENT(@id) FILTER a && a.location " +
                        "LET current = DOCUMENT(DOCUMENT(a.location).region) FILTER current " +
                        "LET before = (FOR r IN @@regions " +
                        "    FILTER r.realm == current.realm && r.level < current.level " +
                        "    SORT r.level DESC LIMIT @radius RETURN r) " +
                        "LET after = (FOR r IN @@regions " +
                        "    FILTER r.realm == current.realm && r.level > current.level " +
                        "    SORT r.level LIMIT @radius RETURN r) " +
                        "LET regions = APPEND(REVERSE(before), APPEND([current], after)) " +
                        "FOR region IN regions RETURN region ",
                new MapBuilder()
                        .put("id", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("radius", radius)
                        .put("@regions", CollectionType.REGIONS.getName())
                        .get(),
                Region.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }
}
