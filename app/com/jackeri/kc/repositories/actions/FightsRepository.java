package com.jackeri.kc.repositories.actions;

import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.entity.DocumentEntity;
import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.entities.Adventurer;
import com.jackeri.kc.models.fights.Fight;
import com.jackeri.kc.models.fights.FightEnded;
import com.jackeri.kc.models.itemdata.EquipmentSlot;
import com.jackeri.kc.models.itemdata.edges.MonsterLoot;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class FightsRepository extends Repository {

    private static final Class<Fight> DOCUMENT_CLASS = Fight.class;

    @Inject
    public FightsRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.FIGHTS;
    }

    public CompletableFuture<Boolean> isFighting(String id) {
        return getDatabase().query(
                "FOR f, e IN 1..1 OUTBOUND @id @@edges FILTER e.type == @edgeType LIMIT 1 RETURN true",
                new MapBuilder()
                        .put("id", id)
                        .put("@edges", CollectionType.ACTION_PARTICIPATION.getName())
                        .put("edgeType", DocumentType.FIGHT_PARTICIPATION)
                        .get(),
                Boolean.class
        ).thenApply(Iterator::hasNext);
    }

    public CompletableFuture<Fight> getFight(String fightKey) {
        return getDatabase().query(
                "LET f = DOCUMENT(@id)" +

                        "LET attackers = (FOR v, e IN 1..1 INBOUND f._id @@edges FILTER e.type == @edgeType && e.attacker " +
                        "   LET adventurer = (FILTER v.type == 'ADVENTURER' RETURN v)[0] " +
                        "   LET attrBonuses = (FILTER adventurer FOR item, edge IN 1..1 OUTBOUND adventurer._id @@worn " +
                        "      RETURN {health: item.health, speed: item.speed, " +
                        "          attack: 0, defense: 0 + item.defense}) " +
//                        "          attack: 0 + item.attack, defense: 0 + item.defense}) " +
                        "   LET weapon = (FILTER adventurer FOR item, edge IN 1..1 OUTBOUND adventurer._id @@worn " +
                        "      FILTER edge.equipmentSlot == @weaponSlot LIMIT 1 " +
                        "      RETURN merge(item, {itemData: DOCUMENT(item.itemData)}))[0] " +
                        "   LET weaponProficiency = (FILTER adventurer FILTER weapon FOR wp IN @@wps" +
                        "      FILTER wp.adventurer == adventurer._id && wp.weaponType == weapon.itemData.weaponType" +
                        "      RETURN wp.value)[0] " +
                        "   LET skills = (FILTER adventurer FOR skill, edge IN 1..1 OUTBOUND adventurer._id @@skillOwnerships " +
                        "      FILTER edge.equipped && edge.slotIndex != null " +
                        "      SORT edge.slotIndex RETURN merge(skill, {slotIndex: edge.slotIndex})) " +
                        "   RETURN merge(v, { skills, weapon, weaponProficiency, " +
                        "   healthBonus: sum(attrBonuses[*].health), speedBonus: sum(attrBonuses[*].speed), " +
                        "   attackBonus: sum(attrBonuses[*].attack), defenseBonus: sum(attrBonuses[*].defense)})) " +

                        "LET defenders = (FOR v, e IN 1..1 INBOUND f._id @@edges FILTER e.type == @edgeType && !e.attacker " +
                        "   LET adventurer = (FILTER v.type == 'ADVENTURER' RETURN v)[0] " +
                        "   LET attrBonuses = (FILTER adventurer FOR item, edge IN 1..1 OUTBOUND adventurer._id @@worn " +
                        "      RETURN {health: item.health, speed: item.speed, " +
                        "          attack: 0, defense: 0 + item.defense}) " +
//                        "          attack: 0 + item.attack, defense: 0 + item.defense}) " +
                        "   LET weapon = (FILTER adventurer FOR item, edge IN 1..1 OUTBOUND adventurer._id @@worn " +
                        "      FILTER edge.equipmentSlot == @weaponSlot LIMIT 1 " +
                        "      RETURN merge(item, {itemData: DOCUMENT(item.itemData)}))[0] " +
                        "   LET weaponProficiency = (FILTER adventurer FILTER weapon FOR wp IN @@wps" +
                        "      FILTER wp.adventurer == adventurer._id && wp.weaponType == weapon.itemData.weaponType" +
                        "      RETURN wp.value)[0] " +
                        "   LET skills = (FILTER adventurer FOR skill, edge IN 1..1 OUTBOUND adventurer._id @@skillOwnerships " +
                        "      FILTER edge.equipped && edge.slotIndex != null " +
                        "      SORT edge.slotIndex RETURN merge(skill, {slotIndex: edge.slotIndex})) " +
                        "   RETURN merge(v, { skills, weapon, weaponProficiency, " +
                        "   healthBonus: sum(attrBonuses[*].health), speedBonus: sum(attrBonuses[*].speed), " +
                        "   attackBonus: sum(attrBonuses[*].attack), defenseBonus: sum(attrBonuses[*].defense)})) " +

                        "RETURN MERGE(f, {attackers, defenders, area: DOCUMENT(f.area)}) ",
                new MapBuilder()
                        .put("id", CollectionType.FIGHTS.getId(fightKey))
                        .put("@edges", CollectionType.ACTION_PARTICIPATION.getName())
                        .put("@worn", CollectionType.WORN_EQUIPMENTS.getName())
                        .put("@wps", CollectionType.WEAPON_PROFICIENCIES.getName())
                        .put("edgeType", DocumentType.FIGHT_PARTICIPATION)
                        .put("weaponSlot", EquipmentSlot.WEAPON)
                        .put("@skillOwnerships", CollectionType.SKILL_OWNERSHIPS.getName())
                        .get(),
                DOCUMENT_CLASS
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<Fight> getAdventurerFight(String key) {
        return getDatabase().query(
                "FOR f, p IN 1..1 OUTBOUND @id @@edges FILTER p.type == @edgeType " +

                        "LET attackers = (FOR v, e IN 1..1 INBOUND f._id @@edges FILTER e.type == @edgeType && e.attacker " +
                        "   LET adventurer = (FILTER v.type == 'ADVENTURER' RETURN v)[0] " +
                        "   LET attrBonuses = (FILTER adventurer FOR item, edge IN 1..1 OUTBOUND adventurer._id @@worn " +
                        "      RETURN {health: item.health, speed: item.speed, " +
                        "          attack: 0, defense: 0 + item.defense}) " +
//                        "          attack: 0 + item.attack, defense: 0 + item.defense}) " +
                        "   LET weapon = (FILTER adventurer FOR item, edge IN 1..1 OUTBOUND adventurer._id @@worn " +
                        "      FILTER edge.equipmentSlot == @weaponSlot LIMIT 1 " +
                        "      RETURN merge(item, {itemData: DOCUMENT(item.itemData)}))[0] " +
                        "   LET weaponProficiency = (FILTER adventurer FILTER weapon FOR wp IN @@wps" +
                        "      FILTER wp.adventurer == adventurer._id && wp.weaponType == weapon.itemData.weaponType" +
                        "      RETURN wp.value)[0] " +
                        "   LET skills = (FILTER adventurer FOR skill, edge IN 1..1 OUTBOUND adventurer._id @@skillOwnerships " +
                        "      FILTER edge.equipped && edge.slotIndex != null " +
                        "      SORT edge.slotIndex RETURN merge(skill, {slotIndex: edge.slotIndex})) " +
                        "   RETURN merge(v, { skills, weapon, weaponProficiency, " +
                        "   healthBonus: sum(attrBonuses[*].health), speedBonus: sum(attrBonuses[*].speed), " +
                        "   attackBonus: sum(attrBonuses[*].attack), defenseBonus: sum(attrBonuses[*].defense)})) " +

                        "LET defenders = (FOR v, e IN 1..1 INBOUND f._id @@edges FILTER e.type == @edgeType && !e.attacker " +
                        "   LET adventurer = (FILTER v.type == 'ADVENTURER' RETURN v)[0] " +
                        "   LET attrBonuses = (FILTER adventurer FOR item, edge IN 1..1 OUTBOUND adventurer._id @@worn " +
                        "      RETURN {health: item.health, speed: item.speed, " +
                        "          attack: 0, defense: 0 + item.defense}) " +
//                        "          attack: 0 + item.attack, defense: 0 + item.defense}) " +
                        "   LET weapon = (FILTER adventurer FOR item, edge IN 1..1 OUTBOUND adventurer._id @@worn " +
                        "      FILTER edge.equipmentSlot == @weaponSlot LIMIT 1 " +
                        "      RETURN merge(item, {itemData: DOCUMENT(item.itemData)}))[0] " +
                        "   LET weaponProficiency = (FILTER adventurer FILTER weapon FOR wp IN @@wps" +
                        "      FILTER wp.adventurer == adventurer._id && wp.weaponType == weapon.itemData.weaponType" +
                        "      RETURN wp.value)[0] " +
                        "   LET skills = (FILTER adventurer FOR skill, edge IN 1..1 OUTBOUND adventurer._id @@skillOwnerships " +
                        "      FILTER edge.equipped && edge.slotIndex != null " +
                        "      SORT edge.slotIndex RETURN merge(skill, {slotIndex: edge.slotIndex})) " +
                        "   RETURN merge(v, { skills, weapon, weaponProficiency, " +
                        "   healthBonus: sum(attrBonuses[*].health), speedBonus: sum(attrBonuses[*].speed), " +
                        "   attackBonus: sum(attrBonuses[*].attack), defenseBonus: sum(attrBonuses[*].defense)})) " +

                        "RETURN MERGE(f, {attackers, defenders, area: DOCUMENT(f.area)}) ",
                new MapBuilder()
                        .put("id", CollectionType.ENTITIES.getId(key))
                        .put("@edges", CollectionType.ACTION_PARTICIPATION.getName())
                        .put("@worn", CollectionType.WORN_EQUIPMENTS.getName())
                        .put("@wps", CollectionType.WEAPON_PROFICIENCIES.getName())
                        .put("edgeType", DocumentType.FIGHT_PARTICIPATION)
                        .put("weaponSlot", EquipmentSlot.WEAPON)
                        .put("@skillOwnerships", CollectionType.SKILL_OWNERSHIPS.getName())
                        .get(),
                DOCUMENT_CLASS
        ).thenApply(result -> result.hasNext() ? result.next() : null);
    }

    public CompletableFuture<Fight> createFight(Vector<String> attackers, Vector<String> defenders,
                                                Fight.FightType fightType, String area) {
        Fight fight = new Fight(attackers, defenders, area, fightType);

        return getDatabase().collection(CollectionType.FIGHTS.getName())
                .insertDocument(fight).thenApply(DocumentEntity::getKey)
                .thenCompose(key -> {
                    fight.setKey(key);
                    return getDatabase().collection(CollectionType.ACTION_PARTICIPATION.getName())
                            .insertDocuments(fight.extractParticipation())
                            .thenCompose(ignored -> getFight(key));
                });
    }

    public CompletableFuture<Boolean> removeParticipants(String fightKey) {
        return getDatabase().query(
                "FOR adventurer, participation IN 1..1 INBOUND @id @@edges " +
                        "FILTER participation.type == @edgeType " +
                        "REMOVE participation IN @@edges",
                new MapBuilder()
                        .put("id", getCollectionType().getId(fightKey))
                        .put("@edges", CollectionType.ACTION_PARTICIPATION.getName())
                        .put("edgeType", DocumentType.FIGHT_PARTICIPATION)
                        .get(),
                Boolean.class
        ).thenApply(Iterator::hasNext);
    }

    public CompletableFuture<Boolean> updateLogs(Fight fight) {
        return getDatabase().query(
                "LET f = DOCUMENT(@id) UPDATE f WITH { logs: @logs } IN @@collection",
                new MapBuilder()
                        .put("id", fight.getId())
                        .put("logs", fight.getLogs())
                        .put("@collection", getCollectionType().getName())
                        .get(),
                Boolean.class
        ).thenApply(Iterator::hasNext);
    }

    public CompletableFuture<Stream<Adventurer>> getAdventurers(String fightKey) {
        return getDatabase().query(
                "FOR entity IN 1..1 INBOUND @id @@edges " +
                        "FILTER entity.type == @type " +
                        "RETURN entity",
                new MapBuilder()
                        .put("id", getCollectionType().getId(fightKey))
                        .put("type", DocumentType.ADVENTURER.name())
                        .put("@edges", CollectionType.ACTION_PARTICIPATION.getName())
                        .get(),
                Adventurer.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }

    public CompletableFuture<Boolean> removeMonsters(String fightKey) {
        return getDatabase().query(
                "FOR entity IN 1..1 INBOUND @id @@edges " +
                        "FILTER entity.type == @type " +
                        "REMOVE entity IN @@entities " +
                        "RETURN true",
                new MapBuilder()
                        .put("id", getCollectionType().getId(fightKey))
                        .put("type", DocumentType.MONSTER.name())
                        .put("@entities", CollectionType.ENTITIES.getName())
                        .put("@edges", CollectionType.ACTION_PARTICIPATION.getName())
                        .get(),
                Boolean.class
        ).thenApply(Iterator::hasNext);
    }

    public CompletableFuture<Void> endFight(Fight fight) {
        FightEnded fightEnded = fight.toFightEnded();
        return getCollection().replaceDocument(
                fight.getKey(), fightEnded)
                .thenApply(ignored -> null);
    }

    public CompletableFuture<Stream<MonsterLoot>> getLoots(String key, boolean isAttacker) {
        return getDatabase().query(
                "FOR monster, participation IN 1..1 INBOUND @fightId @@edges1 " +
                        "FILTER monster.type == @entityType && participation.attacker == @isAttacker " +
                        "FOR item, loot IN 1..1 INBOUND monster.monsterData @@edges2 " +
                        "FILTER loot.type == @lootType " +
                        "RETURN merge(loot, {_from: item}) ",
                new MapBuilder()
                        .put("fightId", CollectionType.FIGHTS.getId(key))
                        .put("isAttacker", isAttacker)
                        .put("entityType", DocumentType.MONSTER)
                        .put("lootType", DocumentType.MONSTER_LOOT)
                        .put("@edges1", CollectionType.ACTION_PARTICIPATION.getName())
                        .put("@edges2", CollectionType.ITEM_DATA_EDGES.getName())
                        .get(),
                MonsterLoot.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }
}
