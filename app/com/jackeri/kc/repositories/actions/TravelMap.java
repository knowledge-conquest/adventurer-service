package com.jackeri.kc.repositories.actions;

import com.jackeri.kc.models.Document;
import com.jackeri.kc.models.areas.Area;
import com.jackeri.kc.models.areas.AreaPath;
import com.jackeri.kc.models.regions.Region;
import lombok.Getter;
import play.libs.Json;

import java.util.Vector;

public class TravelMap {

    @Getter
    private Region region;

    @Getter
    private Vector<Area> areas;

    @Getter
    private Vector<AreaPath> edges;

    public TravelMap(Region region, Vector<Area> areas, Vector<AreaPath> edges) {
        this.region = region;
        this.areas = areas;
        this.edges = edges;
    }

    public String toJson() {
        return Json.toJson(new Response()).toString();
    }

    private final class Response {

        @Getter
        private Region.RegionResponse region;

        @Getter
        private Vector<Area.AreaResponse> areas;

        @Getter
        private Vector<AreaPath.AreaPathResponse> edges;

        public Response() {
            this.region = TravelMap.this.region.toResponse();
            this.areas = toResponses(TravelMap.this.areas);
            this.edges = toResponses(TravelMap.this.edges);
        }

        @SuppressWarnings("unchecked")
        private <T extends Document.DocumentResponse, U extends Document> Vector<T> toResponses(Vector<U> documents) {
            Vector<T> responses = new Vector<>();
            for (U document : documents) {
                responses.add((T) document.toResponse());
            }

            return responses;
        }
    }
}
