package com.jackeri.kc.repositories.skills;

import com.arangodb.async.ArangoCursorAsync;
import com.arangodb.util.MapBuilder;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.skills.Skill;
import com.jackeri.kc.repositories.Repository;
import com.jackeri.kc.services.ArangoAsyncService;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class SkillRepository extends Repository {

    @Inject
    public SkillRepository(ArangoAsyncService arango) {
        super(arango);
    }

    @Override
    public CollectionType getCollectionType() {
        return CollectionType.SKILLS;
    }

    public CompletableFuture<Stream<Skill>> getSkills(String adventurerKey, int count, int offset, int sort) {
        String sortText = "";
        switch (sort) {
            case 1:
                sortText = "s.skillEffect, s.level DESC, s.weaponType, s.skillTier ";
                break;
            case 2:
                sortText = "s.skillTier, s.level DESC, s.skillEffect, s.weaponType ";
                break;
            case 3:
                sortText = "s.weaponType, s.level DESC, s.skillEffect, s.skillTier ";
                break;
            default:
                sortText = "s.level DESC, s.skillEffect, s.weaponType, s.skillTier ";
        }
        return getDatabase().query(
                "FOR s, e IN 1..1 OUTBOUND @adventurerId @@ownerships " +
                        "FILTER !e.equipped " +
                        "SORT " + sortText +
                        "LIMIT @offset, @count " +
                        "RETURN s",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("offset", offset)
                        .put("count", count)
                        .put("@ownerships", CollectionType.SKILL_OWNERSHIPS.getName())
                        .get(),
                Skill.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }

    public CompletableFuture<Stream<Skill>> getEquippedSkills(String adventurerKey) {
        return getDatabase().query(
                "FOR s, e IN 1..1 OUTBOUND @adventurerId @@ownerships " +
                        "FILTER e.equipped && e.slotIndex != null " +
                        "SORT e.slotIndex RETURN merge(s, {slotIndex: e.slotIndex}) ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("@ownerships", CollectionType.SKILL_OWNERSHIPS.getName())
                        .get(),
                Skill.class
        ).thenApply(ArangoCursorAsync::streamRemaining);
    }

    public CompletableFuture<Void> equip(String adventurerKey, String skillKey, int slotIndex) {
        return getDatabase().query(
                "FOR s, e IN 1..1 OUTBOUND @adventurerId @@ownerships " +
                        "FILTER s._key == @skillKey LIMIT 1 " +
                        "FILTER !e.equipped " +
                        "UPDATE e._key WITH {equipped: true, slotIndex: @slotIndex} IN @@ownerships ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("skillKey", skillKey)
                        .put("slotIndex", slotIndex)
                        .put("@ownerships", CollectionType.SKILL_OWNERSHIPS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Void> unEquip(String adventurerKey, int slotIndex) {
        return getDatabase().query(
                "FOR s, e IN 1..1 OUTBOUND @adventurerId @@ownerships " +
                        "FILTER e.equipped && e.slotIndex == @slotIndex " +
                        "UPDATE e._key WITH {equipped: false, slotIndex: null} IN @@ownerships ",
                new MapBuilder()
                        .put("adventurerId", CollectionType.ENTITIES.getId(adventurerKey))
                        .put("slotIndex", slotIndex)
                        .put("@ownerships", CollectionType.SKILL_OWNERSHIPS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Void> reduceCooldowns(Vector<String> skills) {
        return getDatabase().query(
                "FOR key IN @skills " +
                        "LET skill = DOCUMENT(CONCAT(@collection, '/', key)) FILTER skill " +
                        "UPDATE key WITH {cooldown: MAX([0, skill.cooldown - 1])} IN @@skills ",
                new MapBuilder()
                        .put("skills", skills)
                        .put("collection", CollectionType.SKILLS.getName())
                        .put("@skills", CollectionType.SKILLS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Void> clearCooldowns(Vector<String> skills) {
        return getDatabase().query(
                "FOR skill IN @skills " +
                        "UPDATE skill WITH {cooldown: 0} IN @@skills ",
                new MapBuilder()
                        .put("skills", skills)
                        .put("@skills", CollectionType.SKILLS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Void> setCooldown(String skill, int cooldown) {
        return getDatabase().query(
                "UPDATE @skill WITH {cooldown: @cooldown} IN @@skills ",
                new MapBuilder()
                        .put("skill", skill)
                        .put("cooldown", cooldown)
                        .put("@skills", CollectionType.SKILLS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }

    public CompletableFuture<Void> practiceUpdate(String key, long level, long experience) {
        return getDatabase().query(
                "UPDATE @skill WITH {level: @level, experience: @experience} IN @@skills ",
                new MapBuilder()
                        .put("skill", key)
                        .put("level", level)
                        .put("experience", experience)
                        .put("@skills", CollectionType.SKILLS.getName())
                        .get(),
                String.class
        ).thenApply(ignored -> null);
    }
}
