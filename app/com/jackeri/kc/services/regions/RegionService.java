package com.jackeri.kc.services.regions;

import com.jackeri.kc.models.regions.Region;
import com.jackeri.kc.repositories.regions.RegionRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class RegionService {

    @Inject
    private RegionRepository regionRepository;

    public CompletableFuture<Region> createAndGetRegion(Region region) {
        return regionRepository.createAndGet(region);
    }
}
