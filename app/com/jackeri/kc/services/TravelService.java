package com.jackeri.kc.services;

import com.jackeri.kc.exceptions.NoPathException;
import com.jackeri.kc.models.areas.Area;
import com.jackeri.kc.models.regions.Region;
import com.jackeri.kc.repositories.actions.TravelMap;
import com.jackeri.kc.repositories.actions.TravelRepository;
import com.jackeri.kc.services.entities.AdventurerService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class TravelService {

    private final TravelRepository travelRepository;

    @Inject
    public TravelService(TravelRepository travelRepository) {
        this.travelRepository = travelRepository;
    }

    public CompletableFuture<Area> adventurerTravelTo(String adventurerKey, String destinationKey) {
        return travelRepository.travelTo(adventurerKey, destinationKey)
                .thenApply(area -> {
                    if (area == null) throw NoPathException.completionException();

                    return area;
                });
    }

    public CompletableFuture<Boolean> adventurerTeleport(String adventurerKey) {
        return travelRepository.teleport(adventurerKey);
    }

    public CompletableFuture<TravelMap> getAdventurerRegionMap(String adventurerKey) {
        return travelRepository.getAdventurerRegionMap(adventurerKey);
    }

    public CompletionStage<Stream<Region>> getAdventurerRealmMap(String adventurerKey, int radius) {
        return travelRepository.getRealmMap(adventurerKey, radius);
    }
}
