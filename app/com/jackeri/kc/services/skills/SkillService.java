package com.jackeri.kc.services.skills;

import com.jackeri.kc.models.skills.Skill;
import com.jackeri.kc.repositories.skills.SkillRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class SkillService {

    private static final int SKILL_SLOT_COUNT = 5;

    private SkillRepository skillRepository;

    @Inject
    public SkillService(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    public CompletableFuture<Stream<Skill>> getSkills(String adventurerKey, int count, int offset, int sort) {
        return skillRepository.getSkills(adventurerKey, count, offset, sort);
    }

    public CompletableFuture<Stream<Skill>> getEquippedSkills(String adventurerKey) {
        return skillRepository.getEquippedSkills(adventurerKey);
    }

    public CompletableFuture<Void> equip(String adventurerKey, String skillKey, int slotIndex) {
        return unEquip(adventurerKey, slotIndex)
                .thenCompose(ignoredVoid -> skillRepository.equip(adventurerKey, skillKey, slotIndex));
    }

    public CompletableFuture<Void> unEquip(String adventurerKey, int slotIndex) {
        return skillRepository.unEquip(adventurerKey, slotIndex);
    }

    public boolean checkSlotIndex(int slotIndex) {
        return slotIndex > 0 && slotIndex < SKILL_SLOT_COUNT;
    }
}
