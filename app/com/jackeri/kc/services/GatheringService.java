package com.jackeri.kc.services;

import com.jackeri.kc.exceptions.WrongAreaException;
import com.jackeri.kc.models.areas.GatheringArea;
import com.jackeri.kc.models.itemdata.ItemData;
import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.repositories.areas.GatheringAreaRepository;
import com.jackeri.kc.repositories.items.ResourceRepository;
import com.jackeri.kc.utils.Pair;
import com.jackeri.kc.utils.Random;
import com.jackeri.kc.utils.Range;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class GatheringService {

    private final GatheringAreaRepository gatheringAreaRepository;
    private final ResourceRepository resourceRepository;

    @Inject
    public GatheringService(GatheringAreaRepository gatheringAreaRepository,
                            ResourceRepository resourceRepository) {
        this.gatheringAreaRepository = gatheringAreaRepository;
        this.resourceRepository = resourceRepository;
    }

    public CompletableFuture<GatheringArea> getGatheringAreaForAdventurer(String adventurerKey) {
        return gatheringAreaRepository.getAreaForAdventurer(adventurerKey)
                .thenApply(area -> {
                    if (area == null) throw WrongAreaException.completionException();
                    return area;
                });
    }

    public CompletableFuture<Stream<Item>> gather(String adventurerKey) {
        return getGatheringAreaForAdventurer(adventurerKey)
            .thenCompose(area -> {
                Vector<Item> items = new Vector<>();
                for (Pair<ItemData, Range<Long>> loot : area.getLoots()) {
                    long quantity = Random.getLong(loot.getB());
                    if (quantity > 0) {
                        items.addAll(loot.getA().createItem(quantity, true));
                    }
                }
                return resourceRepository.addResources(items, adventurerKey)
                        .thenApply(ignoredVoid -> items.stream());
            });
    }
}
