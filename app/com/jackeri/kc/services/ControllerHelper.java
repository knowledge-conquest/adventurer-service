package com.jackeri.kc.services;

import com.jackeri.kc.exceptions.NotFoundException;
import com.jackeri.kc.exceptions.UnauthorizedException;
import com.jackeri.kc.models.Document;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.utils.Attrs;
import com.jackeri.kc.utils.exceptions.NoUserIdException;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.stream.Stream;

public class ControllerHelper {

    private AdventurerService adventurerService;

    @Inject
    public ControllerHelper(AdventurerService adventurerService) {
        this.adventurerService = adventurerService;
    }

    public Long extractUserIDFrom(Http.Request request) throws NoUserIdException {
        return request.attrs().get(Attrs.VERIFIED_JWT).getUserId();
    }

    public CompletableFuture<Void> userAuthorizationGuard(Http.Request request, String adventurerKey) {
        try {
            return adventurerService.isAdventurerOwnedBy(adventurerKey, extractUserIDFrom(request))
                    .thenApply(ok -> {
                        if (!ok) throw UnauthorizedException.completionException();
                        return null;
                    });
        } catch (NoUserIdException e) {
            throw new CompletionException(e);
        }
    }

    public Result toJson(Object object) {
        return Results.ok(Json.toJson(object));
    }

    public Result toResponse(Document document) {
        return Results.ok(Json.toJson(document.toResponse()));
    }

    public Result toResponseOrNotFound(Document document) {
        if (document == null) throw NotFoundException.completionException();
        return Results.ok(Json.toJson(document.toResponse()));
    }

    public <U extends Document> Result toResponses(Stream<U> documents) {
        return Results.ok(Json.toJson(documents.map(Document::toResponse)));
    }

    public <U extends Document> Result toResponses(Collection<U> documents) {
        return toResponses(documents.stream());
    }
}
