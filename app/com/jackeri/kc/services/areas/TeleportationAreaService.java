package com.jackeri.kc.services.areas;

import com.jackeri.kc.repositories.areas.TeleportationAreaRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class TeleportationAreaService {

    @Inject
    private TeleportationAreaRepository teleportationAreaRepository;

    public CompletableFuture<Boolean> setTeleportationDestination(String areaId, String destinationId) {
        return teleportationAreaRepository.setDestination(areaId, destinationId);
    }
}
