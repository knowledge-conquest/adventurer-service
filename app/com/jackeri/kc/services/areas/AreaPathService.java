package com.jackeri.kc.services.areas;

import com.jackeri.kc.models.areas.AreaPath;
import com.jackeri.kc.repositories.areas.AreaPathRepository;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class AreaPathService {

    @Inject
    private AreaPathRepository areaPathRepository;

    public CompletableFuture<Stream<String>> createAreaPaths(Vector<AreaPath> areaPaths) {
        return areaPathRepository.create(areaPaths);
    }
}