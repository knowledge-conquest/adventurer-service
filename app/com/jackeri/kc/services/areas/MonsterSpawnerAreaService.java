package com.jackeri.kc.services.areas;

import com.jackeri.kc.models.areas.monster.MonsterArea;
import com.jackeri.kc.models.areas.monster.MonsterSpawnerArea;
import com.jackeri.kc.repositories.areas.MonsterSpawnerAreaRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class MonsterSpawnerAreaService {

    @Inject
    private MonsterSpawnerAreaRepository monsterSpawnerAreaRepository;

    public CompletableFuture<MonsterArea> getMonsterAreaForAdventurer(String adventurerKey) {
        return monsterSpawnerAreaRepository.fetchMonsterAreaForAdventurer(adventurerKey);
    }
}
