package com.jackeri.kc.services.areas;

import com.jackeri.kc.generation.map.MapGraph;
import com.jackeri.kc.models.areas.Area;
import com.jackeri.kc.repositories.areas.AreaRepository;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class AreaService {

    @Inject
    private AreaRepository areaRepository;

    public CompletableFuture<Area> createAndGetArea(Area area) {
        return areaRepository.createAndGet(area);
    }

    public CompletableFuture<Stream<Area>> createAndGetAreas(Vector<Area> areas) {
        return areaRepository.createAndGet(areas);
    }

    public CompletableFuture<String> create(Area area) {
        return areaRepository.create(area);
    }

    public CompletableFuture<Stream<String>> create(Vector<Area> areas) {
        return areaRepository.create(areas);
    }

    public CompletableFuture<Area> getArea(String key) {
        return areaRepository.findByKey(key, Area.class);
    }

    public CompletableFuture<Void> setAreasPositions(Vector<MapGraph.Vertex> vertices) {
        return areaRepository.setPositions(vertices);
    }
}
