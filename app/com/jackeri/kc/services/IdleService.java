package com.jackeri.kc.services;

import com.jackeri.kc.exceptions.EmptySkillSlotException;
import com.jackeri.kc.exceptions.NotFoundException;
import com.jackeri.kc.exceptions.UnsupportedActionStateException;
import com.jackeri.kc.exceptions.WrongAreaException;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.itemdata.WeaponType;
import com.jackeri.kc.models.participations.ActionState;
import com.jackeri.kc.models.participations.idle.*;
import com.jackeri.kc.models.participations.idle.reward.CultivationReward;
import com.jackeri.kc.models.participations.idle.reward.SkillPracticeReward;
import com.jackeri.kc.models.participations.idle.reward.SpawnRecoveryReward;
import com.jackeri.kc.models.participations.idle.reward.TrainingReward;
import com.jackeri.kc.models.skills.Skill;
import com.jackeri.kc.repositories.IdleRepositiory;
import com.jackeri.kc.services.entities.AdventurerService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class IdleService {

    private final IdleRepositiory idleRepositiory;
    private final AdventurerService adventurerService;

    @Inject
    public IdleService(IdleRepositiory idleRepositiory, AdventurerService adventurerService) {
        this.idleRepositiory = idleRepositiory;
        this.adventurerService = adventurerService;
    }

    public CompletableFuture<IdleActionParticipation> get(String adventurerKey, ActionState actionState) {
        if (!actionState.isIdle()) {
            throw UnsupportedActionStateException.completionException();
        }

        return idleRepositiory.get(adventurerKey, actionState)
                .thenApply(action -> {
                    if (action == null) {
                        throw NotFoundException.completionException();
                    }

                    return action;
                });
    }

    public CompletableFuture<IdleActionParticipation> startCultivation(String adventurerKey) {
        return start(
                adventurerKey,
                new CultivationParticipation(CollectionType.ENTITIES.getId(adventurerKey), ""),
                DocumentType.CULTIVATION_AREA);
    }

    public CompletableFuture<IdleActionParticipation> startSpawnRecovery(String adventurerKey) {
        return start(
                adventurerKey,
                new SpawnRecoveryParticipation(CollectionType.ENTITIES.getId(adventurerKey), ""),
                DocumentType.SPAWN_AREA);
    }

    public CompletableFuture<IdleActionParticipation> startTraining(String adventurerKey, WeaponType weaponType) {
        return start(
                adventurerKey,
                new TrainingParticipation(CollectionType.ENTITIES.getId(adventurerKey), "", weaponType),
                DocumentType.TRAINING_AREA);
    }

    public CompletableFuture<IdleActionParticipation> startSkillPractice(String adventurerKey, int slotIndex) {
        return adventurerService.getAdventurer(adventurerKey)
                .thenCompose(adventurer -> {
                    Skill skill = adventurer.getSkill(slotIndex);
                    if (skill == null) throw EmptySkillSlotException.completionException();
                    return start(
                            adventurerKey,
                            new SkillPracticeParticipation(CollectionType.ENTITIES.getId(adventurerKey), "", slotIndex),
                            DocumentType.SKILL_PRACTICE_AREA);
                });
    }

    private CompletableFuture<IdleActionParticipation> start
            (String adventurerKey, IdleActionParticipation action, DocumentType areaType) {
        return adventurerService.idleGuard(adventurerKey)
                .thenCompose(ignoredVoid -> idleRepositiory.start(action, areaType))
                .thenApply(result -> {
                    if (result == null)
                        throw WrongAreaException.completionException();
                    return result;
                });
    }

    public CompletableFuture<IdleActionParticipation> stopCultivation(String adventurerKey) {
        return stop(adventurerKey, ActionState.CULTIVATION)
                .thenCompose(action -> {
                    CultivationParticipation cultivation = (CultivationParticipation) action;
                    CultivationReward reward = new CultivationReward(
                            cultivation.getArea().getDocument().getRegion().getDocument().getLevel(),
                            cultivation.getDuration());
                    cultivation.setReward(reward);
                    return adventurerService.cultivationUpdate(adventurerKey, reward.getExperience())
                            .thenApply(ignoredVoid -> cultivation);
                });
    }

    public CompletableFuture<IdleActionParticipation> stopSpawnRecovery(String adventurerKey) {
        return stop(adventurerKey, ActionState.SPAWN_RECOVERY)
                .thenCompose(action -> {
                    SpawnRecoveryParticipation recovery = (SpawnRecoveryParticipation) action;
                    SpawnRecoveryReward reward = new SpawnRecoveryReward(
                            recovery.getArea().getDocument().getRegion().getDocument().getLevel(),
                            recovery.getDuration());
                    recovery.setReward(reward);
                    return adventurerService.spawnRecoveryUpdate(adventurerKey, reward.getHealth())
                            .thenApply(ignoredVoid -> recovery);
                });
    }

    public CompletableFuture<IdleActionParticipation> stopTraining(String adventurerKey) {
        return stop(adventurerKey, ActionState.TRAINING)
                .thenCompose(action -> {
                    TrainingParticipation training = (TrainingParticipation) action;
                    TrainingReward reward = new TrainingReward(
                            training.getArea().getDocument().getRegion().getDocument().getLevel(),
                            training.getDuration());
                    training.setReward(reward);
                    return adventurerService
                            .trainingUpdate(adventurerKey, reward.getWeaponProficiency(), training.getWeaponType())
                            .thenApply(ignoredVoid -> training);
                });
    }

    public CompletableFuture<IdleActionParticipation> stopSkillPractice(String adventurerKey) {
        return stop(adventurerKey, ActionState.SKILL_PRACTICE)
                .thenCompose(action -> {
                    SkillPracticeParticipation skillPractice = (SkillPracticeParticipation) action;
                    SkillPracticeReward reward = new SkillPracticeReward(
                            skillPractice.getArea().getDocument().getRegion().getDocument().getLevel(),
                            skillPractice.getDuration());
                    skillPractice.setReward(reward);
                    return adventurerService
                            .skillPracticeUpdate(adventurerKey, reward.getExperience(), skillPractice.getSlotIndex())
                            .thenApply(ignoredVoid -> skillPractice);
                });
    }

    private CompletableFuture<IdleActionParticipation> stop(String adventurerKey, ActionState actionState) {
        return idleRepositiory.stop(adventurerKey, actionState)
                .thenApply(action -> {
                    if (action == null) {
                        throw NotFoundException.completionException();
                    }

                    return action;
                });
    }
}
