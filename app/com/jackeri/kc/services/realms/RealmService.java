package com.jackeri.kc.services.realms;

import com.jackeri.kc.models.realms.Realm;
import com.jackeri.kc.repositories.realms.RealmRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class RealmService {

    private final RealmRepository realmRepository;

    @Inject
    public RealmService(RealmRepository realmRepository) {
        this.realmRepository = realmRepository;
    }

    public CompletableFuture<String> createRealm(String adventurerId) {
        return realmRepository.create(new Realm(adventurerId));
    }
}
