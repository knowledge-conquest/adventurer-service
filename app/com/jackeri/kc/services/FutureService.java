package com.jackeri.kc.services;

import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;

@Singleton
public class FutureService {

    public <U> CompletableFuture<U> supply(U object) {
        return CompletableFuture.completedFuture(object);
    }
}
