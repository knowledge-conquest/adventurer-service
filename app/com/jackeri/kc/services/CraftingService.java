package com.jackeri.kc.services;

import com.jackeri.kc.exceptions.*;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.itemdata.ItemData;
import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.models.recipes.Recipe;
import com.jackeri.kc.repositories.entities.AdventurerRepository;
import com.jackeri.kc.repositories.items.BaseItemRepository;
import com.jackeri.kc.repositories.items.ItemOwnershipRepository;
import com.jackeri.kc.repositories.recipes.RecipeRepository;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.utils.Pair;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class CraftingService {

    private final RecipeRepository recipeRepository;
    private final AdventurerService adventurerService;
    private final BaseItemRepository baseItemRepository;

    @Inject
    public CraftingService(RecipeRepository recipeRepository,
                           AdventurerService adventurerService,
                           BaseItemRepository baseItemRepository) {
        this.recipeRepository = recipeRepository;
        this.adventurerService = adventurerService;
        this.baseItemRepository = baseItemRepository;
    }

    public CompletableFuture<Stream<Recipe>> fetchAreaRecipesForAdventurer(String adventurerKey, int offset, int count) {
        return adventurerService.getLocation(adventurerKey)
                .thenCompose(location -> {
                    if (location.getDocumentType() != DocumentType.CRAFTING_AREA)
                        throw WrongAreaException.completionException();

                    if (!location.getRegion().isPopulated())
                        throw BadRequestException.completionException();

                    return recipeRepository.fetchRecipes(
                            location.getRegion().getDocument().getRealm().getId(),
                            location.getRegion().getDocument().getLevel(),
                            offset, count);
                });
    }

    public CompletableFuture<Stream<Item>> adventurerIngredients(String adventurerKey, String recipeKey) {
        return recipeRepository.getAdventurerIngredients(adventurerKey, recipeKey);
    }

    public CompletableFuture<Stream<Item>> craft(String adventurerKey, String recipeKey, int count) {
        if (count <= 0) throw BadArgumentException.completionException();

        return  adventurerService.idleGuard(adventurerKey)
                .thenCompose(realmId -> adventurerService.getLocation(adventurerKey)
                        .thenCompose(location -> {
                            if (location.getDocumentType() != DocumentType.CRAFTING_AREA)
                                throw WrongAreaException.completionException();

                            return recipeRepository.fetchRecipe(recipeKey)
                                    .thenCompose(recipe -> {
                                        if (recipe == null) throw NotFoundException.completionException();

                                        return craft(adventurerKey, recipe, count);
                                    });
                        }));
    }

    private CompletableFuture<Stream<Item>> craft(String adventurerKey, Recipe recipe, int count) {
        return recipeRepository.craftable(adventurerKey, recipe.getKey(), count)
                .thenCompose(ingredientCount -> {

                    if (ingredientCount < recipe.getIngredients().size()) {
                        throw BadRequestException.completionException();
                    }

                    return recipeRepository.craft(adventurerKey, recipe.getKey(), count)
                            .thenCompose(ignored -> {
                                Vector<Item> items = new Vector<>();
                                for (Pair<ItemData, Long> pair : recipe.getResults()) {
                                    items.addAll(pair.getA().createItem(pair.getB() * count));
                                }

                                return baseItemRepository.createItemsForAdventurer(items, adventurerKey);
                            });
                });
    }
}
