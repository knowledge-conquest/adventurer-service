package com.jackeri.kc.services.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.typesafe.config.Config;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class JWTManager {

    private static final String SECRET_CONFIG_KEY = "jwt.secret";
    private static final String ISSUER_CONFIG_KEY = "jwt.issuer";

    private JWTVerifier verifier;

    @Inject
    public JWTManager(Config config) {

        String secret = config.getString(SECRET_CONFIG_KEY);
        String ISSUER = config.getString(ISSUER_CONFIG_KEY);

        verifier = JWT.require(Algorithm.HMAC256(secret))
                .withIssuer(ISSUER)
                .build();
    }

    public VerifiedJWT verify(String token) {
        return new VerifiedJWT(verifier.verify(token));
    }
}
