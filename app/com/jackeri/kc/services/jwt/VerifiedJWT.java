package com.jackeri.kc.services.jwt;

import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.jackeri.kc.utils.exceptions.NoUserIdException;
import lombok.Getter;

import java.util.Date;

public class VerifiedJWT {

    public static final String USER_ID_CLAIM_KEY = "userId";

    private DecodedJWT decodedJWT;

    @Getter
    private boolean expired;

    public VerifiedJWT(DecodedJWT decodedJWT) {
        this.decodedJWT = decodedJWT;
        this.expired = decodedJWT.getExpiresAt() != null && new Date().after(decodedJWT.getExpiresAt());
    }

    public Claim getClaim(String name) {
        return decodedJWT.getClaim(name);
    }

    public long getUserId() throws NoUserIdException {
        Claim claim = getClaim(USER_ID_CLAIM_KEY);

        if (claim.isNull())
            throw new NoUserIdException();

        return claim.asLong();
    }
}
