package com.jackeri.kc.services;

import com.jackeri.kc.exceptions.NotFoundException;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.DocumentType;
import com.jackeri.kc.models.Reference;
import com.jackeri.kc.models.areas.monster.MonsterArea;
import com.jackeri.kc.models.entities.Adventurer;
import com.jackeri.kc.models.entities.Entity;
import com.jackeri.kc.models.fights.Fight;
import com.jackeri.kc.models.fights.fightlogs.*;
import com.jackeri.kc.models.itemdata.ItemData;
import com.jackeri.kc.models.itemdata.ItemType;
import com.jackeri.kc.models.itemdata.WeaponData;
import com.jackeri.kc.models.itemdata.edges.MonsterLoot;
import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.models.skills.Skill;
import com.jackeri.kc.models.skills.SkillEffect;
import com.jackeri.kc.repositories.actions.FightsRepository;
import com.jackeri.kc.repositories.areas.MonsterBossAreaRepository;
import com.jackeri.kc.repositories.items.ResourceRepository;
import com.jackeri.kc.repositories.skills.SkillRepository;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.services.entities.EntityService;
import com.jackeri.kc.services.entities.MonsterService;
import com.jackeri.kc.utils.Random;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.LinkedList;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Singleton
public class FightService {

    @Inject
    private EntityService entityService;

    @Inject
    private AdventurerService adventurerService;

    @Inject
    private MonsterService monsterService;

    @Inject
    private FightsRepository fightsRepository;

    @Inject
    private ResourceRepository resourceRepository;

    @Inject
    private MonsterBossAreaRepository monsterBossAreaRepository;

    @Inject
    private SkillRepository skillRepository;

    public CompletableFuture<Fight> endFight(Fight fight) {
        Vector<Adventurer> adventurers = fight.gainExperience();

        if (fight.getFightType() == Fight.FightType.MINION) {
            for (Reference<Entity> entity : fight.getEntities()) {
                if (entity.isPopulated() && entity.getDocument().getDocumentType() == DocumentType.ADVENTURER) {
                    if (!entity.getDocument().isAlive()) {
                        entity.getDocument().heal(1L);
                        adventurers.add((Adventurer) entity.getDocument());
                        fight.getLogs().add(new Escape(entity.getKey()));
                    }
                }
            }
        }

        return adventurerService.updateAfterFightEnd(adventurers)
                .thenCompose(ignored -> skillRepository.clearCooldowns(getSkillIds(fight)))
                .thenCompose(ignored -> {
                    if (!fight.getArea().isPopulated()
                            || fight.getFightType() != Fight.FightType.BOSS
                            || fight.getWinner() <= 0) {
                        return CompletableFuture.completedFuture(null);
                    }

                    return monsterBossAreaRepository.defeatBoss(fight.getArea().getKey())
                            .thenCompose(ignoredVoid -> monsterBossAreaRepository.openTpAreas(fight.getArea().getKey()));
                }).thenCompose((ignored -> distributeLoots(fight)))
                .thenCompose(ignored -> fightsRepository.removeMonsters(fight.getKey()))
                .thenCompose(ignored -> fightsRepository.removeParticipants(fight.getKey()))
                .thenCompose(ignored -> {
                    fight.getLogs().add(new End(fight.getWinner()));
                    return fightsRepository.updateLogs(fight);
                }).thenCompose(ignored -> fightsRepository.endFight(fight))
                .thenApply(ignoredVoid -> fight);
    }

    private CompletableFuture<Void> distributeLoots(Fight fight) {
        int winner = fight.getWinner();
        if (winner == 0) {
            return CompletableFuture.completedFuture(null);
        }

        return fightsRepository.getLoots(fight.getKey(), winner < 0)
                .thenCompose(lootStream -> {
                    // Place loot in vector to be able to iterate multiple times
                    Vector<MonsterLoot> loots = new Vector<>();
                    lootStream.forEach(loots::add);

                    // stop is there is no loots
                    if (loots.isEmpty()) {
                        return CompletableFuture.completedFuture(null);
                    }

                    CompletableFuture<Void> future = null;
                    for (Reference<Entity> entity : fight.getWinners()) {
                        if (entity.isPopulated() && entity.getDocument().getDocumentType() == DocumentType.ADVENTURER) {
                            // loops through every winner that is an adventurer

                            // TODO: handle equipment loots
                            // Create the looted items
                            Vector<Item.ItemResponse> itemLog = new Vector<>();
                            Vector<Item> resources = new Vector<>();
                            for (MonsterLoot loot : loots) {
                                if (loot.getItemData().isPopulated()) {
                                    ItemData itemData = loot.getItemData().getDocument();
                                    if (itemData.getItemType() == ItemType.RESOURCE) {
                                        itemData.createItem(Random.getLong(loot.getQuantity()), true)
                                                .forEach(item -> {
                                                    resources.add(item);
                                                    itemLog.add(item.toResponse());
                                                });
                                    }
                                }
                            }
                            fight.getLogs().add(new Loot(entity.getKey(), itemLog));

                            // Add items to the adventurer's inventory
                            CompletableFuture<Void> current = resourceRepository
                                    .addResources(resources, entity.getKey());
                            if (future == null) {
                                future = current;
                            } else {
                                future.thenCompose(ignored -> current);
                            }
                        }
                    }

                    return future != null ? future : CompletableFuture.completedFuture(null);
                });
    }

    public CompletableFuture<Fight> executeAttack(Fight fight, Adventurer attacker, String defenderKey, Skill skill) {

        Entity defender = fight.getEntity(defenderKey).getDocument();

        if (skill == null || skill.getSkillEffect() == SkillEffect.DAMAGE) {
            executeAttack(attacker, defender, fight, skill);
        } else if (skill.getSkillEffect() == SkillEffect.HEAL) {
            executeHeal(attacker, defender, fight, skill);
        }

        CompletableFuture<Void> update = CompletableFuture.allOf(
                entityService.updateHealth(attacker),
                entityService.updateHealth(defender),
                fightsRepository.updateLogs(fight)
        );

        if (fight.hasEnded()) {
            return update.thenCompose(ignored -> endFight(fight))
                    .thenApply(ignoredVoid -> fight);
        }

        return update.thenCompose(ignored -> {
            if (skill == null) return CompletableFuture.completedFuture(null);
            return skillRepository.setCooldown(skill.getKey(), skill.getSkillTier().getCooldown());
        }).thenCompose(ignored -> fightsRepository.getFight(fight.getKey()));
    }

    private void executeAttack(Entity attacker, Entity defender, Fight fight, Skill skill) {

        if (!attacker.isAlive() || !defender.isAlive()) {
            return;
        }

        boolean attackerFirst = attacker.isQuickerThan(defender);

        Entity a = attackerFirst ? attacker : defender;
        Entity b = attackerFirst ? defender : attacker;

        long damage = a.launchAttack(b, attackerFirst ? skill : null);
        if (skill == null || !attackerFirst) {
            fight.getLogs().add(new Attack(a.getKey(), b.getKey(), damage));
        } else {
            fight.getLogs().add(new com.jackeri.kc.models.fights.fightlogs.Skill(
                    a.getKey(), b.getKey(), skill.getKey(), damage));
        }

        if (!b.isAlive()) {
            if (fight.getFightType() != Fight.FightType.MINION || b.getDocumentType() != DocumentType.ADVENTURER) {
                fight.getLogs().add(new Death(b.getKey()));
            }
            return;
        }

        damage = b.launchAttack(a, attackerFirst ? null : skill);
        if (skill == null || attackerFirst) {
            fight.getLogs().add(new Attack(b.getKey(), a.getKey(), damage));
        } else {
            fight.getLogs().add(new com.jackeri.kc.models.fights.fightlogs.Skill(
                    b.getKey(), a.getKey(), skill.getKey(), damage));
        }
    }

    private void executeHeal(Entity attacker, Entity defender, Fight fight, Skill skill) {
        if (!attacker.isAlive() || !defender.isAlive()) {
            return;
        }

        long heal = skill.getValue();
        double modifier = 0.5;
        if (attacker.getDocumentType() == DocumentType.ADVENTURER) {
            Adventurer adventurer = (Adventurer) attacker;
            if (adventurer.getWeapon() != null && ((WeaponData) adventurer.getWeapon().getItemData().getDocument()).getWeaponType() == skill.getWeaponType()) {
                modifier = 1.0;
            }
        }

        heal = (long) (heal * modifier);

        attacker.heal(heal);
        fight.getLogs().add(new com.jackeri.kc.models.fights.fightlogs.Skill(
                attacker.getKey(), attacker.getKey(), skill.getKey(), heal));

        long damage = defender.launchAttack(attacker, null);
        fight.getLogs().add(new Attack(defender.getKey(), attacker.getKey(), damage));
    }

    public CompletableFuture<Fight> startMonsterAreaFight(String adventurerKey, MonsterArea area) {
        return monsterService.spawnMonster(area.getMonsterData().getDocument())
                .thenCompose(monster -> {
                    Vector<String> attackers = new Vector<>();
                    attackers.add(CollectionType.ENTITIES.getId(adventurerKey));

                    Vector<String> defenders = new Vector<>();
                    defenders.add(monster.getId());

                    Fight.FightType fightType = null;
                    switch (area.getMonsterData().getDocument().getMonsterType()) {
                        case MINION:
                            fightType = Fight.FightType.MINION;
                            break;
                        case BOSS:
                            fightType = Fight.FightType.BOSS;
                            break;
                    }

                    return fightsRepository.createFight(attackers, defenders, fightType, area.getId());
                });
    }

    public CompletableFuture<Fight> getAdventurerFight(String key) {
        return fightsRepository.getAdventurerFight(key)
                .thenApply(fight -> {
                    if (fight == null) throw NotFoundException.completionException();
                    return fight;
                });
    }

    public CompletionStage<Void> nextTurn(Fight fight) {

        fight.getLogs().add(new NextTurn());

        return skillRepository.reduceCooldowns(getSkillIds(fight))
                .thenApply(ignoredVoid -> null);
    }

    private Vector<String> getSkillIds(Fight fight) {
        Vector<String> skills = new Vector<>();
        for (Reference<Entity> entity : fight.getEntities()) {
            if (entity.isPopulated() && entity.getDocument().getDocumentType() == DocumentType.ADVENTURER) {
                Adventurer adventurer = (Adventurer) entity.getDocument();
                for (Skill skill : adventurer.getSkills()) {
                    skills.add(skill.getKey());
                }
            }
        }
        return skills;
    }
}
