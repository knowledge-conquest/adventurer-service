package com.jackeri.kc.services;

import com.jackeri.kc.exceptions.WrongAreaException;
import com.jackeri.kc.models.areas.treasure.LootTreasureArea;
import com.jackeri.kc.models.areas.treasure.SkillTreasureArea;
import com.jackeri.kc.models.areas.treasure.TreasureArea;
import com.jackeri.kc.models.itemdata.ItemData;
import com.jackeri.kc.models.items.Item;
import com.jackeri.kc.models.skills.Skill;
import com.jackeri.kc.repositories.areas.LootTreasureAreaRepository;
import com.jackeri.kc.repositories.areas.SkillTreasureAreaRepository;
import com.jackeri.kc.repositories.items.EquipmentRepository;
import com.jackeri.kc.repositories.items.ResourceRepository;
import com.jackeri.kc.utils.Pair;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class TreasureService {

    private final LootTreasureAreaRepository lootTreasureAreaRepository;
    private final SkillTreasureAreaRepository skillTreasureAreaRepository;
    private final ResourceRepository resourceRepository;
    private final EquipmentRepository equipmentRepository;

    @Inject
    public TreasureService(LootTreasureAreaRepository lootTreasureAreaRepository,
                           SkillTreasureAreaRepository skillTreasureAreaRepository,
                           ResourceRepository resourceRepository,
                           EquipmentRepository equipmentRepository) {
        this.lootTreasureAreaRepository = lootTreasureAreaRepository;
        this.skillTreasureAreaRepository = skillTreasureAreaRepository;
        this.resourceRepository = resourceRepository;
        this.equipmentRepository = equipmentRepository;
    }

    public CompletableFuture<LootTreasureArea> getLootTreasureAreaForAdventurer(String adventurerKey) {
        return lootTreasureAreaRepository.getAreaForAdventurer(adventurerKey)
                .thenApply(area -> {
                    if (area == null) throw WrongAreaException.completionException();
                    return area;
                });
    }

    public CompletableFuture<Stream<Item>> openLootTreasure(String adventurerKey) {
        return lootTreasureAreaRepository.openAndGetAreaForAdventurer(adventurerKey)
            .thenCompose(area -> {
                if (area == null) throw WrongAreaException.completionException();

                Vector<Item> resources = new Vector<>();
                Vector<Item> equipments = new Vector<>();

                for (Pair<ItemData, Long> loot : area.getLoots()) {
                    switch (loot.getA().getItemType()) {
                        case RESOURCE:
                                resources.addAll(loot.getA().createItem(loot.getB(), true));
                            break;
                        case EQUIPMENT:
                            equipments.addAll(loot.getA().createItem(loot.getB(), true));
                            break;
                    }
                }
                return resourceRepository.addResources(resources, adventurerKey)
                        .thenCompose(ignoredVoid -> equipmentRepository.addEquipments(equipments, adventurerKey))
                        .thenApply(ignoredVoid -> {
                            resources.addAll(equipments);
                            return resources.stream();
                        });
            });
    }

    public CompletableFuture<SkillTreasureArea> getSkillTreasureAreaForAdventurer(String adventurerKey) {
        return skillTreasureAreaRepository.getAreaForAdventurer(adventurerKey)
                .thenApply(area -> {
                    if (area == null) throw WrongAreaException.completionException();
                    return area;
                });
    }

    public CompletableFuture<Skill> openSkillTreasure(String adventurerKey) {
        return skillTreasureAreaRepository.openAndGetAreaForAdventurer(adventurerKey)
                .thenApply(area -> {
                    if (area == null) throw WrongAreaException.completionException();
                    return area.getSkill();
                });
    }
}
