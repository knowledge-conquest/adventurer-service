package com.jackeri.kc.services.entities;

import com.jackeri.kc.models.entities.Monster;
import com.jackeri.kc.models.entities.MonsterData;
import com.jackeri.kc.repositories.entities.MonsterRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class MonsterService extends EntityService {

    @Inject
    private MonsterRepository monsterRepository;

    public CompletableFuture<Monster> spawnMonster(MonsterData monsterData) {
        return monsterRepository.createAndGet(new Monster(monsterData));
    }
}
