package com.jackeri.kc.services.entities;

import com.jackeri.kc.models.entities.MonsterData;
import com.jackeri.kc.repositories.entities.MonsterDataRepository;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class MonsterDataService {

    @Inject
    private MonsterDataRepository monsterDataRepository;

    public CompletableFuture<Stream<MonsterData>> createAndGetMonsterData(Vector<MonsterData> monsters) {
        return monsterDataRepository.createAndGet(monsters);
    }
}
