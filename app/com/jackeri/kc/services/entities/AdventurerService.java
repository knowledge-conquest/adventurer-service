package com.jackeri.kc.services.entities;

import com.fasterxml.jackson.databind.JsonNode;
import com.jackeri.kc.exceptions.EntityBusyException;
import com.jackeri.kc.exceptions.NotFoundException;
import com.jackeri.kc.models.areas.Area;
import com.jackeri.kc.models.entities.Adventurer;
import com.jackeri.kc.models.entities.WeaponProficiency;
import com.jackeri.kc.models.itemdata.WeaponType;
import com.jackeri.kc.models.participations.ActionState;
import com.jackeri.kc.models.skills.Skill;
import com.jackeri.kc.repositories.entities.AdventurerRepository;
import com.jackeri.kc.repositories.items.EquipmentRepository;
import com.jackeri.kc.repositories.skills.SkillRepository;

import javax.inject.Inject;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class AdventurerService extends EntityService {

    @Inject
    private AdventurerRepository adventurerRepository;

    @Inject
    private EquipmentRepository equipmentRepository;

    @Inject
    private SkillRepository skillRepository;

    public CompletableFuture<Adventurer> create(String name, long userId) {
        Vector<WeaponProficiency> weaponProficiencies = new Vector<>();
        for (WeaponType weaponType : WeaponType.values()) {
            weaponProficiencies.add(new WeaponProficiency("", weaponType));
        }
        return adventurerRepository.createAdventurer(new Adventurer(name, userId), weaponProficiencies);
    }

    public CompletableFuture<Adventurer> getAdventurer(String adventurerKey) {
        return adventurerRepository.fetchAdventurer(adventurerKey)
                .thenApply(adventurer -> {
                    if (adventurer == null) throw NotFoundException.completionException();

                    return adventurer;
                });
    }

    public CompletableFuture<Area> getLocation(String key) {
        return adventurerRepository.getLocation(key)
                .thenApply(area -> {
                    if (area == null) throw NotFoundException.completionException();

                    return area;
                });
    }

    public CompletableFuture<Stream<Adventurer>> getUserAdventurers(long userId) {
        return adventurerRepository.adventurersOf(userId);
    }

    public CompletableFuture<Boolean> isAdventurerOwnedBy(String key, long userId) {
        return adventurerRepository.ownedBy(key, userId);
    }

    public CompletableFuture<Boolean> teleportAdventurerToRegion(String adventurerId, String regionId) {
        return adventurerRepository.teleportToRegion(adventurerId, regionId);
    }

    public CompletableFuture<String> getAdventurerRealmId(String adventurerKey) {
        return adventurerRepository.getRealmId(adventurerKey);
    }

    public CompletableFuture<Boolean> setAdventurerLocation(String adventurerKey, String locationId) {
        return adventurerRepository.setLocation(adventurerKey, locationId);
    }

    public CompletableFuture<Void> updateAfterFightEnd(Vector<Adventurer> adventurers) {
        return adventurerRepository.afterFightUpdates(adventurers);
    }

    public CompletableFuture<ActionState> getState(String key) {
        return adventurerRepository.hasRealm(key)
                .thenCompose(hasRealm -> {
                    if (!hasRealm) {
                        return CompletableFuture.completedFuture(ActionState.NO_REALM);
                    }

                    return adventurerRepository.isDead(key)
                            .thenCompose(dead -> {
                                if (dead == null) throw NotFoundException.completionException();

                                if (dead) return CompletableFuture.completedFuture(ActionState.DEAD);

                                return adventurerRepository.getActionState(key)
                                        .thenApply(actionState -> {
                                            if (actionState == null) return ActionState.IDLE;
                                            return actionState;
                                        });
                            });
                });
    }

    public CompletableFuture<Void> cultivationUpdate(String adventurerKey, long experience) {
        return getAdventurer(adventurerKey)
                .thenCompose(adventurer -> {
                    adventurer.gainExperience(experience);
                    return adventurerRepository.cultivationUpdate(
                            adventurerKey, adventurer.getLevel(), adventurer.getExperience());
                });
    }

    public CompletableFuture<Void> spawnRecoveryUpdate(String adventurerKey, long health) {
        return getAdventurer(adventurerKey)
                .thenCompose(adventurer -> {
                    adventurer.heal(health);
                    return adventurerRepository.spawnRecoveryUpdate(adventurerKey, adventurer.getHealth());
                });
    }

    public CompletableFuture<Void> trainingUpdate(String adventurerKey, double weaponProficiency, WeaponType weaponType) {
        return adventurerRepository.trainingUpdate(adventurerKey, weaponProficiency, weaponType);
    }

    public CompletableFuture<Void> skillPracticeUpdate(String adventurerKey, long experience, int slotIndex) {
        return getAdventurer(adventurerKey)
                .thenCompose(adventurer -> {
                    Skill skill = adventurer.getSkill(slotIndex);
                    if (skill == null) {
                        return CompletableFuture.completedFuture(null);
                    }

                    skill.gainExperience(experience);
                    return skillRepository.practiceUpdate(skill.getKey(), skill.getLevel(), skill.getExperience());
                });
    }

    public CompletableFuture<Void> idleGuard(String key) {
        return getState(key).thenApply(state -> {
            if (state != ActionState.IDLE) throw EntityBusyException.completionException();
            return null;
        });
    }

    public CompletableFuture<Void> leaveRealm(String adventurerKey) {
        return equipmentRepository.unEquipAll(adventurerKey)
                .thenCompose(ignoredVoid -> adventurerRepository.removeItems(adventurerKey))
                .thenCompose(ignoredVoid -> adventurerRepository.fetchAdventurer(adventurerKey))
                .thenCompose(adventurer -> {
                    adventurer.reset();
                    return adventurerRepository.getCollection().updateDocument(adventurerKey, adventurer);
                }).thenApply(ignored -> null);
    }

    public CompletionStage<Stream<WeaponProficiency>> getWeaponProficiencies(String adventurerKey) {
        return adventurerRepository.getWeaponProficiencies(adventurerKey);
    }
}
