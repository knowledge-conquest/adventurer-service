package com.jackeri.kc.services.entities;

import com.jackeri.kc.models.entities.Entity;
import com.jackeri.kc.repositories.entities.EntityRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

public class EntityService {

    @Inject
    private EntityRepository entityRepository;

    public CompletableFuture<?> updateHealth(Entity entity) {
        return entityRepository.updateHealth(entity.getKey(), entity.getHealth());
    }
}
