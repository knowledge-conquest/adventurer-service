package com.jackeri.kc.generation;

import com.jackeri.kc.models.itemdata.*;
import com.jackeri.kc.models.itemdata.edges.RecipeComponent;
import com.jackeri.kc.models.recipes.Recipe;
import com.jackeri.kc.utils.Pair;
import com.jackeri.kc.utils.Random;
import lombok.Getter;

import java.util.Vector;

public class RegionInfo {

    @Getter private final String id;
    @Getter private final long level;
    @Getter private final String realmId;

    @Getter private final Vector<WeaponType> weaponTypes;
    @Getter private final Vector<EquipmentSlot> armorsTypes;
    @Getter private int minionCount;
    @Getter private int gatheringAreas;
    @Getter private int lootTreasures;
    @Getter private int skillTreasures;
    private boolean cultivation;
    private boolean training;
    private boolean skillPractice;
    @Getter private int metalCount;
    @Getter private int woodCount;

    private final Vector<String> areas;
    @Getter private final Vector<String> weapons;
    @Getter private final Vector<String> helmets;
    @Getter private final Vector<String> chestPlates;
    @Getter private final Vector<String> pants;
    @Getter private final Vector<String> gloves;
    @Getter private final Vector<String> boots;
    @Getter private final Vector<String> metals;
    @Getter private final Vector<String> woods;
    @Getter private final Vector<String> monsterLoots;
    @Getter private final Vector<String> gatheringLoots;

    public RegionInfo(String id, long level, String realmId) {
        this.id = id;
        this.level = level;
        this.realmId = realmId;

        weaponTypes = new Vector<>();
        armorsTypes = new Vector<>();
        areas = new Vector<>();
        weapons = new Vector<>();
        helmets = new Vector<>();
        chestPlates = new Vector<>();
        pants = new Vector<>();
        gloves = new Vector<>();
        boots = new Vector<>();
        metals = new Vector<>();
        woods = new Vector<>();
        monsterLoots = new Vector<>();
        gatheringLoots = new Vector<>();

        generate();
    }

    private void generate() {
        generateWeapons();
        generateArmours();

        // gathering areas
        gatheringAreas = Random.getInt(getResourceCount() + 1);

        // minions
        minionCount = getResourceCount() - gatheringAreas;

        // training and cultivation
        if (minionCount == 0) {
            cultivation = true;
        }
        if (Random.getBoolean(40)) {
            cultivation = true;
        }
        if (Random.getBoolean(40)) {
            training = true;
        }
        if (Random.getBoolean(40)) {
            skillPractice = true;
        }

        // treasures
        int treasures = 0;
        if (Random.getBoolean(15)) {
            treasures = 1;
            if (Random.getBoolean(5)) {
                treasures = 2;
            }
        }
        for (int i = 0; i < treasures; i += 1) {
            if (Random.getBoolean(50)) {
                lootTreasures += 1;
            } else {
                skillTreasures += 1;
            }
        }
    }

    private void generateWeapons() {
        if (Random.getBoolean(60)) {
            addWeapon();
            metalCount = 1;
            woodCount = 1;

            if (Random.getBoolean(25)) {
                addWeapon();
                metalCount = 2;
                woodCount = Random.getBoolean(50) ? 2 : 1;
            }
            if (Random.getBoolean(5)) {
                addWeapon();
                metalCount = 3;
                woodCount = Random.getBoolean(50) ? 2 : 1;
            }
        }
    }

    private void addWeapon() {
        weaponTypes.add(WeaponType.values()[Random.getInt(WeaponType.values().length)]);
    }

    private void generateArmours() {
        generateArmour(EquipmentSlot.HELMET);
        generateArmour(EquipmentSlot.CHEST_PLATE);
        generateArmour(EquipmentSlot.PANTS);
        generateArmour(EquipmentSlot.GLOVES);
        generateArmour(EquipmentSlot.BOOTS);
    }

    private void generateArmour(EquipmentSlot slot) {
        if (Random.getBoolean(80)) {
            armorsTypes.add(slot);
            if (metalCount < 1) {
                metalCount = 1;
            }

            if (Random.getBoolean(5)) {
                armorsTypes.add(slot);
                if (metalCount < 2) {
                    metalCount = 2;
                }
            }
        }
    }

    public int getResourceCount() {
        return metalCount + woodCount;
    }

    public boolean hasSpawn() {
        return minionCount > 0;
    }

    public boolean hasCultivationArea() {
        return cultivation;
    }

    public boolean hasTrainingArea() {
        return training;
    }

    public boolean hasSkillPracticeArea() {
        return skillPractice;
    }

    public void addItemData(String id, ItemData item) {
        switch (item.getItemType()) {
            case EQUIPMENT:
                EquipmentData equipment = (EquipmentData) item;
                switch (equipment.getEquipmentSlot()) {
                    case WEAPON:
                        getWeapons().add(id);
                        break;
                    case HELMET:
                        getHelmets().add(id);
                        break;
                    case CHEST_PLATE:
                        getChestPlates().add(id);
                        break;
                    case PANTS:
                        getPants().add(id);
                        break;
                    case GLOVES:
                        getGloves().add(id);
                        break;
                    case BOOTS:
                        getBoots().add(id);
                        break;
                }
                break;
            case RESOURCE:
                ResourceData resource = (ResourceData) item;
                switch (resource.getResourceType()) {
                    case METAL:
                        getMetals().add(id);
                        break;
                    case WOOD:
                        getWoods().add(id);
                        break;
                    case ROCK:
                    case PLANT:
                        break;
                }
                break;
        }
    }

    Pair<Recipe, Vector<RecipeComponent>> createWeaponRecipe(int i) {
        Recipe recipe = new Recipe(getLevel(), getRealmId());
        Vector<RecipeComponent> recipeComponents = new Vector<>();
        recipeComponents.add(new RecipeComponent(
                getWeapons().get(i),
                "", 1L,
                RecipeComponent.ComponentType.RESULT
        ));
        recipeComponents.add(new RecipeComponent(
                getMetals().get(Math.min(i, getMetals().size() - 1)),
                "", (i + 1) * 60,
                RecipeComponent.ComponentType.INGREDIENT
        ));
        recipeComponents.add(new RecipeComponent(
                getWoods().get(Math.min(i, getWoods().size() - 1)),
                "", (i + 1) * 20,
                RecipeComponent.ComponentType.INGREDIENT
        ));
        return new Pair<>(recipe, recipeComponents);
    }

    public Pair<Recipe, Vector<RecipeComponent>> createArmorRecipe(Vector<String> ids, int i, int count) {
        Recipe recipe = new Recipe(getLevel(), getRealmId());
        Vector<RecipeComponent> recipeComponents = new Vector<>();
        recipeComponents.add(new RecipeComponent(
                ids.get(i),
                "", 1L,
                RecipeComponent.ComponentType.RESULT
        ));
        recipeComponents.add(new RecipeComponent(
                getMetals().get(Math.min(i, getMetals().size() - 1)),
                "", (i + 1) * count,
                RecipeComponent.ComponentType.INGREDIENT
        ));
        return new Pair<>(recipe, recipeComponents);
    }

    public void spreadLoots() {
        Vector<String> loots = new Vector<>();
        loots.addAll(getMetals());
        loots.addAll(getWoods());

        for (String loot : loots) {
            if (monsterLoots.size() == minionCount) {
                gatheringLoots.add(loot);
            } else if (gatheringLoots.size() == gatheringAreas) {
                monsterLoots.add(loot);
            } else {
                if (Random.getBoolean(50)) {
                    monsterLoots.add(loot);
                } else {
                    gatheringLoots.add(loot);
                }
            }
        }
    }

    synchronized public Vector<String> getAreas() {
        return areas;
    }
}
