package com.jackeri.kc.generation;


import com.jackeri.kc.generation.map.Map;
import com.jackeri.kc.generation.map.MapGraph;
import com.jackeri.kc.models.CollectionType;
import com.jackeri.kc.models.areas.*;
import com.jackeri.kc.models.areas.monster.MonsterBossArea;
import com.jackeri.kc.models.areas.monster.MonsterSpawnerArea;
import com.jackeri.kc.models.areas.treasure.LootTreasureArea;
import com.jackeri.kc.models.areas.treasure.SkillTreasureArea;
import com.jackeri.kc.models.areas.treasure.TreasureArea;
import com.jackeri.kc.models.entities.MonsterData;
import com.jackeri.kc.models.fights.fightlogs.Loot;
import com.jackeri.kc.models.itemdata.*;
import com.jackeri.kc.models.itemdata.edges.GatheringLoot;
import com.jackeri.kc.models.itemdata.edges.MonsterLoot;
import com.jackeri.kc.models.itemdata.edges.RecipeComponent;
import com.jackeri.kc.models.itemdata.edges.TreasureLoot;
import com.jackeri.kc.models.recipes.Recipe;
import com.jackeri.kc.models.regions.Region;
import com.jackeri.kc.models.skills.Skill;
import com.jackeri.kc.models.skills.SkillEffect;
import com.jackeri.kc.models.skills.SkillTier;
import com.jackeri.kc.repositories.areas.GatheringAreaRepository;
import com.jackeri.kc.repositories.areas.SkillTreasureAreaRepository;
import com.jackeri.kc.repositories.entities.MonsterDataRepository;
import com.jackeri.kc.repositories.itemdata.ItemDataRepository;
import com.jackeri.kc.repositories.recipes.RecipeRepository;
import com.jackeri.kc.services.areas.AreaPathService;
import com.jackeri.kc.services.areas.AreaService;
import com.jackeri.kc.services.areas.TeleportationAreaService;
import com.jackeri.kc.services.entities.AdventurerService;
import com.jackeri.kc.services.realms.RealmService;
import com.jackeri.kc.services.regions.RegionService;
import com.jackeri.kc.utils.Pair;
import com.jackeri.kc.utils.Random;

import javax.inject.Inject;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;

public class RealmGenerator {

    private final AdventurerService adventurerService;
    private final TeleportationAreaService teleportationAreaService;
    private final RealmService realmRepository;
    private final RegionService regionRepository;
    private final AreaService areaService;
    private final AreaPathService areaPathService;
    private final MonsterDataRepository monsterDataRepository;
    private final ItemDataRepository itemDataRepository;
    private final RecipeRepository recipeRepository;
    private final GatheringAreaRepository gatheringAreaRepository;
    private final SkillTreasureAreaRepository skillTreasureAreaRepository;

    @Inject
    public RealmGenerator(AdventurerService adventurerService,
                          TeleportationAreaService teleportationAreaService,
                          RealmService realmRepository,
                          RegionService regionRepository,
                          AreaService areaService,
                          AreaPathService areaPathService,
                          MonsterDataRepository monsterDataRepository,
                          ItemDataRepository itemDataRepository,
                          RecipeRepository recipeRepository,
                          GatheringAreaRepository gatheringAreaRepository,
                          SkillTreasureAreaRepository skillTreasureAreaRepository) {
        this.adventurerService = adventurerService;
        this.teleportationAreaService = teleportationAreaService;
        this.realmRepository = realmRepository;
        this.regionRepository = regionRepository;
        this.areaService = areaService;
        this.areaPathService = areaPathService;
        this.monsterDataRepository = monsterDataRepository;
        this.itemDataRepository = itemDataRepository;
        this.recipeRepository = recipeRepository;
        this.gatheringAreaRepository = gatheringAreaRepository;
        this.skillTreasureAreaRepository = skillTreasureAreaRepository;
    }

    public CompletableFuture<Boolean> createRealm(String adventurerKey) {
        String adventurerId = CollectionType.ENTITIES.getId(adventurerKey);
        return realmRepository.createRealm(adventurerId)
                .thenCompose(realmId -> createRegion(realmId, null, 1L, 5, 5))
                .thenCompose(region -> adventurerService.teleportAdventurerToRegion(adventurerId, region.getId()));
    }

    public CompletableFuture<Region> createRegion(String realmId, String previousTPAreaId,
                                                  long level, int width, int height) {
//        Random.update();
        return regionRepository.createAndGetRegion(new Region(realmId, "Region", level, width, height))
                .thenCompose(region -> {
                    RegionInfo regionInfo = new RegionInfo(region.getId(), level, realmId);

                    return generateItems(regionInfo)
                            .thenCompose(ignoredVoid -> generateCrafting(regionInfo))
                            .thenCompose(ignoredVoid -> generateGathering(regionInfo))
                            .thenCompose(ignoredVoid -> generateMinions(regionInfo))
                            .thenCompose(ignoredVoid -> generateBoss(regionInfo))
                            .thenCompose(ignoredVoid -> generateBackwardTPArea(regionInfo, previousTPAreaId))
                            .thenCompose(ignoredVoid -> generateLootTreasureAreas(regionInfo))
                            .thenCompose(ignoredVoid -> generateSkillTreasureAreas(regionInfo))
                            .thenCompose(ignoredVoid -> generateBaseAreas(regionInfo))
                            .thenCompose(ignoredVoid -> {
                                MapGraph graph = new Map(regionInfo.getAreas(), width, height, 30)
                                        .calculateGraph();
                                return CompletableFuture.allOf(
                                        areaPathService.createAreaPaths(graph.createAreaPaths()),
                                        areaService.setAreasPositions(graph.getVertices())
                                );
                            }).thenApply(ignoredVoid -> region);
                });
    }

    private CompletableFuture<Void> generateItems(RegionInfo regionInfo) {
        Vector<ItemData> items = new Vector<>();
        for (int i = 0; i < regionInfo.getMetalCount(); ++i) {
            items.add(new ResourceData(
                    ItemTier.values()[i], regionInfo.getLevel(), regionInfo.getRealmId(), ResourceType.METAL));
        }
        for (int i = 0; i < regionInfo.getWoodCount(); ++i) {
            items.add(new ResourceData(
                    ItemTier.values()[i], regionInfo.getLevel(), regionInfo.getRealmId(), ResourceType.WOOD));
        }
        for (int i = 0; i < regionInfo.getWeaponTypes().size(); ++i) {
            items.add(WeaponData.generate(
                    ItemTier.values()[i], regionInfo.getLevel(),
                    regionInfo.getWeaponTypes().get(i), regionInfo.getRealmId()));
        }

        int j = 0;
        EquipmentSlot lastSlot = null;
        for (int i = 0; i < regionInfo.getArmorsTypes().size(); ++i) {
            EquipmentSlot slot = regionInfo.getArmorsTypes().get(i);
            if (slot != lastSlot) {
                j = 0;
                lastSlot = slot;
            }
            items.add(ArmourData.generate(
                    ItemTier.values()[j], regionInfo.getLevel(), slot, regionInfo.getRealmId()));
            j++;
        }

        return itemDataRepository.create(items)
                .thenApply(idStream -> {
                    Iterator<String> ids = idStream.iterator();
                    for (ItemData item : items) {
                        regionInfo.addItemData(ids.next(), item);
                    }

                    regionInfo.spreadLoots();

                    return null;
                });
    }

    private CompletableFuture<Void> generateCrafting(RegionInfo regionInfo) {
        return areaService.create(new CraftingArea(regionInfo.getId()))
                .thenCompose(areaId -> {
                    regionInfo.getAreas().add(areaId);

                    Vector<Pair<Recipe, Vector<RecipeComponent>>> recipes = new Vector<>();
                    for (int i = 0; i < regionInfo.getWeapons().size(); ++i) {
                        recipes.add(regionInfo.createWeaponRecipe(i));
                    }
                    for (int i = 0; i < regionInfo.getHelmets().size(); ++i) {
                        recipes.add(regionInfo
                                .createArmorRecipe(regionInfo.getHelmets(), i, 80));
                    }
                    for (int i = 0; i < regionInfo.getChestPlates().size(); ++i) {
                        recipes.add(regionInfo
                                .createArmorRecipe(regionInfo.getChestPlates(), i, 100));
                    }
                    for (int i = 0; i < regionInfo.getPants().size(); ++i) {
                        recipes.add(regionInfo
                                .createArmorRecipe(regionInfo.getPants(), i, 100));
                    }
                    for (int i = 0; i < regionInfo.getGloves().size(); ++i) {
                        recipes.add(regionInfo
                                .createArmorRecipe(regionInfo.getGloves(), i, 60));
                    }
                    for (int i = 0; i < regionInfo.getBoots().size(); ++i) {
                        recipes.add(regionInfo
                                .createArmorRecipe(regionInfo.getBoots(), i, 60));
                    }

                    return recipeRepository.createRecipes(recipes)
                            .thenApply(ignoredVoid -> null);
                });
    }

    private CompletableFuture<Void> generateGathering(RegionInfo regionInfo) {
        Vector<Pair<GatheringArea, Vector<GatheringLoot>>> areas = new Vector<>();
        for (String item : regionInfo.getGatheringLoots()) {
            GatheringArea area = new GatheringArea(regionInfo.getId(), Random.getLong(1L, 5L));
            Vector<GatheringLoot> loots = new Vector<>();
            loots.add(new GatheringLoot(item, "", Random.getLongRange(1L, 10L)));
            areas.add(new Pair<>(area, loots));
        }

        return gatheringAreaRepository.storeGenerations(areas)
                .thenApply(ids -> {
                    ids.forEach(regionInfo.getAreas()::add);
                    return null;
                });
    }

    private CompletableFuture<Void> generateMinions(RegionInfo regionInfo) {
        Vector<Pair<Pair<MonsterData, MonsterSpawnerArea>, Vector<MonsterLoot>>> monsters = new Vector<>();
        int i = 0;
        for (String loot : regionInfo.getMonsterLoots()) {
            ++i;

            MonsterData monsterData = new MonsterData("Monster " + i, regionInfo.getLevel(), MonsterData.MonsterType.MINION);
            MonsterSpawnerArea area = new MonsterSpawnerArea(regionInfo.getId(), "");

            Vector<MonsterLoot> loots = new Vector<>();
            loots.add(new MonsterLoot(loot, "", Random.getLongRange(5L, 15L)));

            monsters.add(new Pair<>(new Pair<>(monsterData, area), loots));
        }

        return monsterDataRepository.storeGenerations(monsters)
                .thenApply(ids -> {
                    ids.forEach(regionInfo.getAreas()::add);
                    return null;
                });
    }

    private CompletableFuture<Void> generateBoss(RegionInfo regionInfo) {
        Pair<MonsterData, Pair<MonsterBossArea, TeleportationArea>> boss = new Pair<>(
                new MonsterData("Boss", regionInfo.getLevel(), MonsterData.MonsterType.BOSS),
                new Pair<>(
                        new MonsterBossArea(regionInfo.getId(), ""),
                        new TeleportationArea(regionInfo.getId(), false, TeleportationArea.Direction.FORWARD)
                )
        );
        return monsterDataRepository.storeBossGeneration(boss)
                .thenApply(ids -> {
                    ids.forEach(regionInfo.getAreas()::add);
                    return null;
                });
    }

    private CompletableFuture<Void> generateBackwardTPArea(RegionInfo regionInfo, String destinationId) {
        if (destinationId == null)
            return CompletableFuture.completedFuture(null);

        return areaService.create(
                new TeleportationArea(regionInfo.getId(), destinationId, true, TeleportationArea.Direction.BACKWARD)
        ).thenCompose(tpArea -> teleportationAreaService
                .setTeleportationDestination(destinationId, tpArea)
                .thenApply(ignored -> {
                    regionInfo.getAreas().add(tpArea);
                    return null;
                })
        );
    }

    private CompletableFuture<Void> generateLootTreasureAreas(RegionInfo regionInfo) {
        Vector<Pair<ItemData, Pair<LootTreasureArea, TreasureLoot>>> treasures = new Vector<>();
        for (int i = 0; i < regionInfo.getLootTreasures(); ++i) {
            EquipmentSlot slot = EquipmentSlot.values()[Random.getInt(EquipmentSlot.values().length)];
            ItemData itemData;
            if (slot == EquipmentSlot.WEAPON) {
                WeaponType weaponType = WeaponType.values()[Random.getInt(WeaponType.values().length)];
                itemData = WeaponData.generate(
                        ItemTier.LEGENDARY, regionInfo.getLevel(), weaponType, regionInfo.getRealmId());
            } else {
                itemData = ArmourData.generate(
                        ItemTier.LEGENDARY, regionInfo.getLevel(), slot, regionInfo.getRealmId());
            }
            treasures.add(
                    new Pair<>(
                            itemData,
                            new Pair<>(
                                    new LootTreasureArea(regionInfo.getId()),
                                    new TreasureLoot("", "", 1L)
                            )
                    ));
        }

        return itemDataRepository.storeLootTreasureGenerations(treasures)
                .thenApply(ids -> {
                    ids.forEach(regionInfo.getAreas()::add);
                    return null;
                });
    }

    private CompletableFuture<Void> generateSkillTreasureAreas(RegionInfo regionInfo) {
        Vector<SkillTreasureArea> treasures = new Vector<>();
        for (int i = 0; i < regionInfo.getSkillTreasures(); ++i) {
            Skill skill = new Skill(
                    SkillEffect.values()[Random.getInt(SkillEffect.values().length)],
                    SkillTier.values()[Random.getInt(SkillTier.values().length)],
                    WeaponType.values()[Random.getInt(WeaponType.values().length)]
            );
            treasures.add(new SkillTreasureArea(regionInfo.getId(), skill));
        }

        return skillTreasureAreaRepository.create(treasures)
                .thenApply(ids -> {
                    ids.forEach(regionInfo.getAreas()::add);
                    return null;
                });
    }

    private CompletableFuture<Void> generateBaseAreas(RegionInfo regionInfo) {
        Vector<Area> areas = new Vector<>();
        areas.add(new SpawnArea(regionInfo.getId()));
        if (regionInfo.hasTrainingArea()) {
            areas.add(new TrainingArea(regionInfo.getId()));
        }
        if (regionInfo.hasCultivationArea()) {
            areas.add(new CultivationArea(regionInfo.getId()));
        }
        if (regionInfo.hasSkillPracticeArea()) {
            areas.add(new SkillPracticeArea(regionInfo.getId()));
        }

        return areaService.create(areas)
                .thenApply(ids -> {
                    ids.forEach(regionInfo.getAreas()::add);
                    return null;
                });
    }
}
