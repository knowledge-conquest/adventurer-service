package com.jackeri.kc.generation.map;

import com.jackeri.kc.generation.map.cells.*;
import com.jackeri.kc.utils.Pair;
import com.jackeri.kc.utils.Random;
import com.jackeri.kc.utils.V2Int;
import play.libs.Json;

import java.util.Collections;
import java.util.Stack;
import java.util.Vector;

public class Map {

    private final int width;

    private final int height;

    private final int additionalEdgePossibility;

    private final Vector<VertexCell> vertices;

    private final Vector<Vector<Cell>> map;

    public Map(Vector<String> vertexIds, int width, int height, int additionalEdgePossibility) {
        this.width = width;
        this.height = height;
        this.additionalEdgePossibility = additionalEdgePossibility;

        // Create base map
        this.map = new Vector<>(width);
        for (int x = 0; x < width; ++x) {
            map.add(new Vector<>(height));
            for (int y = 0; y < height; ++y) {
                V2Int position = new V2Int(x, y);
                map.get(x).add(new BaseCell(V2Int.toIndex(position, width), position));
            }
        }

        // Place vertices
        this.vertices = new Vector<>(vertexIds.size());
        Vector<Integer> cellIndexes = Random.getRandomIndexes(width * height);
        for (int i = 0; i < vertexIds.size(); i += 1) {
            V2Int position = V2Int.fromIndex(cellIndexes.get(i), width);
            VertexCell vertex = new VertexCell(vertexIds.get(i), V2Int.toIndex(position, width), position);

            vertices.add(vertex);
            map.get(position.getX()).set(position.getY(), vertex);
        }

        generateMaze();
    }

    private void generateMaze() {
        UnionFind unionFind = new UnionFind(width * height);

        // loop through every edge of the graph.
        Vector<Pair<V2Int, Movement>> edges = edges();
        for (Pair<V2Int, Movement> edge : edges) {
            V2Int position = edge.getA();
            Movement movement = edge.getB();

            Cell a = cellAt(position);
            Cell b = cellAt(position.getMoved(movement));

            int ufIndexA = unionFind.find(a.getIndex());
            int ufIndexB = unionFind.find(b.getIndex());
            boolean noLoop = ufIndexA != ufIndexB;

            if (noLoop) {
                unionFind.union(ufIndexA, ufIndexB);
            }

            // Check if the edge can be added to the graph
            if (noLoop || Random.getInt(100) < additionalEdgePossibility) {
                connectCells(a, b, movement);
            }
        }
    }

    private void connectCells(Cell a, Cell b, Movement movement) {
        if (a.hasAnyCommonCell(b)) {
            return;
        }

        // Add movements
        a.addMovement(movement);
        b.addMovement(movement.getOppositeMovement());

        Vector<VertexCell> verticesA = a.getConnectedVertices();
        Vector<VertexCell> verticesB = b.getConnectedVertices();
        Vector<BaseCell> cellsA = a.getConnectedCells();
        Vector<BaseCell> cellsB = b.getConnectedCells();

        for (VertexCell vertex : verticesA) {
            vertex.connectVertices(verticesB);
            vertex.connectCells(cellsB);
        }

        for (VertexCell vertex : verticesB) {
            vertex.connectVertices(verticesA);
            vertex.connectCells(cellsA);
        }

        for (BaseCell cell : cellsA) {
            cell.connectVertices(verticesB);
            cell.connectCells(cellsB);
        }

        for (BaseCell cell : cellsB) {
            cell.connectVertices(verticesA);
            cell.connectCells(cellsA);
        }
    }

    private Vector<Pair<V2Int, Movement>> edges() {
        Vector<Pair<V2Int, Movement>> edges = new Vector<>(
                width * (height - 1) + height * (width - 1));

        for (int x = 0; x < width - 1; x += 1) {
            for (int y = 0; y < height - 1; y += 1) {
                edges.add(new Pair<>(new V2Int(x, y), Movement.RIGHT));
                edges.add(new Pair<>(new V2Int(x, y), Movement.DOWN));
            }
        }

        for (int x = 0; x < width - 1; x += 1) {
            edges.add(new Pair<>(new V2Int(x, height - 1), Movement.RIGHT));
        }

        for (int y = 0; y < width - 1; y += 1) {
            edges.add(new Pair<>(new V2Int(width - 1, y), Movement.DOWN));
        }

        Collections.shuffle(edges, Random.get());
        return edges;
    }

    private Cell cellAt(V2Int position) {
        return map.get(position.getX()).get(position.getY());
    }

    public MapGraph calculateGraph() {
        MapGraph graph = new MapGraph();

        for (VertexCell vertex : vertices) {
            clearVisited();

            graph.addVertex(vertex.getId(), vertex.getPosition());

            V2Int position = vertex.getPosition().copy();
            Stack<Movement> path = new Stack<>();

            while (!vertex.isVisited()) {

                // Get the current cell and mark it as visited;
                Cell cell = cellAt(position);
                cell.setWalked(true);

                // Check if a vertex has been found
                if (cell.isVertex() && cell != vertex) {

                    // An edge has been found
                    graph.addEdge(vertex, (VertexCell) cell, new Vector<>(path));

                    cell.setVisited(true);
                    position.move(path.pop().getOppositeMovement());

                } else {

                    // Check for possible movements
                    Movement movement = getFirstPossibleMovement(cell);
                    if (movement != null) {

                        // Walk through the path
                        path.push(movement);
                        position.move(movement);
                    } else {

                        // Dead end!
                        cell.setVisited(true);
                        if (!path.empty()) {
                            position.move(path.pop().getOppositeMovement());
                        }
                    }
                }
            }
        }

        return graph;
    }

    private Movement getFirstPossibleMovement(Cell cell) {
        for (Movement movement : Movement.movements()) {
            V2Int p = cell.getPosition().getMoved(movement);

            if (positionOnMap(p)) {
                Cell neighbour = cellAt(p);

                if (!neighbour.isWalked() && cell.hasMovement(movement)) {
                    return movement;
                }
            }
        }

        return null;
    }

    private boolean positionOnMap(V2Int position) {
        return position.getX() >= 0 && position.getX() < width
                && position.getY() >= 0 && position.getY() < height;
    }

    private void clearVisited() {
        for (Vector<Cell> cs : map) {
            for (Cell c : cs) {
                c.setVisited(false);
                c.setWalked(false);
            }
        }
    }
}
