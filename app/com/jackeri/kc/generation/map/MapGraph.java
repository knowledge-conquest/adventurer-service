package com.jackeri.kc.generation.map;

import com.jackeri.kc.generation.map.cells.VertexCell;
import com.jackeri.kc.models.areas.AreaPath;
import com.jackeri.kc.utils.V2Int;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Vector;

public class MapGraph {

    @Getter
    private final Vector<Vertex> vertices;

    @Getter
    private final Vector<Edge> edges;

    @AllArgsConstructor
    public static class Vertex {
        @Getter
        private final String id;

        @Getter
        private final V2Int position;
    }

    private static class Edge {
        @Getter @Setter
        private VertexCell a;

        @Getter @Setter
        private VertexCell b;

        @Getter @Setter
        private Vector<Movement> path;

        public Edge(VertexCell a, VertexCell b, Vector<Movement> path) {
            this.a = a;
            this.b = b;
            this.path = path;
        }

        public int size() {
            return getPath().size();
        }
    }

    public MapGraph() {
        this.vertices = new Vector<>();
        this.edges =new Vector<>();
    }

    public void addVertex(String id, V2Int position) {
        vertices.add(new Vertex(id, position));
    }

    public void addEdge(VertexCell a, VertexCell b, Vector<Movement> path) {
        for (Edge e : edges) {
            if ((e.a == a && e.b == b) || (e.a == b && e.b == a)) {
                return;
            }
        }

        edges.add(new Edge(a, b, path));
    }

    public Vector<AreaPath> createAreaPaths() {
        Vector<AreaPath> areaPaths = new Vector<>();

        for (Edge edge : edges) {
            areaPaths.add(new AreaPath(edge.getA().getId(), edge.getB().getId(), edge.getPath()));
        }

        return areaPaths;
    }
}
