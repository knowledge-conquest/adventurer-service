package com.jackeri.kc.generation.map;

import com.jackeri.kc.utils.V2Int;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Movement {
    UP(0b1000, new V2Int(0, -1)),
    DOWN(0b0100, new V2Int(0, 1)),
    LEFT(0b0010, new V2Int(-1, 0)),
    RIGHT(0b0001, new V2Int(1, 0)),
    NONE(0b0000, new V2Int(0, 0));

    @Getter
    private final int value;

    private final V2Int direction;

    public int getX() {
        return direction.getX();
    }

    public int getY() {
        return direction.getY();
    }

    public Movement getOppositeMovement() {
        switch (this) {
            case UP:
                return DOWN;
            case DOWN:
                return UP;
            case LEFT:
                return RIGHT;
            case RIGHT:
                return LEFT;
            default:
                return NONE;
        }
    }

    public static Movement[] movements() {
        return new Movement[] {
                UP, DOWN, LEFT, RIGHT
        };
    }
}
