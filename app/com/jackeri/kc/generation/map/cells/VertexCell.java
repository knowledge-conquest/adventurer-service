package com.jackeri.kc.generation.map.cells;

import com.jackeri.kc.utils.V2Int;
import lombok.Getter;

import java.util.Vector;

public class VertexCell extends Cell {

    @Getter
    private final String id;

    public VertexCell(String id, int index, V2Int position) {
        super(index, position);
        this.id = id;
    }

    @Override
    public boolean isVertex() {
        return true;
    }

    @Override
    public Vector<BaseCell> getConnectedCells() {
        return new Vector<>();
    }

    @Override
    public Vector<VertexCell> getConnectedVertices() {
        Vector<VertexCell> connections = new Vector<>(1);
        connections.add(this);
        return connections;
    }
}
