package com.jackeri.kc.generation.map.cells;

import com.jackeri.kc.generation.map.Movement;
import com.jackeri.kc.utils.V2Int;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.Vector;

public abstract class Cell {

    @Getter
    private final int index;

    @Getter
    private final V2Int position;

    @Getter(AccessLevel.PROTECTED)
    private int movementValue;

    @Getter(AccessLevel.PROTECTED)
    private Vector<BaseCell> cells;

    @Getter(AccessLevel.PROTECTED)
    private Vector<VertexCell> vertices;

    @Getter @Setter
    private boolean walked;

    @Getter @Setter
    private boolean visited;

    public Cell(int index, V2Int position) {
        this.index = index;
        this.position = position;
        this.movementValue = Movement.NONE.getValue();
        this.cells = new Vector<>();
        this.vertices = new Vector<>();
        this.walked = false;
        this.visited = false;
    }

    public boolean hasAnyCommonCell(Cell other) {
        for (Cell cell : getVertices()) {
            if (cell.isConnectedTo(other)) {
                return true;
            }
        }

        for (Cell cell : getCells()) {
            if (cell.isConnectedTo(other)) {
                return true;
            }
        }

        return false;
    }

    private boolean isConnectedTo(Cell other) {
        if (other == null) return false;
        if (this == other) return true;

        for (Cell cell : getVertices()) {
            if (other == cell) {
                return true;
            }
        }

        for (Cell cell : getCells()) {
            if (other == cell) {
                return true;
            }
        }

        return false;
    }

    public abstract boolean isVertex();

    public void addMovement(Movement movement) {
        movementValue |= movement.getValue();
    }

    public boolean hasMovement(Movement movement) {
        return (movementValue & movement.getValue()) > 0;
    }

    public abstract Vector<BaseCell> getConnectedCells();

    public abstract Vector<VertexCell> getConnectedVertices();

    public void connectCells(Vector<BaseCell> cells) {
        this.cells.addAll(cells);
    }

    public void connectVertices(Vector<VertexCell> vertices) {
        this.vertices.addAll(vertices);
    }
}
