package com.jackeri.kc.generation.map.cells;

import com.jackeri.kc.utils.V2Int;

import java.util.Vector;

public class BaseCell extends Cell {

    public BaseCell(int index, V2Int position) {
        super(index, position);
    }

    @Override
    public boolean isVertex() {
        return false;
    }

    @Override
    public Vector<BaseCell> getConnectedCells() {
        Vector<BaseCell> connections = new Vector<>(getCells().size() + 1);
        connections.add(this);
        connections.addAll(getCells());
        return connections;
    }

    @Override
    public Vector<VertexCell> getConnectedVertices() {
        Vector<VertexCell> connections = new Vector<>(getVertices().size());
        connections.addAll(getVertices());
        return connections;
    }
}
