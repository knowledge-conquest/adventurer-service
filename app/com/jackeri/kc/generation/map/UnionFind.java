package com.jackeri.kc.generation.map;

import java.util.Vector;

public class UnionFind {

    private final Vector<Subset> subsets;

    public UnionFind(int size) {
        subsets = new Vector<>(size);
        for (int i = 0; i < size; i += 1) {
            subsets.add(new Subset(i));
        }
    }

    private static class Subset {
        int parent;
        int rank = 0;

        public Subset(int parent) {
            this.parent = parent;
        }
    }

    // A utility function to find set of an element i
    // (uses path compression technique)
    public int find(int i) {

        // find root and make root as parent of i (path compression)
        if (subsets.get(i).parent != i)  {
            subsets.get(i).parent = find(subsets.get(i).parent);
        }

        return subsets.get(i).parent;
    }

    // A function that does union of two sets of x and y
    // (uses union by rank)
    public void union(int x, int y) {
        int xRoot = find(x);
        int yRoot = find(y);

        // Attach smaller rank tree under root of high rank tree
        // (Union by Rank)
        if (subsets.get(xRoot).rank < subsets.get(yRoot).rank)  {
            subsets.get(xRoot).parent = yRoot;
        } else if (subsets.get(xRoot).rank > subsets.get(yRoot).rank) {
            subsets.get(yRoot).parent = xRoot;
        } else {
            // If ranks are same, then make one as root and increment
            // its rank by one
            subsets.get(yRoot).parent = xRoot;
            subsets.get(xRoot).rank++;
        }
    }
}
